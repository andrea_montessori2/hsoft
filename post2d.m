clear all
nx=750;
ny=416;

rcutoff= 60;
eq_diam=[];
ecc=[];
ttt=0;
for tt=50:10:1000
    ttt=ttt+1;
    u=load(sprintf('u%06d.out',tt));
    v=load(sprintf('v%06d.out',tt));
    rhoB=load(sprintf('rhoB%06d.out',tt));
    rhoB=reshape(rhoB,nx,ny);
    rhoA=load(sprintf('rhoA%06d.out',tt));
    rhoA=reshape(rhoA,nx,ny);
    u=reshape(u,nx,ny);
    v=reshape(v,nx,ny);
    mu=sqrt(u.^2 + v.^2);
    phis=(rhoA-rhoB)./(rhoA + rhoB);
    Ibw=logical(zeros(nx,ny));
    for i=1:nx
       for j=1:ny
          if(phis(i,j)>=0.1)
             Ibw(i,j)=1;
          end
       end
    end
  %  Ilabel = bwlabel(Ibw);
  %  stat = regionprops(Ilabel,'Centroid') ;%,'EquivDiameter','Eccentricity');
  %  imshow(Ibw'); hold on; %xlim([220 nx]);
    Ilabel = bwlabel(Ibw);
    stat = regionprops(Ilabel,'centroid','EquivDiameter');
    pres_size=size(eq_diam,1);
    kkk=0;
     for iii=1:size(stat,1)
         if(stat(iii).Centroid(2)>200 && stat(iii).Centroid(2)<nx-50)
             kkk=kkk+1;
             eq_diam(pres_size+kkk,1)=stat(iii).EquivDiameter;
             %ecc(pres_size+kkk,1)=stat(iii).Eccentricity;
         end
     end

    imagesc(phis'); hold on;axis xy; axis equal;
    contour(rhoB(1:end,:)',[0.95 0.96],'LineWidth',3,'LineColor','r')
    
    for x = 1: numel(stat)
       plot(stat(x).Centroid(2),stat(x).Centroid(1),'ro');
       xc(x)=stat(x).Centroid(2);
       yc(x)=stat(x).Centroid(1);
       X(x,1)=xc(x);
       X(x,2)=yc(x);
    end
    voronoi(xc,yc)
    %nump = size(xc,2);
    %plabels = arrayfun(@(n) {sprintf('X%d', n)}, (1:nump)');
    %hold on
    %Hpl = text(xc(:), yc(:), plabels, 'FontWeight', ...
    %      'bold', 'HorizontalAlignment','center', ...
    %      'BackgroundColor', 'none');
    dt = delaunayTriangulation(X);
    N = neighbors(dt);
    [ConnList,except_list,Points] = triang_except(dt,80,nx);
   % triplot(dt,'-r');
    %
    [a,counter]=anglestriang(dt,ConnList,nx);
    [g6]= gq(a,3);
    %g6_time=cat(2,g6_time,g6);
    colormap('hot')
    %imagesc(reshape(phis(:,:),nx,ny)')
   % hold on
    %contour(rhoB(1:end,:)',[0.95 0.96],'LineWidth',2,'LineColor','r')
    axis xy
    axis equal
    colorbar
    pause (0.01)
    %caxis([-1 1])
    %F(tt) = getframe(gcf) ;
    drawnow
    tt
end
writerObj = VideoWriter('myVideo.avi');
writerObj.FrameRate = 5;

% open the video writer
open(writerObj);
% write the frames to the video
for i=1:length(F)
    % convert the image to a frame
    frame = F(i) ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);

%% isfluid
clear all
nx=384;
ny=1600;
figure
for tt=1:1
    isf=load('isf.out');
    isf=reshape(isf,nx,ny);
    %
    imagesc(isf')
    hold on
     axis xy
    axis equal
    colorbar
    pause (0.01)
    %caxis([0 0.01])
    %F(tt) = getframe(gcf) ;
    drawnow
    
end
writerObj = VideoWriter('myVideo.avi');
writerObj.FrameRate = 5;

% open the video writer
open(writerObj);
% write the frames to the video
for i=1:length(F)
    % convert the image to a frame
    frame = F(i) ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);

%% density and velocity fields

clear all
nx=600;
ny=1600;
figure
ttt=0;
isf=load('isf.out');
isf=reshape(isf,nx,ny);
for tt=1:1:1001
    ttt=ttt+1;
    u=load(sprintf('u%06d.out',tt));
    v=load(sprintf('v%06d.out',tt));
    %rho=load(sprintf('rho%06d.out',tt));
    rhoB=load(sprintf('rhoB%06d.out',tt));
    rhoB=reshape(rhoB,nx,ny);
    rhoA=load(sprintf('rhoA%06d.out',tt));
    rhoA=reshape(rhoA,nx,ny);
    rhoC=load(sprintf('rhoC%06d.out',tt));
    rhoC=reshape(rhoC,nx,ny);
    %rho=reshape(rho,nx,ny);
    u=reshape(u,nx,ny);
    v=reshape(v,nx,ny);
    mu=sqrt(u.^2 + v.^2);
    %uplot(tt)=mu(10,ny/2);
    phis=(rhoA-rhoB)./(rhoA + rhoB +rhoC);
    %
    %imagesc(reshape(phis(:,:),nx,ny)')
    %imagesc(rhoA(:,:)')
    imagesc(phis(1:nx,1:ny)')
    hold on
    contour(rhoC(:,:)',[-0.97 -0.96],'LineWidth',2,'LineColor','w')
    contour(isf(:,:)',[0 1],'LineWidth',2,'LineColor','w')

    %contour(rho(:,:)',[1 ],'LineWidth',2,'LineColor','w')
    sst=4;
    [x,y]=meshgrid(1:sst:nx,1:sst:ny);
    %uu=u./abs(mu);
    %vv=v./abs(mu);
    %quiver(x,y,reshape(uu(1:2:nx,1:2:ny,10),round(nx/2),round(nz/2))',reshape(vv(1:2:nx,1:2:ny,10),round(nx/2),round(nz/2))','LineWidth',2,'Color','w')
    %quiver(x,y,uu(1:sst:nx,1:sst:ny)',vv(1:sst:nx,1:sst:ny)','LineWidth',2,'Color','g')
    %f=gcf;
    %f.Position=[100 50 400 400]
    axis xy
    axis equal
    %axis off
    %xlim([500 750])
    %ylim([ny/2-100 ny/2+100])
    %ylim([ny/2-50 ny/2+50])
    colormap('hot')
    %colorbar 
    %caxis([0 max(max(mu))])
    %F(ttt) = getframe(f) ;
    %saveas(gcf,sprintf('FIG%d.png',tt));
    %print(gcf,sprintf('FIG%d.png',tt),'-dpng','-r300')
    drawnow
    tt
    pause %(0.001)
end

writerObj = VideoWriter('myVideo','MPEG-4');
writerObj.Quality=100;
writerObj.FrameRate = 5;


% open the video writer
open(writerObj);
% write the frames to the video
for i=1:length(F)
    % convert the image to a frame
    frame = F(i) ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);

%% avg flowfield
clear all
nx=800;
ny=402;
figure
ttt=0;
count=0;
kkkk=1;
muavg=zeros(nx,ny,100);
for tt=600:1:1001
    ttt=ttt+1;
    u=load(sprintf('u%06d.out',tt));
    v=load(sprintf('v%06d.out',tt));
    u=reshape(u,nx,ny);
    v=reshape(v,nx,ny);
    mu=sqrt(u.^2 + v.^2);
    if(ttt<=10)
        muavg(:,:,kkkk)=muavg(:,:,kkkk)+reshape(u(:,:),nx,ny,1);
    else
        muavg(:,:,kkkk)=muavg(:,:,kkkk)./10;
        kkkk=kkkk+1;
        ttt=0;
        imagesc(muavg(:,:,kkkk-1)');axis xy; axis equal;xlim([220 nx]);caxis([0 0.007])
        pause(0.01)
        drawnow
    end
    tt
end



