    module subs_lb_2c_3d
    
    use vars_lb_2c_3d
    
    implicit none
    
    contains
    
    !**********************************************************!
    subroutine moments_2c
        implicit none
        integer :: mm,i,j,k
        !$omp do private(k) 
		do k=1,nz
			do j=1,ny
				do i=1,nx
				   if(isfluid_LB(i,j,k).eq.1)then
					   rho_c(1,i,j,k)=0.0d0
					   rho_c(2,i,j,k)=0.0d0
					   uxyz(1,i,j,k)=0.0d0
					   uxyz(2,i,j,k)=0.0d0
					   uxyz(3,i,j,k)=0.0d0
					   do mm=0,26
						   rho_c(1,i,j,k)=rho_c(1,i,j,k) + g(1,mm,i,j,k) 
						   rho_c(2,i,j,k)=rho_c(2,i,j,k) + g(2,mm,i,j,k)
						   uxyz(1,i,j,k)=uxyz(1,i,j,k) + g(1,mm,i,j,k)*dex(mm) + g(2,mm,i,j,k)*dex(mm) 
						   uxyz(2,i,j,k)=uxyz(2,i,j,k) + g(1,mm,i,j,k)*dey(mm) + g(2,mm,i,j,k)*dey(mm) 
						   uxyz(3,i,j,k)=uxyz(3,i,j,k) + g(1,mm,i,j,k)*dez(mm) + g(2,mm,i,j,k)*dez(mm) 
					   enddo
					   uxyz(1,i,j,k)=uxyz(1,i,j,k)/(rho_c(1,i,j,k)+rho_c(2,i,j,k))
					   uxyz(2,i,j,k)=uxyz(2,i,j,k)/(rho_c(1,i,j,k)+rho_c(2,i,j,k))
					   uxyz(3,i,j,k)=uxyz(3,i,j,k)/(rho_c(1,i,j,k)+rho_c(2,i,j,k))
				   endif
				enddo
			enddo
		enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
    subroutine collision_2c
        implicit none
        integer :: mm,i,j,k
        real*8,dimension(2,0:26) :: feq
        real*8 :: uu,udotc,nu_avg
        !
        !$omp do private(k)
        do k=1,nz
			do j=1,ny
				do i=1,nx
					if(isfluid_LB(i,j,k).eq.1)then
						uu=0.5d0*(uxyz(1,i,j,k)*uxyz(1,i,j,k) + uxyz(2,i,j,k)*uxyz(2,i,j,k)&
								+ uxyz(3,i,j,k)*uxyz(3,i,j,k))/cssq
						nu_avg=1.0d0/((rho_c(1,i,j,k)/(rho_c(1,i,j,k)+ rho_c(2,i,j,k)))*one_ov_nuR + & 
							         (rho_c(2,i,j,k)/(rho_c(1,i,j,k)+ rho_c(2,i,j,k)))*one_ov_nuB)
						omega_c(i,j,k)=2.0d0/(6.0d0*nu_avg + 1.0d0)
						do mm=0,26
							udotc=(uxyz(1,i,j,k)*dex(mm) + uxyz(2,i,j,k)*dey(mm) + uxyz(3,i,j,k)*dez(mm))/cssq
							feq(1,mm)=p(mm)*rho_c(1,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							feq(2,mm)=p(mm)*rho_c(2,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							g(1,mm,i,j,k)=g(1,mm,i,j,k) + omega_c(i,j,k)*(feq(1,mm) - g(1,mm,i,j,k))  
							g(2,mm,i,j,k)=g(2,mm,i,j,k) + omega_c(i,j,k)*(feq(2,mm) - g(2,mm,i,j,k))
						enddo
					endif
				enddo
			enddo
		enddo
        !$omp end do
    endsubroutine

    !**********************************************************!
    subroutine collision_w_lim_2c
        implicit none
        integer :: mm,i,j,k
        real*8,dimension(2,0:26) :: feq
        real*8 :: uu,udotc,nu_avg,feqtot,ftot
        !
		c_entropy=1
        !$omp do private(k)
        do k=1,nz
			do j=1,ny
				do i=1,nx
					if(isfluid_LB(i,j,k).eq.1)then
						uu=0.5d0*(uxyz(1,i,j,k)*uxyz(1,i,j,k) + uxyz(2,i,j,k)*uxyz(2,i,j,k)&
								+ uxyz(3,i,j,k)*uxyz(3,i,j,k))/cssq
						nu_avg=1.0d0/((rho_c(1,i,j,k)/(rho_c(1,i,j,k)+ rho_c(2,i,j,k)))*one_ov_nuR + & 
							         (rho_c(2,i,j,k)/(rho_c(1,i,j,k)+ rho_c(2,i,j,k)))*one_ov_nuB)
						omega_c(i,j,k)=2.0d0/(6.0d0*nu_avg + 1.0d0)
						deltaS(i,j,k)=0.0d0
						do mm=0,26
							udotc=(uxyz(1,i,j,k)*dex(mm) + uxyz(2,i,j,k)*dey(mm) + uxyz(3,i,j,k)*dez(mm))/cssq
							feq(1,mm)=p(mm)*rho_c(1,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							feq(2,mm)=p(mm)*rho_c(2,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							feqtot=feq(1,mm) + feq(2,mm)
							ftot=g(1,mm,i,j,k) + g(2,mm,i,j,k)
							!
							deltaS(i,j,k)= deltaS(i,j,k) + ftot*(dlog(ftot/feqtot)) !feqtot*(dlog(feqtot)/p(mm)) - ftot*(dlog(ftot)/p(mm))
							!
							g(1,mm,i,j,k)=g(1,mm,i,j,k) + omega_c(i,j,k)*(feq(1,mm) - g(1,mm,i,j,k))  
							g(2,mm,i,j,k)=g(2,mm,i,j,k) + omega_c(i,j,k)*(feq(2,mm) - g(2,mm,i,j,k))
							!
						enddo							
					endif
				enddo
			enddo
		enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
	  subroutine ehrenfest_2c
	    implicit none
        integer :: i,mm
		real*8 :: udotc,uu,feq1,feq2
		integer,dimension(3) :: limres

		!$omp do private(i)
		do i=1,100
		  limres = maxloc(deltas,mask=deltas>1.0d-4) 
		  if (sum(limres).ne.0) then
			!
			uu=0.5d0*(uxyz(1,limres(1),limres(2),limres(3))*uxyz(1,limres(1),limres(2),limres(3)) &
			+ uxyz(2,limres(1),limres(2),limres(3))*uxyz(2,limres(1),limres(2),limres(3))&
								+ uxyz(3,limres(1),limres(2),limres(3))*uxyz(3,limres(1),limres(2),limres(3)))/cssq
			do mm=0,26
				udotc=(uxyz(1,limres(1),limres(2),limres(3))*dex(mm) + uxyz(2,limres(1),limres(2),limres(3))*dey(mm) + &
				uxyz(3,limres(1),limres(2),limres(3))*dez(mm))/cssq
				feq1=p(mm)*rho_c(1,limres(1),limres(2),limres(3))* &
				(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
				!
				feq2=p(mm)*rho_c(2,limres(1),limres(2),limres(3))* &
				(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
				!
				g(1,mm,limres(1),limres(2),limres(3))= feq1
				g(2,mm,limres(1),limres(2),limres(3))= feq2
			enddo
			! blank out DStmp so we don't find same realisation again
			deltaS(limres(1),limres(2),limres(3)) = 0.0d0
			
		  endif
		end do
		!$omp end do	
	  
	  end subroutine 
	!**********************************************************!
	subroutine regularized
	
	integer:: i,j,k,l
	real*8 :: pxxR,pyyR,pzzR,pxyR,pxzR,pyzR,pxxB,pyyB,pzzB,pxyB,pxzB,pyzB
	real*8 :: fneqR,fneqB,oneovcssq_dum,uu,udotc
	real*8, dimension(2,0:26) :: feq
	
    !$omp do private(k)
        do k=1,nz
            do j=1,ny
                do i=1,nz
                    if(isfluid_LB(i,j,k).eq.1)then
                        pxxR=0.0d0
		                pyyR=0.0d0
                        pzzR=0.0d0
		                pxyR=0.0d0
                        pxzR=0.0d0
                        pyzR=0.0d0
		                pxxB=0.0d0
		                pyyB=0.0d0
                        pzzB=0.0d0
		                pxyB=0.0d0
                        pxzB=0.0d0
                        pyzB=0.0d0
                        !
						
						uu=0.5d0*(uxyz(1,i,j,k)*uxyz(1,i,j,k) + uxyz(2,i,j,k)*uxyz(2,i,j,k)&
								+ uxyz(3,i,j,k)*uxyz(3,i,j,k))/cssq
						do l=0,26
							udotc=(uxyz(1,i,j,k)*dex(l) + uxyz(2,i,j,k)*dey(l) + uxyz(3,i,j,k)*dez(l))/cssq		
							feq(1,l)=p(l)*rho_c(1,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							feq(2,l)=p(l)*rho_c(2,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
						enddo
						
						!
                        do l=0,26
                            fneqR= g(1,l,i,j,k) - feq(1,l)
                            fneqB= g(2,l,i,j,k) - feq(2,l)
                            pxxR=pxxR + (dex(l)*dex(l) - cssq)*fneqR
			                pyyR=pyyR + (dey(l)*dey(l) - cssq)*fneqR
                            pzzR=pzzR + (dez(l)*dez(l) - cssq)*fneqR
			                pxyR=pxyR +  dex(l)*dey(l)*fneqR
                            pxzR=pxzR +  dex(l)*dez(l)*fneqR
                            pyzR=pyzR +  dey(l)*dez(l)*fneqR
                            !
			                pxxB=pxxB + (dex(l)*dex(l) - cssq)*fneqB
			                pyyB=pyyB + (dey(l)*dey(l) - cssq)*fneqB
                            pzzB=pzzB + (dez(l)*dez(l) - cssq)*fneqB
			                pxyB=pxyB +  dex(l)*dey(l)*fneqB
                            pxzB=pxzB +  dex(l)*dez(l)*fneqB
                            pyzB=pyzB +  dey(l)*dez(l)*fneqB
                        enddo
                        do l=0,26
                            g(1,l,i,j,k)= feq(1,l) + ((0.5d0*p(l))/(cssq**2))*((dex(l)*dex(l) - cssq)*pxxR + &
							            (dey(l)*dey(l) - cssq)*pyyR+&
                                        (dez(l)*dez(l) - cssq)*pzzR+&
							            2.0d0*dex(l)*dey(l)*pxyR +&
                                        2.0d0*dex(l)*dez(l)*pxzR +&
                                        2.0d0*dey(l)*dez(l)*pyzR)
										
			               g(2,l,i,j,k)= feq(2,l) + ((0.5d0*p(l))/(cssq**2))*((dex(l)*dex(l) - cssq)*pxxB + &
							            (dey(l)*dey(l) - cssq)*pyyB+&
                                        (dez(l)*dez(l) - cssq)*pzzB+&
							            2.0d0*dex(l)*dey(l)*pxyB +&
                                        2.0d0*dex(l)*dez(l)*pxzB +&
                                        2.0d0*dey(l)*dez(l)*pyzB)
										
							
                        enddo
                    endif
                enddo
            enddo
        enddo
		!$omp enddo
    endsubroutine
    !**********************************************************!
    subroutine surface_tension_2c
    
        implicit none
        integer :: i,j,k,mm
        real*8 :: acoeff,gradRx,gradRy,gradBx,gradBy,phis,psix,psiy,psinorm,psinorm_sq,ckl_RB,temp,cosphi
		real*8 ::gradRz,gradBz,psiz
        real*8,dimension(0:26) :: rhosum,fdum,feq,e_dot_psi
        !
        !
        !$omp do private(k)
		do k=1,nz
			do j=1,ny
				do i=1,nx
					phis=(rho_c(1,i,j,k) - rho_c(2,i,j,k))/(rho_c(1,i,j,k) + rho_c(2,i,j,k))
					if(isfluid_LB(i,j,k)==1)then
						acoeff=(9.0d0/2.0d0)*omega_c(i,j,k)*surf_tens   
						do mm=0,26
							rhosum(mm)=  rho_c(1,i+ex(mm),j+ey(mm),k+ez(mm)) + rho_c(2,i+ex(mm),j+ey(mm),k+ez(mm))    
						enddo
						!
						gradRx=0.0d0
						gradBx=0.0d0
						!
						gradRy=0.0d0
						gradBy=0.0d0
						!
						gradRz=0.0d0
						gradBz=0.0d0
						do mm=1,26
							gradRx=gradRx + (p(mm)/cssq)*dex(mm)*(rho_c(1,i+ex(mm),j+ey(mm),k+ez(mm))/rhosum(mm))
							gradBx=gradBx + (p(mm)/cssq)*dex(mm)*(rho_c(2,i+ex(mm),j+ey(mm),k+ez(mm))/rhosum(mm))
							!
							gradRy=gradRy + (p(mm)/cssq)*dey(mm)*(rho_c(1,i+ex(mm),j+ey(mm),k+ez(mm))/rhosum(mm))
							gradBy=gradBy + (p(mm)/cssq)*dey(mm)*(rho_c(2,i+ex(mm),j+ey(mm),k+ez(mm))/rhosum(mm))
							!
							gradRz=gradRz + (p(mm)/cssq)*dez(mm)*(rho_c(1,i+ex(mm),j+ey(mm),k+ez(mm))/rhosum(mm))
							gradBz=gradBz + (p(mm)/cssq)*dez(mm)*(rho_c(2,i+ex(mm),j+ey(mm),k+ez(mm))/rhosum(mm))
						enddo
						!
						psix= (rho_c(2,i,j,k)/rhosum(0))*gradRx - (rho_c(1,i,j,k)/rhosum(0))*gradBx !!!!*3
						psiy= (rho_c(2,i,j,k)/rhosum(0))*gradRy - (rho_c(1,i,j,k)/rhosum(0))*gradBy
						psiz= (rho_c(2,i,j,k)/rhosum(0))*gradRz - (rho_c(1,i,j,k)/rhosum(0))*gradBz
						!
						psinorm=dsqrt(psix**2 + psiy**2 + psiz**2)
						psinorm_sq=psinorm**2
						
						dnx(i,j,k)=-(psix/psinorm) !uscente dalla goccia
					!
						dny(i,j,k)=-(psiy/psinorm)
					!
						dnz(i,j,k)=-(psiz/psinorm)
						do mm=0,26
							fdum(mm)=g(1,mm,i,j,k) + g(2,mm,i,j,k) 
						enddo
						ckl_RB=min(1.d0,1.0d6*rho_c(1,i,j,k)*rho_c(2,i,j,k))
						do mm=0,26
							e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy + dez(mm)*psiz
							temp=psinorm*(p(mm)*e_dot_psi(mm)**2/psinorm_sq - b_l(mm))
							if(isnan(temp)) temp=0.0d0
							!perturbation step on the blind distros
							fdum(mm)=fdum(mm) + acoeff*temp*ckl_RB 
						enddo
						!
						do mm=0,26
							feq(mm)=(rho_c(1,i,j,k) + rho_c(2,i,j,k))*p(mm)
						enddo 
						do mm=0,26
							temp=dsqrt(dex(mm)**2 + dey(mm)**2 + dez(mm)**2)*psinorm
							cosphi=e_dot_psi(mm)/temp
							if(isnan(cosphi) .or.dabs(cosphi).gt.10.0d0) cosphi=0.0d0
							!
							temp=beta_st*rho_c(1,i,j,k)*rho_c(2,i,j,k)*cosphi/rhosum(0)**2
							g(1,mm,i,j,k)=fdum(mm)*rho_c(1,i,j,k)/rhosum(0) + temp*feq(mm)
							g(2,mm,i,j,k)=fdum(mm)*rho_c(2,i,j,k)/rhosum(0) - temp*feq(mm) 
						enddo
					endif
				enddo
			enddo
		enddo
        !$omp enddo
    endsubroutine
    !**********************************************************!
    subroutine surface_tension_cap_stress_2c
    
        implicit none
        integer :: i,j,k,mm
        real*8 :: acoeff,phis,psix,psiy,psiz,psinorm,psinorm_sq,ckl_RB,temp,cosphi
        real*8,dimension(0:26) :: rhosum, rhodiff,fdum,feq,e_dot_psi
        !
        !
        !$omp do private(k)
		do k=1,nz
			do j=1,ny
				do i=1,nx
					phis=(rho_c(1,i,j,k) - rho_c(2,i,j,k))/(rho_c(1,i,j,k) + rho_c(2,i,j,k))
					if(isfluid_LB(i,j,k)==1)then
						acoeff=(9.0d0/4.0d0)*omega_c(i,j,k)*surf_tens   
						do mm=0,26
							rhosum(mm)=  rho_c(1,i+ex(mm),j+ey(mm),k+ez(mm)) + rho_c(2,i+ex(mm),j+ey(mm),k+ez(mm))
							rhodiff(mm)= rho_c(1,i+ex(mm),j+ey(mm),k+ez(mm)) - rho_c(2,i+ex(mm),j+ey(mm),k+ez(mm))							
						enddo
						!
						psix=0.0d0
						psiy=0.0d0
						psiz=0.0d0
						do mm=1,26
							psix=psix + (p(mm)/cssq)*dex(mm)*(rhodiff(mm)/rhosum(mm))
							!
							psiy=psiy + (p(mm)/cssq)*dey(mm)*(rhodiff(mm)/rhosum(mm))
							!
							psiz=psiz + (p(mm)/cssq)*dez(mm)*(rhodiff(mm)/rhosum(mm))
						enddo
						!
						psinorm=dsqrt(psix**2 + psiy**2 + psiz**2)
						psinorm_sq=psinorm**2
						
						dnx(i,j,k)=-(psix/psinorm) !uscente dalla goccia
					!
						dny(i,j,k)=-(psiy/psinorm)
					!
						dnz(i,j,k)=-(psiz/psinorm)
						do mm=0,26
							fdum(mm)=g(1,mm,i,j,k) + g(2,mm,i,j,k) 
						enddo
						ckl_RB=min(1.d0,1.0d6*rho_c(1,i,j,k)*rho_c(2,i,j,k))
						do mm=0,26
							e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy + dez(mm)*psiz
							temp=psinorm*(p(mm)*e_dot_psi(mm)**2/psinorm_sq - b_l(mm))
							if(isnan(temp)) temp=0.0d0
							!perturbation step on the blind distros
							fdum(mm)=fdum(mm) + acoeff*temp*ckl_RB 
						enddo
						!
						do mm=0,26
							feq(mm)=(rho_c(1,i,j,k) + rho_c(2,i,j,k))*p(mm)
						enddo 
						do mm=0,26
							temp=dsqrt(dex(mm)**2 + dey(mm)**2 + dez(mm)**2)*psinorm
							cosphi=e_dot_psi(mm)/temp
							if(isnan(cosphi)) cosphi=0.0d0
							!
							temp=beta_st*rho_c(1,i,j,k)*rho_c(2,i,j,k)*cosphi/rhosum(0)**2
							g(1,mm,i,j,k)=fdum(mm)*rho_c(1,i,j,k)/rhosum(0) + temp*feq(mm)
							g(2,mm,i,j,k)=fdum(mm)*rho_c(2,i,j,k)/rhosum(0) - temp*feq(mm) 
						enddo
					endif
				enddo
			enddo
		enddo
        !$omp enddo
    endsubroutine
    !**********************************************************!
    subroutine disjoining_force_2c
    
    implicit none
    
    integer:: sum,i,j,k,l,mm,llx,llz,lly,inx,iny,inz
    real*8:: phis,phisn,dnorm
	!real*8 :: feq,feqshift,uushift,ushift,vshift,wshift,uu,udotcshift,udotc
	
    !$omp do private(k,force_disj_sc)    
    do k=1,nz
		do j=1,ny
			do i=1,nx
				 phis=(rho_c(1,i,j,k) - rho_c(2,i,j,k)) &
							/(rho_c(1,i,j,k) + rho_c(2,i,j,k))
				 dnorm=dsqrt(dnx(i,j,k)**2 + dny(i,j,k)**2 + dnz(i,j,k)**2)
				 if(isfluid_LB(i,j,k)==1 .and. dabs(phis).lt.0.2d0)then !sono interfaccia
					!
					inx=nint(dnx(i,j,k))
					iny=nint(dny(i,j,k))
					inz=nint(dnz(i,j,k))          
					!
					llx=i+5*inx 
					lly=j+5*iny
					llz=k+5*inz
					if(llx.lt.1) llx=1
					if(llx.gt.nx) llx=nx
					!
					if(lly.gt.ny) lly=ny
					if(lly.lt.1) lly=1
					!
					if(llz.gt.nz) llz=nz
					if(llz.lt.1) llz=1
					!
					phisn=(rho_c(1,llx,lly,llz)-rho_c(2,llx,lly,llz)) /(rho_c(1,llx,lly,llz)+rho_c(2,llx,lly,llz))
					if(phisn.gt.-0.2d0)then
						force_disj_sc(1)=A_rep*dnx(i,j,k)
						force_disj_sc(2)=A_rep*dny(i,j,k)
						force_disj_sc(3)=A_rep*dnz(i,j,k)
						
						do l=1,26
							g(1,l,i,j,k)=g(1,l,i,j,k) + (p(l)/cssq)*(force_disj_sc(1)*dex(l) + force_disj_sc(2)*dey(l) + force_disj_sc(3)*dez(l))
							g(1,l,llx,lly,llz)=g(1,l,llx,lly,llz) - (p(l)/cssq)*(force_disj_sc(1)*dex(l) + force_disj_sc(2)*dey(l) + force_disj_sc(3)*dez(l))
						enddo
					endif
				 endif
			enddo
		enddo
	enddo
    !$omp enddo 
    endsubroutine
    !**********************************************************!
	subroutine disjoining_force_EDM_2c
    
    implicit none
    
    integer:: sum,i,j,k,l,mm,llx,llz,lly,inx,iny,inz
    real*8:: phis,phisn,dnorm
	real*8 :: uu,uus,udotc,udotcs
	real*8, dimension(0:26) :: feq_R,feqs_R
	!real*8 :: feq,feqshift,uushift,ushift,vshift,wshift,uu,udotcshift,udotc
	
    !$omp do private(k,force_disj_sc)    
    do k=1,nz
		do j=1,ny
			do i=1,nx
				 phis=(rho_c(1,i,j,k) - rho_c(2,i,j,k)) &
							/(rho_c(1,i,j,k) + rho_c(2,i,j,k))
				 dnorm=dsqrt(dnx(i,j,k)**2 + dny(i,j,k)**2 + dnz(i,j,k)**2)
				 if(isfluid_LB(i,j,k)==1 .and. dabs(phis).lt.0.2d0)then !sono interfaccia
					!
					inx=nint(dnx(i,j,k))
					iny=nint(dny(i,j,k))
					inz=nint(dnz(i,j,k))          
					!
					llx=i+5*inx 
					lly=j+5*iny
					llz=k+5*inz
					if(llx.lt.1) llx=1
					if(llx.gt.nx) llx=nx
					!
					if(lly.gt.ny) lly=ny
					if(lly.lt.1) lly=1
					!
					if(llz.gt.nz) llz=nz
					if(llz.lt.1) llz=1
					!
					phisn=(rho_c(1,llx,lly,llz)-rho_c(2,llx,lly,llz)) /(rho_c(1,llx,lly,llz)+rho_c(2,llx,lly,llz))
					if(phisn.gt.-0.2d0)then
						force_disj_sc(1)=A_rep*dnx(i,j,k)
						force_disj_sc(2)=A_rep*dny(i,j,k)
						force_disj_sc(3)=A_rep*dnz(i,j,k)
						uu=0.5d0*(uxyz(1,i,j,k)*uxyz(1,i,j,k) + uxyz(2,i,j,k)*uxyz(2,i,j,k) + &
						          uxyz(3,i,j,k)*uxyz(3,i,j,k))/cssq
					!
						uus=0.5d0*((uxyz(1,i,j,k) + force_disj_sc(1))*(uxyz(1,i,j,k) + force_disj_sc(1)) + &
								   (uxyz(2,i,j,k) + force_disj_sc(2))*(uxyz(2,i,j,k) + force_disj_sc(2))  + &
								   (uxyz(3,i,j,k) + force_disj_sc(3))*(uxyz(3,i,j,k) + force_disj_sc(3)))/cssq
						do mm=0,8
							udotc=(uxyz(1,i,j,k)*dex(mm) + uxyz(2,i,j,k)*dey(mm) + uxyz(3,i,j,k)*dez(mm))/cssq
							udotcs=((uxyz(1,i,j,k)+force_disj_sc(1))*dex(mm) + (uxyz(2,i,j,k)+force_disj_sc(2))*dey(mm) + &
							        (uxyz(3,i,j,k)+force_disj_sc(3))*dez(mm))/cssq
							feq_R(mm)=p(mm)*rho_c(1,i,j,k)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							!
							feqs_R(mm)=p(mm)*rho_c(1,i,j,k)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
							g(1,mm,i,j,k)=g(1,mm,i,j,k) + (feqs_R(mm) - feq_R(mm))
							g(1,mm,llx,lly,llz)=g(1,mm,llx,lly,llz) - (feqs_R(mm) - feq_R(mm))
						enddo
					endif
				 endif
			enddo
		enddo
	enddo
    !$omp enddo 
    endsubroutine
    !**********************************************************!
    subroutine streaming_opt_2c
        implicit none
        
        integer:: i,j,k,l
!1st distro
        !$omp do private(k)
		do k=0,nz+1
			do j=0,ny+1
				do i=0,nx+1
					if(isfluid_LB(i,j,k).ne.3) then ! stream solo se non � gas n� solido interno
						do l=0,26 
							fb(l,i+ex(l),j+ey(l),k+ez(l)) = g(1,l,i,j,k)
						enddo
					endif
				enddo      
			enddo
		enddo
        !$omp end do
        !$omp do private(k) 
		do k=0,nz+1
			do j=0,ny+1
				do i=0,nx+1
					do l=0,26 
						g(1,l,i,j,k) = fb(l,i,j,k)
					enddo
				enddo      
			enddo
		enddo
        !$omp enddo
!2nd distro
        !$omp do private(k)
		do k=0,nz+1
			do j=0,ny+1
				do i=0,nx+1
					if(isfluid_LB(i,j,k).ne.3) then ! stream solo se non � gas n� solido interno
						do l=0,26 
							fb(l,i+ex(l),j+ey(l),k+ez(l)) = g(2,l,i,j,k)
						enddo
					endif
				enddo      
			enddo
		enddo
        !$omp end do
		!
        !$omp do private(k)
		do k=0,nz+1
			do j=0,ny+1
				do i=0,nx+1
					do l=0,26 
						g(2,l,i,j,k) = fb(l,i,j,k)
					enddo
				enddo      
			enddo
		enddo
        !$omp enddo

    endsubroutine
    
    !**********************************************************!
    subroutine all_periodic_bcs_2c
        implicit none
        !$omp single
		!faces
        !west
         g(:,:,0,1:ny,1:nz)=g(:,:,nx,1:ny,1:nz)
        !east
         g(:,:,nx+1,1:ny,1:nz)= g(:,:,1,1:ny,1:nz)
        !rear
         g(:,:,1:nx,ny+1,1:nz)= g(:,:,1:nx,1,1:nz)
        ! front
         g(:,:,1:nx,0,1:nz)= g(:,:,1:nx,ny,1:nz)
		 !north
         g(:,:,1:nx,1:ny,nz+1)= g(:,:,1:nx,1:ny,1)
        ! south
         g(:,:,1:nx,1:ny,0)= g(:,:,1:nx,1:ny,nz)
		 !edges
		 g(:,:,0,1:ny,nz+1)=g(:,:,nx,1:ny,1) 
		 g(:,:,0,1:ny,0)=g(:,:,nx,1:ny,nz) 
		 g(:,:,nx+1,1:ny,nz+1)= g(:,:,1,1:ny,1)
		 g(:,:,nx+1,1:ny,0)= g(:,:,1,1:ny,nz)
		 
		 g(:,:,1:nx,ny+1,nz+1)= g(:,:,1:nx,1,1)
		 g(:,:,1:nx,ny+1,0)= g(:,:,1:nx,1,nz)
		 g(:,:,1:nx,0,nz+1)= g(:,:,1:nx,ny,1)
		 g(:,:,1:nx,0,0)= g(:,:,1:nx,ny,nz)
		 
		 g(:,:,0,0,1:nz)= g(:,:,nx,ny,1:nz)
		 g(:,:,nx+1,ny+1,1:nz)= g(:,:,1,1,1:nz)
		 g(:,:,0,ny+1,1:nz)= g(:,:,nx,1,1:nz)
		 g(:,:,nx+1,0,1:nz)= g(:,:,1,ny,1:nz)
		 !
        !corners
         g(:,:,0,0,0)= g(:,:,nx,ny,nz)
		 g(:,:,nx+1,ny+1,nz+1)= g(:,:,1,1,1)
		 !
         g(:,:,nx+1,0,0)= g(:,:,1,ny,nz)
		 g(:,:,0,ny+1,0)= g(:,:,nx,1,nz)
		 g(:,:,0,0,nz+1)= g(:,:,nx,ny,1)
		 !
		 g(:,:,nx+1,ny+1,0)= g(:,:,1,1,nz)
		 g(:,:,nx+1,0,nz+1)= g(:,:,1,ny,1)
		 g(:,:,0,ny+1,nz+1)= g(:,:,nx,1,1)
         
         !**density boundary conds**!
         !west
         rho_c(:,0,1:ny,1:nz)=rho_c(:,nx,1:ny,1:nz)
         !east
         rho_c(:,nx+1,1:ny,1:nz)= rho_c(:,1,1:ny,1:nz)
         !rear
         rho_c(:,1:nx,ny+1,1:nz)= rho_c(:,1:nx,1,1:nz)
         ! front
         rho_c(:,1:nx,0,1:nz)= rho_c(:,1:nx,ny,1:nz)
		 !north
         rho_c(:,1:nx,1:ny,nz+1)= rho_c(:,1:nx,1:ny,1)
         ! south
         rho_c(:,1:nx,1:ny,0)= rho_c(:,1:nx,1:ny,nz)
		!edges
		 rho_c(:,0,1:ny,nz+1)= rho_c(:,nx,1:ny,1) 
		 rho_c(:,0,1:ny,0)= rho_c(:,nx,1:ny,nz) 
		 rho_c(:,nx+1,1:ny,nz+1)= rho_c(:,1,1:ny,1)
		 rho_c(:,nx+1,1:ny,0)=  rho_c(:,1,1:ny,nz)
		!
		 rho_c(:,1:nx,ny+1,nz+1)= rho_c(:,1:nx,1,1)
		 rho_c(:,1:nx,ny+1,0)= rho_c(:,1:nx,1,nz)
		 rho_c(:,1:nx,0,nz+1)= rho_c(:,1:nx,ny,1)
		 rho_c(:,1:nx,0,0)= rho_c(:,1:nx,ny,nz)
		 
		 rho_c(:,0,0,1:nz)=  rho_c(:,nx,ny,1:nz)
		 rho_c(:,nx+1,ny+1,1:nz)=  rho_c(:,1,1,1:nz)
		 rho_c(:,0,ny+1,1:nz)=  rho_c(:,nx,1,1:nz)
		 rho_c(:,nx+1,0,1:nz)=  rho_c(:,1,ny,1:nz)
		 !corners
		 rho_c(:,0,0,0)= rho_c(:,nx,ny,nz)
		 rho_c(:,nx+1,ny+1,nz+1)= rho_c(:,1,1,1)
		 !
		 rho_c(:,nx+1,0,0)= rho_c(:,1,ny,nz)
		 rho_c(:,0,ny+1,0)= rho_c(:,nx,1,nz)
		 rho_c(:,0,0,nz+1)= rho_c(:,nx,ny,1)
		 !
		 rho_c(:,nx+1,ny+1,0)= rho_c(:,1,1,nz)
		 rho_c(:,nx+1,0,nz+1)= rho_c(:,1,ny,1)
		 rho_c(:,0,ny+1,nz+1)= rho_c(:,nx,1,1)
		 !
        !$omp end single
    endsubroutine    
    !**********************************************************!
    subroutine mixed_bcs_2c
        implicit none
        real*8,dimension(0:26) :: fdum
        integer :: i,l,j,k
        !$omp single
		!faces
        !west
         g(:,:,0,1:ny,1:nz)=g(:,:,nx,1:ny,1:nz)
        !east
         g(:,:,nx+1,1:ny,1:nz)= g(:,:,1,1:ny,1:nz)
        !rear
         g(:,:,1:nx,ny+1,1:nz)= g(:,:,1:nx,1,1:nz)
        !front
         g(:,:,1:nx,0,1:nz)= g(:,:,1:nx,ny,1:nz)
		 !north
         g(:,:,1:nx,1:ny,nz+1)= g(:,:,1:nx,1:ny,1)
        !south
         g(:,:,1:nx,1:ny,0)= g(:,:,1:nx,1:ny,nz)
		 !edges
		 g(:,:,0,1:ny,nz+1)=g(:,:,nx,1:ny,1) 
		 g(:,:,0,1:ny,0)=g(:,:,nx,1:ny,nz) 
		 g(:,:,nx+1,1:ny,nz+1)= g(:,:,1,1:ny,1)
		 g(:,:,nx+1,1:ny,0)= g(:,:,1,1:ny,nz)
		 
		 g(:,:,1:nx,ny+1,nz+1)= g(:,:,1:nx,1,1)
		 g(:,:,1:nx,ny+1,0)= g(:,:,1:nx,1,nz)
		 g(:,:,1:nx,0,nz+1)= g(:,:,1:nx,ny,1)
		 g(:,:,1:nx,0,0)= g(:,:,1:nx,ny,nz)
		 
		 g(:,:,0,0,1:nz)= g(:,:,nx,ny,1:nz)
		 g(:,:,nx+1,ny+1,1:nz)= g(:,:,1,1,1:nz)
		 g(:,:,0,ny+1,1:nz)= g(:,:,nx,1,1:nz)
		 g(:,:,nx+1,0,1:nz)= g(:,:,1,ny,1:nz)
		 !
        !corners
         g(:,:,0,0,0)= g(:,:,nx,ny,nz)
		 g(:,:,nx+1,ny+1,nz+1)= g(:,:,1,1,1)
		 !
         g(:,:,nx+1,0,0)= g(:,:,1,ny,nz)
		 g(:,:,0,ny+1,0)= g(:,:,nx,1,nz)
		 g(:,:,0,0,nz+1)= g(:,:,nx,ny,1)
		 !
		 g(:,:,nx+1,ny+1,0)= g(:,:,1,1,nz)
		 g(:,:,nx+1,0,nz+1)= g(:,:,1,ny,1)
		 g(:,:,0,ny+1,nz+1)= g(:,:,nx,1,1)
         
         !**density boundary conds**!
         !west
         rho_c(:,0,1:ny,1:nz)=rho_c(:,nx,1:ny,1:nz)
         !east
         rho_c(:,nx+1,1:ny,1:nz)= rho_c(:,1,1:ny,1:nz)
         !rear
         rho_c(:,1:nx,ny+1,1:nz)= rho_c(:,1:nx,1,1:nz)
         ! front
         rho_c(:,1:nx,0,1:nz)= rho_c(:,1:nx,ny,1:nz)
		 !north
         rho_c(:,1:nx,1:ny,nz+1)= rho_c(:,1:nx,1:ny,1)
         ! south
         rho_c(:,1:nx,1:ny,0)= rho_c(:,1:nx,1:ny,nz)
		!edges
		 rho_c(:,0,1:ny,nz+1)= rho_c(:,nx,1:ny,1) 
		 rho_c(:,0,1:ny,0)= rho_c(:,nx,1:ny,nz) 
		 rho_c(:,nx+1,1:ny,nz+1)= rho_c(:,1,1:ny,1)
		 rho_c(:,nx+1,1:ny,0)=  rho_c(:,1,1:ny,nz)
		!
		 rho_c(:,1:nx,ny+1,nz+1)= rho_c(:,1:nx,1,1)
		 rho_c(:,1:nx,ny+1,0)= rho_c(:,1:nx,1,nz)
		 rho_c(:,1:nx,0,nz+1)= rho_c(:,1:nx,ny,1)
		 rho_c(:,1:nx,0,0)= rho_c(:,1:nx,ny,nz)
		 
		 rho_c(:,0,0,1:nz)=  rho_c(:,nx,ny,1:nz)
		 rho_c(:,nx+1,ny+1,1:nz)=  rho_c(:,1,1,1:nz)
		 rho_c(:,0,ny+1,1:nz)=  rho_c(:,nx,1,1:nz)
		 rho_c(:,nx+1,0,1:nz)=  rho_c(:,1,ny,1:nz)
		 !corners
		 rho_c(:,0,0,0)= rho_c(:,nx,ny,nz)
		 rho_c(:,nx+1,ny+1,nz+1)= rho_c(:,1,1,1)
		 !
		 rho_c(:,nx+1,0,0)= rho_c(:,1,ny,nz)
		 rho_c(:,0,ny+1,0)= rho_c(:,nx,1,nz)
		 rho_c(:,0,0,nz+1)= rho_c(:,nx,ny,1)
		 !
		 rho_c(:,nx+1,ny+1,0)= rho_c(:,1,1,nz)
		 rho_c(:,nx+1,0,nz+1)= rho_c(:,1,ny,1)
		 rho_c(:,0,ny+1,nz+1)= rho_c(:,nx,1,1)
        
		do k=1,nz
			do j=1,ny
				do i=1,nx
					if(isfluid_LB(i,j,k).eq.0)then
						do l=0,26
							fdum(l) = g(1,opp(l),i,j,k)
						enddo
						do l=0,26
							g(1,l,i,j,k) = fdum(l)
						enddo
						
						do l=0,26
							fdum(l) = g(2,opp(l),i,j,k)
						enddo
						do l=0,26
							g(2,l,i,j,k) = fdum(l)
						enddo 
						if(k==nz) then
							do l=0,26
								g(1,l,i,j,k) = g(1,l,i,j,k) + rho_c(1,i,j,nz-1)*6.0d0*u_wall*dex(l)*p(l)
								g(2,l,i,j,k) = g(2,l,i,j,k) + rho_c(2,i,j,nz-1)*6.0d0*u_wall*dex(l)*p(l)
							enddo
						endif
					endif
				enddo
			enddo
		enddo
        !$omp end single
    endsubroutine 
    !**********************************************************!
    subroutine constant_force_2c
    implicit none
    integer :: mm,i,j,k
    !$omp do  private(k)
	do k=1,nz
		do j=1,ny
			do i=1,nx
				if(isfluid_LB(i,j,k).eq.1)then
					do mm=1,26
						g(1,mm,i,j,k)=g(1,mm,i,j,k) + (p(mm)/cssq)*(fx_LB_2c(1)*dex(mm) + fy_LB_2c(1)*dey(mm) + fz_LB_2c(1)*dez(mm))*rho_c(1,i,j,k)
						g(2,mm,i,j,k)=g(2,mm,i,j,k) + (p(mm)/cssq)*(fx_LB_2c(2)*dex(mm) + fy_LB_2c(2)*dey(mm) + fz_LB_2c(2)*dez(mm))*rho_c(2,i,j,k)
					enddo
				endif
			enddo
		enddo
	enddo
    !$omp end do 
    endsubroutine
    !**********************************************************!
    subroutine LB_sim_data_2c
    
    implicit none
    real*8 :: visc_LB1,visc_LB2
    
    nx=150
    ny=150
	nz=150
    tau1=0.505d0
    tau2=0.505d0
    cssq=1.0d0/3.0d0
    visc_LB1=cssq*(tau1-0.5d0)
    visc_LB2=cssq*(tau2-0.5d0)
    one_ov_nuR=1.0d0/visc_LB1
    one_ov_nuB=1.0d0/visc_LB2
    allocate(fx_LB_2c(2),fy_LB_2c(2),fz_LB_2c(2))
    fx_LB_2c(2)=0.0d-6
    fy_LB_2c(2)=0.0d-6
	fz_LB_2c(2)=0.0d-6
    fx_LB_2c(1)=0.0d-6
    fy_LB_2c(1)=0.0d0
	fz_LB_2c(1)=0.0d0
    beta_st=0.999d0
    surf_tens=0.001d0
    A_rep=-0.001d0
	u_wall=0.1d0
    continue
    write(6,*) '*******************LB data*****************'
    write(6,*) 'nx',nx
    write(6,*) 'ny',ny
	write(6,*) 'nz',nz
    write(6,*) 'tau_1',tau1
    write(6,*) 'tau_2',tau2
    write(6,*) 'visc_1',visc_LB1
    write(6,*) 'visc_2',visc_LB2
    write(6,*) 'fx_LB_1',fx_LB_2c(1)
    write(6,*) 'fy_LB_1',fy_LB_2c(1)
	write(6,*) 'fz_LB_1',fz_LB_2c(1)
    write(6,*) 'fx_LB_2',fx_LB_2c(2)
    write(6,*) 'fy_LB_2',fy_LB_2c(2)
	write(6,*) 'fz_LB_2',fz_LB_2c(2)
    write(6,*) 'beta',beta_st
    write(6,*) 'surface tension',surf_tens
	write(6,*) 'repulsive force',A_rep
	write(6,*) 'u_wall',u_wall
    write(6,*) '*******************************************'
    endsubroutine
    !**********************************************************!
    subroutine init_LB_2c
    
    implicit none
    integer:: l,rad,ndrops,counter,countdrop,ii,jj,kk
    integer, allocatable :: xcenter(:),ycenter(:),zcenter(:)
    uxyz=0.0d0
    rho_c(1,:,:,:)=0.0d0
    rho_c(2,:,:,:)=1.0d0
    rad=10
    ndrops=125
    !shift=nx
    allocate(xcenter(ndrops),ycenter(ndrops),zcenter(ndrops))
	counter=0
	 xcenter(1)=rad + 8 !nx/2
	 ycenter(1)=rad +8 !ny/2
	 zcenter(1)=rad +8 !nz/2
	 do ii=2,ndrops
		xcenter(ii)=xcenter(ii-1) + 2*rad + 8
		ycenter(ii)=ycenter(ii-1)
        zcenter(ii)=zcenter(ii-1)	
		if (xcenter(ii).gt.nx-rad-6)then
			xcenter(ii)=xcenter(1)
			ycenter(ii)=ycenter(ii)+2*rad + 8
			if(ycenter(ii).gt.ny-rad-6)then
				ycenter(ii)=ycenter(1)
				zcenter(ii)=zcenter(ii)+2*rad +8
			endif
		endif
		! if(xcenter(ii).gt.shift-(rad+6))then
			! ycenter(ii)=ycenter(ii-1)+2*rad+6
			! xcenter(ii)=xcenter(1)
		! endif
			
	enddo
	!	
    do countdrop=1, ndrops
		do kk=-rad,rad
			do jj=-rad,rad
				do ii=-rad,rad
					if(ii**2 + jj**2 + kk**2.le.rad**2)then
						rho_c(1,ii + xcenter(countdrop),jj + ycenter(countdrop),kk+zcenter(countdrop) ) = 1.0d0
                        rho_c(2,ii + xcenter(countdrop),jj + ycenter(countdrop),kk+zcenter(countdrop) ) = 0.0d0
					endif
				enddo
			enddo
		enddo
    enddo
    !
	do kk=1,nz
		do jj=1,ny
			do ii=1,nx
				if(isfluid_LB(ii,jj,kk)==0)then
					rho_c(2,ii,jj,kk)=1.0d0
					rho_c(1,ii,jj,kk)=0.0d0
				endif
				if(isfluid_LB(ii,jj,kk)==3)then
					rho_c(2,ii,jj,kk)=-1.0d0
					rho_c(1,ii,jj,kk)=0.0d0
				endif
			enddo
		enddo
    enddo
	!
    do l=0,26
        g(1,l,0:nx+1,0:ny+1,0:nz+1)=p(l)*rho_c(1,0:nx+1,0:ny+1,0:nz+1)
        g(2,l,0:nx+1,0:ny+1,0:nz+1)=p(l)*rho_c(2,0:nx+1,0:ny+1,0:nz+1) ! set distribution to zero vel maxwell eq.
    enddo
    endsubroutine
    !**********************************************************!
    subroutine geometry_2c
        implicit none
        integer :: i,j,ll,ii,jj
        integer :: Lc,dd,Ltot,Htot,Ls,dec,length
        ! generic geometry module
		isfluid_LB=1
        isfluid_LB(1:nx,1:ny,1:nz)=0
        isfluid_LB(2:nx-1,2:ny-1,2:nz-1)=1
        !
    endsubroutine
    !**********************************************************!
    subroutine outLB_2c
    
        implicit none
        
        integer :: ii,jj,kk
		integer,save :: iframe=0
        ! open wrt files
        !$omp single
        if(mod(step,stamp).eq.0)then
			iframe=iframe+1
            open(unit=300,file='u'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=301,file='v'//write_fmtnumb(iframe)//'.out',status='replace')
			open(unit=311,file='w'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=302,file='rhoA'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=304,file='rhoB'//write_fmtnumb(iframe)//'.out',status='replace')
			do kk=1,nz
				do jj=1,ny
					do ii=1,nx
						write(300,*) uxyz(1,ii,jj,kk)
						write(301,*) uxyz(2,ii,jj,kk)
						write(311,*) uxyz(3,ii,jj,kk)
						write(302,*) rho_c(1,ii,jj,kk)
						write(304,*) rho_c(2,ii,jj,kk)
					enddo    
				enddo
			enddo
            close(300)
            close(301)
            close(302)
            close(304)
			close(311)
            write(6,*) 'printing LB at step', step
        endif
        !$omp end single
    endsubroutine
    !**********************************************************!
    subroutine outLB_2c_downsample
    
        implicit none
        
        integer :: ii,jj,kk
		integer,save :: iframe=0
        ! open wrt files
        !$omp single
        if(mod(step,stamp).eq.0)then
			iframe=iframe+1
            open(unit=340,file='u_d'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=341,file='v_d'//write_fmtnumb(iframe)//'.out',status='replace')
			open(unit=342,file='w_d'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=343,file='rhoA_d'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=344,file='rhoB_d'//write_fmtnumb(iframe)//'.out',status='replace')
			do kk=1,nz,2
				do jj=1,ny,2
					do ii=1,nx,2
						write(340,*) uxyz(1,ii,jj,kk)
						write(341,*) uxyz(2,ii,jj,kk)
						write(342,*) uxyz(3,ii,jj,kk)
						write(343,*) rho_c(1,ii,jj,kk)
						write(344,*) rho_c(2,ii,jj,kk)
					enddo    
				enddo
			enddo
            close(340)
            close(341)
            close(342)
            close(343)
			close(344)
            write(6,*) 'printing LB at step', step,c_entropy
        endif
        !$omp end single
    endsubroutine
     !****************************************************************!
     subroutine alloc_LB_2c
        
     implicit none
	 
	 real*8 :: p0,p1,p2,p3
	 
        allocate(p(0:26),ex(0:26),ey(0:26),ez(0:26),dex(0:26),dey(0:26),dez(0:26),opp(0:26))
        allocate(g(2,0:26,-1:nx+2,-1:ny+2,-1:nz+2),fb(0:26,-1:nx+2,-1:ny+2,-1:nz+2),uxyz(3,1:nx,1:ny,1:nz),rho_c(2,0:nx+1,0:ny+1,0:nz+1))
        allocate(isfluid_LB(0:nx+1,0:ny+1,0:nz+1),omega_c(1:nx,1:ny,1:nz),b_l(0:26))
        allocate(dnx(1:nx,1:ny,1:nz),dny(1:nx,1:ny,1:nz),dnz(1:nx,1:ny,1:nz),deltaS(1:nx,1:ny,1:nz))
        allocate(force_disj_sc(3))
        isfluid_LB=1
        g=0.0d0
        uxyz=0.0d0
        rho_c=0.0d0
        omega_c=0.0d0
		p0=(2.0d0/3.0d0)**3
		p1=(2.0d0/3.0d0)**2 * (1.0d0/6.0d0) 
		p2=(1.0d0/6.0d0)**2 * (2.0d0/3.0d0)
		p3=(1.0d0/6.0d0)**3
		p=(/p0,p1,p1,p1,p1,p1,p1,p2,p2,p2,p2,p2,p2,p2,p2,p2,p2,p2,p2,p3,p3,p3,p3,p3,p3,p3,p3/)
		ex=(/0, 1, -1, 0,  0,  0,  0,  1,  -1,  1,  -1,  0,   0,  0,   0,  1,  -1,  -1,   1,  1,   -1,    1,  -1,  -1,   1,   1,  -1/)
		ey=(/0, 0,  0, 1, -1,  0,  0,  1,  -1, -1,   1,  1,  -1,  1,  -1,  0,   0,   0,   0,  1,   -1,    1,  -1,   1,  -1,  -1,   1/)
		ez=(/0, 0,  0, 0,  0,  1, -1,  0,   0,  0,   0,  1,  -1, -1,   1,  1,  -1,   1,  -1,  1,   -1,   -1,   1,   1,  -1,   1,  -1/)
		   ! 1  2   3  4   5   6  7    8    9   10   11  12   13  14   15 16  17   18   19   20   21    22    23   24  25    26   27 
		   ! 1  3   2  5   4   7  6    9    8   11   10  13   12  15   14 17  16   19   18   21   20   23     22   25  24    27   26
		opp=(/0, 2,1,4,3,6,5,8,7,10,9,12,11,14,13,16,15,18,17,20,19,22,21,24,23,26,25/)
		dex=dfloat(ex)
		dey=dfloat(ey)
		dez=dfloat(ez)
        b_l=(/-10.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,&
                       1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,&
                       1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,1.0d0/54.0d0,&
                       1.0d0/216.0d0,1.0d0/216.0d0,1.0d0/216.0d0,1.0d0/216.0d0,1.0d0/216.0d0,1.0d0/216.0d0,1.0d0/216.0d0,1.0d0/216.0d0/)
	    ! b_l=(/-16.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,&
                       ! 2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,&
                       ! 2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,2.0d0/81.0d0,&
                       ! 7.0d0/648.0d0,7.0d0/648.0d0,7.0d0/648.0d0,7.0d0/648.0d0,7.0d0/648.0d0,7.0d0/648.0d0,7.0d0/648.0d0,7.0d0/648.0d0/)
     endsubroutine
     !****************************************************************!
    endmodule