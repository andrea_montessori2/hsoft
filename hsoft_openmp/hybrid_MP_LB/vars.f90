    module vars
    
    implicit none 
    
    real*8,parameter :: pi=3.14159265358979323846264338327950288419716939937510
    real*8, allocatable,dimension(:):: vyrandR,vxrandR,p,dex,dey
    real*8, allocatable,dimension(:):: vyrandB,vxrandB,fxyR_SRD,fxyB_SRD
    real*8, allocatable,dimension(:,:,:) :: mRsrd,mBsrd,usrd,vsrd,mxy
    real*8, allocatable,dimension(:,:) :: dtemp1a,dtemp2a,dtemp1b,dtemp2b,sxyR,vxyR,sxyB,vxyB
    real*8, allocatable,dimension(:,:) :: massboxR,massboxB,uavg,vavg,mRavg,mBavg,vtotrandx,vtotrandy
    real*8, allocatable,dimension(:,:) :: deltaP,qRx,qRy,qBx,qBy,omegaxxR,omegaxxB,omegaxyR,omegaxyB
    real*8, allocatable,dimension(:,:,:) :: forcerep,force,qxy
    integer,allocatable,dimension(:,:) :: isfluid,posR,posB
	integer,allocatable,dimension(:) :: ex,ey
    real*8 :: mfp,visc_srd,temperatureR,temperatureB
    real*8:: deltx,deltt,kbT,temp_updR, temp_updB
    real*8:: xmind,xmaxd,ymind,ymaxd,visc,k_int,k_rep
    integer :: npR,npB,nmedio,nbox,step,stamp,stamp_part,nxs,nys,nsamp,count_samp,radius,rnrg,count_rand,ndrops,part_dens
    integer :: mxrank,my_in,my_fin,nchunkmin,idrank,error
    integer :: threads
    contains
    !
    function gauss()
    !***********************************************************************
    !     
    !     LBsoft subroutine for generating random number normally
    !     distributed by the Box-Muller transformation
    !     originally written in JETSPIN by M. Lauricella et al.
    !     
    !     licensed under Open Software License v. 3.0 (OSL-3.0)
    !     author: M. Lauricella
    !     last modification July 2018
    !     
    !***********************************************************************
  
      implicit none
        
      real*8,parameter :: pi=asin(1.0d0)
      real*8 :: gauss
      real*8 :: dtemp1,dtemp2
      logical :: lredo
  
      call random_number(dtemp1)
      call random_number(dtemp2)
  
      lredo=.true.
  
    ! the number is extract again if it is nan
      do while(lredo)
        lredo=.false.
    !   Box-Muller transformation
        gauss=sqrt(- 2.0d0 *log(dtemp1))*cos(2*pi*dtemp2)
        if(isnan(cos(gauss)))lredo=.true.
      enddo
  
    end function gauss
!****************************************************************!
    function normrand(mu,sigma)
 
    !***********************************************************************
    !     
    !     LBsoft subroutine for generating random number normally
    !     distributed by the Box-Muller transformation
    !     originally written in JETSPIN by M. Lauricella et al.
    !     
    !     licensed under Open Software License v. 3.0 (OSL-3.0)
    !     author: M. Lauricella
    !     last modification July 2018
    !     
    !***********************************************************************
  
      implicit none
        
      real*8,parameter :: pi=asin(1.0d0)
      real*8 :: normrand
      real*8, intent(in) :: mu,sigma
      real*8 :: dtemp1,dtemp2
      logical :: lredo
  
      call random_number(dtemp1)
      call random_number(dtemp2)
      lredo=.true.
  
    ! the number is extract again if it is nan
      do while(lredo)
        lredo=.false.
    !   Box-Muller transformation
        normrand=mu + sigma*sqrt(- 2.0d0 *log(dtemp1))*cos(2*pi*dtemp2)
        if(isnan(cos(normrand)))lredo=.true.
      enddo
  
    end function normrand
    !******************************************************************************!
    function r4_normal_ab ( a, b )

    !*****************************************************************************80
    !
    !! R4_NORMAL_AB returns a scaled pseudonormal R4.
    !
    !  Discussion:
    !
    !    The normal probability distribution function (PDF) is sampled,
    !    with mean A and standard deviation B.
    !
    !  Licensing:
    !
    !    This code is distributed under the GNU LGPL license.
    !
    !  Modified:
    !
    !    06 August 2013
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, real ( kind = 4 ) A, the mean of the PDF.
    !
    !    Input, real ( kind = 4 ) B, the standard deviation of the PDF.
    !
    !    Input/output, integer ( kind = 4 ) SEED, a seed for the random
    !    number generator.
    !
    !    Output, real ( kind = 4 ) R4_NORMAL_AB, a sample of the normal PDF.
    !
      implicit none

      real ( kind = 8 ) a
      real ( kind = 8 ) b
      real ( kind = 8 ) r1
      real ( kind = 8 ) r2
      real ( kind = 8 ) r4_normal_ab
      real ( kind = 8 ), parameter :: r4_pi = 3.14159265358979323846264338327950288419716939937510
      real ( kind = 8 ) x

      !r1 = r4_uniform_01 ( seed )
      !r2 = r4_uniform_01 ( seed )
      call RANDOM_NUMBER(r1)
      call RANDOM_NUMBER(r2)
      x = sqrt ( - 2.0d0 * log ( r1 ) ) * cos ( 2.0d0 * r4_pi * r2 )

      r4_normal_ab = a + b * x

      return
    end
    !**********************************************************************************!
    function dimenumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the number of digits
    !     of an integer number
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************

      implicit none

      integer,intent(in) :: inum
      integer :: dimenumb
      integer :: i
      real*8 :: tmp

      i=1
      tmp=real(inum,kind=8)
      do
      if(tmp< 10.d0 )exit
        i=i+1
        tmp=tmp/ 10.0d0
      enddo

      dimenumb=i

      return

     end function dimenumb

    function write_fmtnumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the string of six characters
    !     with integer digits and leading zeros to the left
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************
 
    implicit none

    integer,intent(in) :: inum
    character(len=6) :: write_fmtnumb
    integer :: numdigit,irest
    !real*8 :: tmp
    character(len=22) :: cnumberlabel
    numdigit=dimenumb(inum)
    irest=6-numdigit
    if(irest>0)then
        write(cnumberlabel,"(a,i8,a,i8,a)")"(a",irest,",i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)repeat('0',irest),inum
    else
        write(cnumberlabel,"(a,i8,a)")"(i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)inum
    endif
 
    return
    end function write_fmtnumb
    endmodule
    
    
    
