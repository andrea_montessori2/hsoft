    module subs

#ifdef _OPENMP
    use omp_lib
#endif
    use vars

    implicit none
    contains
    
    !**********************************************************!
    subroutine  streaming_2c_mpcd
    implicit none
    
    integer :: i,ii,jj
    !$omp do  private(i)
    do i=1,npR
    !streaming RED
        sxyR(:,i)=sxyR(:,i) + vxyR(:,i)*deltt + fxyR_SRD(:)*deltt*deltt*0.5d0
        vxyR(:,i)=vxyR(:,i) + fxyR_SRD(:)*deltt
    !bcs
    !periodic on x
        if(sxyR(1,i)<xmind)then
            sxyR(1,i)=xmaxd-(xmind-sxyR(1,i))
        endif
        if(sxyR(1,i)>xmaxd)then
            sxyR(1,i)=xmind+(sxyR(1,i)-xmaxd)
        endif
            
        if(sxyR(2,i)<ymind)then
            sxyR(2,i)=ymaxd-(ymind-sxyR(2,i))
        endif
        if(sxyR(2,i)>ymaxd)then
            sxyR(2,i)=ymind+(sxyR(2,i)-ymaxd)
        endif
    !internal boundary :: no slip on walls
        ii=nint(sxyR(1,i))
        jj=nint(sxyR(2,i))
        if(isfluid(ii,jj).eq.0 .or. isfluid(ii,jj).eq.3)then
            sxyR(:,i)= sxyR(:,i)-vxyR(:,i)*deltt 
            vxyR(:,i)= -vxyR(:,i) 
        endif
    enddo
    !$omp enddo nowait
!      
    !$omp do  private(i)
    do i=1,npB
    !streaming BLUE
        sxyB(:,i)=sxyB(:,i) + vxyB(:,i)*deltt + fxyB_SRD(:)*deltt*deltt*0.5d0
        vxyB(:,i)=vxyB(:,i) + fxyB_SRD(:)*deltt
    !bcs
    !periodic on x
        if(sxyB(1,i)<xmind)then
            sxyB(1,i)=xmaxd-(xmind-sxyB(1,i))
        endif
        if(sxyB(1,i)>xmaxd)then
            sxyB(1,i)=xmind+(sxyB(1,i)-xmaxd)
        endif
            
        if(sxyB(2,i)<ymind)then
            sxyB(2,i)=ymaxd-(ymind-sxyB(2,i))
        endif
        if(sxyB(2,i)>ymaxd)then
            sxyB(2,i)=ymind+(sxyB(2,i)-ymaxd)
        endif
        !internal boundary :: no slip on walls
        ii=nint(sxyB(1,i))
        jj=nint(sxyB(2,i))
        if(isfluid(ii,jj).eq.0 .or. isfluid(ii,jj).eq.3)then
            sxyB(:,i)= sxyB(:,i)-vxyB(:,i)*deltt 
            vxyB(:,i)= -vxyB(:,i)
        endif
    enddo
    !$omp enddo
    !$omp do private(i)
    do i=1,npR
        sxyR(1,i)=sxyR(1,i) + (-1.0d0+2.0d0*dtemp1a(1,count_rand))*(deltx*0.5d0)
        sxyR(2,i)=sxyR(2,i) + (-1.0d0+2.0d0*dtemp1b(1,count_rand))*(deltx*0.5d0)
    enddo
    !$omp end do
    !$omp do private(i) 
    do i=1,npB
        sxyB(1,i)=sxyB(1,i) + (-1.0d0+2.0d0*dtemp2a(1,count_rand))*(deltx*0.5d0)
        sxyB(2,i)=sxyB(2,i) + (-1.0d0+2.0d0*dtemp2b(1,count_rand))*(deltx*0.5d0)
    enddo
    !$omp end do 
    call part_pos
    endsubroutine
    !********************************************************************************
    subroutine part_pos
    ! costruzione array di posizione
    implicit none
    
    integer :: i
    
    !$omp do private(i)
    do i=1,npR
        posR(1,i)=nint(sxyR(1,i))
		posR(2,i)=nint(sxyR(2,i))
    enddo
    !$omp enddo
    !
    !$omp do private(i)
    do i=1,npB
        posB(1,i)=nint(sxyB(1,i))
		posB(2,i)=nint(sxyB(2,i))
    enddo
    !$omp enddo
    endsubroutine
    !**********************************************************!
    subroutine collision_2c_mpcd
    ! mpcd collisional for 2c with periodic forces & andersen thermostat + local thermostat
        implicit none
    
        integer :: i,ll,ii,jj,iii,jjj,sump
        real*8 :: fmod,qmod,vvx,vvy
		
        !$omp do  private(ii,jj,iii,jjj,ll,fmod) 
        do jj=1,nys
            do ii=1,nxs 
                force(1,ii,jj)=0.0d0
                force(2,ii,jj)=0.0d0
                do ll=1,8
                    iii=ii+ex(ll)
                    jjj=jj+ey(ll)
                    if(iii.eq.0)then
                        iii=nxs
                    endif
                    if(iii.eq.nxs+1)then
                        iii=1
                    endif
                    if(jjj.eq.0)then
                        jjj=nys
                    endif
                    if(jjj.eq.nys+1)then
                        jjj=1
                    endif
                    force(1,ii,jj)=force(1,ii,jj) + p(ll)*deltaP(iii,jjj)*dex(ll)
                    force(2,ii,jj)=force(2,ii,jj) + p(ll)*deltaP(iii,jjj)*dey(ll)
                enddo
                fmod=dsqrt(force(1,ii,jj)**2 + force(2,ii,jj)**2)
                if(fmod.gt.1.0d-8)then
                    force(1,ii,jj)=k_int*force(1,ii,jj)/fmod
                    force(2,ii,jj)=k_int*force(2,ii,jj)/fmod
                endif
            enddo
        enddo
        !$omp end do
        !
        !$omp do private(ii,jj,iii,jjj,sump,ll,fmod) 
        do jj=4,nys-3
            do ii=4,nxs-3
				sump=0
				forcerep(1,ii,jj)=0.0d0
				forcerep(2,ii,jj)=0.0d0
				if(deltaP(ii,jj).lt.-0.9d0)then
					do ll=1,8
						if(deltaP(ii+3*ex(ll),jj+3*ey(ll)).gt.0.999d0) sump=sump + 1
					enddo
				endif
				if(sump.ge.6)then
                    do ll=1,8
                        iii=ii+ex(ll)
                        jjj=jj+ey(ll)
                        forcerep(1,ii,jj)=forcerep(1,ii,jj) + p(ll)*deltaP(iii,jjj)*dex(ll)
                        forcerep(2,ii,jj)=forcerep(2,ii,jj) + p(ll)*deltaP(iii,jjj)*dey(ll)
                    enddo
                    fmod=dsqrt(forcerep(1,ii,jj)**2 + forcerep(2,ii,jj)**2)
                    if(fmod.gt.1.0d-8)then
                        forcerep(1,ii,jj)=k_rep*forcerep(1,ii,jj)/fmod
                        forcerep(2,ii,jj)=k_rep*forcerep(2,ii,jj)/fmod
                    endif      
                endif
            enddo
        enddo
        !$omp end do
        !Red
        !$omp do private(jj)
        do jj=1,nys
            do ii=1,nxs
                qRx(ii,jj)=0.0d0
                qRy(ii,jj)=0.0d0
                qBx(ii,jj)=0.0d0
                qBy(ii,jj)=0.0d0
            enddo
        enddo
        !$omp end do
        !
        !$omp do  private(i,ii,jj) reduction (+:qRx)
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            qRx(ii,jj)=qRx(ii,jj) + (vxyR(1,i)-mxy(1,ii,jj))			
		enddo
		!$omp end do nowait
		!$omp do private(i,ii,jj) reduction(+:qRy)
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            qRy(ii,jj)=qRy(ii,jj) + (vxyR(2,i)-mxy(2,ii,jj))
        enddo
        !$omp end do nowait
        !Blue
        !$omp do  private(i,ii,jj) reduction (-:qBx) 
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            qBx(ii,jj)=qBx(ii,jj) - (vxyB(1,i)-mxy(1,ii,jj))
		enddo
		!$omp end do nowait
		!$omp do  private(i,ii,jj) reduction(-:qBy)
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            qBy(ii,jj)=qBy(ii,jj) - (vxyB(2,i)-mxy(2,ii,jj))
        enddo
        !$omp end do
        !total flux
        !$omp do  private(ii,jj,qmod)
        do jj=1,nys
            do ii=1,nxs  
                qxy(1,ii,jj)=qRx(ii,jj)+qBx(ii,jj)
                qxy(2,ii,jj)=qRy(ii,jj)+qBy(ii,jj)
                qmod=(qxy(1,ii,jj)**2 + qxy(2,ii,jj)**2)**0.5
                if(qmod.gt.1.0d-8)then
                    qxy(:,ii,jj)=qxy(:,ii,jj)/(qmod)
                endif
            enddo
        enddo
        !$omp end do
        !collisionR 
        !$omp single
        temp_updR=0.0d0
        temp_updB=0.0d0
        !$omp end single
		
		!$omp do private(ii,jj)
		do jj=1,nys
            do ii=1,nxs
			omegaxxR(ii,jj)= qxy(1,ii,jj)*force(1,ii,jj) + qxy(2,ii,jj)*force(2,ii,jj)
            omegaxyR(ii,jj)= -qxy(1,ii,jj)*force(2,ii,jj) + qxy(2,ii,jj)*force(1,ii,jj)
			omegaxxB(ii,jj)= qxy(1,ii,jj)*(force(1,ii,jj)+forcerep(1,ii,jj)) + qxy(2,ii,jj)*(force(2,ii,jj)+forcerep(2,ii,jj))
            omegaxyB(ii,jj)= -qxy(1,ii,jj)*(force(2,ii,jj)+forcerep(2,ii,jj)) + qxy(2,ii,jj)*(force(1,ii,jj)+forcerep(1,ii,jj))
			enddo
        enddo
		!$omp enddo
		
        !$omp do  private(i,ii,jj,vvx,vvy) reduction(+:temp_updR)
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            vvx=vxyR(1,i)-mxy(1,ii,jj)
            vvy=vxyR(2,i)-mxy(2,ii,jj)
            vxyR(1,i)=mxy(1,ii,jj) + vxrandR(i) -  vtotrandx(ii,jj) + omegaxxR(ii,jj)*vvx + omegaxyR(ii,jj)*vvy
            vxyR(2,i)=mxy(2,ii,jj) + vyrandR(i) -  vtotrandy(ii,jj) + omegaxxR(ii,jj)*vvy - omegaxyR(ii,jj)*vvx
            temp_updR=temp_updR + (vxyR(1,i)*vxyR(1,i) + vxyR(2,i)*vxyR(2,i))**0.5d0
        enddo
        !$omp end do nowait
        !collisionB        
        !$omp do  private(i,ii,jj,vvx,vvy) reduction(+:temp_updB)
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            vvx=vxyB(1,i)-mxy(1,ii,jj)
            vvy=vxyB(2,i)-mxy(2,ii,jj)
            vxyB(1,i)=mxy(1,ii,jj) + vxrandB(i) -  vtotrandx(ii,jj) + omegaxxB(ii,jj)*vvx + omegaxyB(ii,jj)*vvy
            vxyB(2,i)=mxy(2,ii,jj) + vyrandB(i) -  vtotrandy(ii,jj) + omegaxxB(ii,jj)*vvy - omegaxyB(ii,jj)*vvx
            temp_updB=temp_updB + (vxyB(1,i)*vxyB(1,i) + vxyB(2,i)*vxyB(2,i))**0.5d0
        enddo
        !$omp end do
        !
        !$omp single
        temp_updR=(0.5d0/NpR)*temp_updR
        temp_updB=(0.5d0/NpB)*temp_updB
        !$omp end single
		
        !$omp do private(i)
        do i=1,npR
            vxyR(1,i)=vxyR(1,i)*(temperatureR/temp_updR)**0.5d0
            vxyR(2,i)=vxyR(2,i)*(temperatureR/temp_updR)**0.5d0
        enddo
        !$omp end do nowait
        !$omp do private(i)
        do i=1,npB
            vxyB(1,i)=vxyB(1,i)*(temperatureB/temp_updB)**0.5d0
            vxyB(2,i)=vxyB(2,i)*(temperatureB/temp_updB)**0.5d0
        enddo
        !$omp end do nowait
        !$omp do private(i)
        do i=1,npR
            sxyR(1,i)=sxyR(1,i) - (-1.0d0+2.0d0*dtemp1a(1,count_rand))*(deltx*0.5d0)
            sxyR(2,i)=sxyR(2,i) - (-1.0d0+2.0d0*dtemp1b(1,count_rand))*(deltx*0.5d0)
        enddo
        !$omp end do nowait
        !$omp do private(i) 
        do i=1,npB
            sxyB(1,i)=sxyB(1,i) - (-1.0d0+2.0d0*dtemp2a(1,count_rand))*(deltx*0.5d0)
            sxyB(2,i)=sxyB(2,i) - (-1.0d0+2.0d0*dtemp2b(1,count_rand))*(deltx*0.5d0)
        enddo
        !$omp end do 
    endsubroutine
    !**********************************************************!
    subroutine moments_2c_mpcd
    
    implicit none
    integer :: i,ii,jj
        !
        !$omp do  private(ii,jj)
        do jj=1,nys
            do ii=1,nxs 
				massboxR(ii,jj)=0.0d0
				massboxB(ii,jj)=0.0d0
				mxy(:,ii,jj)=0.0d0
				vtotrandx(ii,jj)=0.0d0
				vtotrandy(ii,jj)=0.0d0
			enddo
        enddo
        !$omp end do nowait
		
        !$omp do  private(i)
        do i=1,npR
            vxrandR(i) = sqrt(kbT)*sqrt ( - 2.0d0 * log ( dtemp1a(i,count_rand) ) ) * cos ( 2.0d0 * pi * dtemp2a(i,count_rand) )
            vyrandR(i) = sqrt(kbT)*sqrt ( - 2.0d0 * log ( dtemp1a(i,count_rand) ) ) * sin ( 2.0d0 * pi * dtemp2a(i,count_rand) )
        enddo
        !$omp enddo
		
        !$omp do  private(i,ii,jj)  reduction(+:vtotrandx)
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            vtotrandx(ii,jj)= vtotrandx(ii,jj) +  vxrandR(i)
		enddo
		!$omp end do nowait
		!$omp do  private(i,ii,jj) reduction(+:vtotrandy) 
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            vtotrandy(ii,jj)= vtotrandy(ii,jj) +  vyrandR(i)
		enddo
		!$omp end do nowait
		!$omp do  private(i,ii,jj)  reduction(+:massboxR) 
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            massboxR(ii,jj)=massboxR(ii,jj)+1.0d0   
		enddo
		!$omp end do nowait
		!$omp do  private(i,ii,jj) reduction(+:mxy)
        do i=1,npR
            ii=posR(1,i)
            jj=posR(2,i)
            mxy(1,ii,jj)=mxy(1,ii,jj) + vxyR(1,i)          
            mxy(2,ii,jj)=mxy(2,ii,jj) + vxyR(2,i)            
        enddo
        !$omp end do nowait
		
        !Andersen thermo B
        !$omp do private(i)
        do i=1,npB
            vxrandB(i) = sqrt(kbT)*sqrt ( - 2.0d0 * log ( dtemp1b(i,count_rand) ) ) * cos ( 2.0d0 * pi * dtemp2b(i,count_rand) )
            vyrandB(i) = sqrt(kbT)*sqrt ( - 2.0d0 * log ( dtemp1b(i,count_rand) ) ) * sin ( 2.0d0 * pi * dtemp2b(i,count_rand) )
        enddo
        !$omp end do
		
        !$omp do private(i) reduction(+:vtotrandx) 
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            vtotrandx(ii,jj)= vtotrandx(ii,jj) +  vxrandB(i)
		enddo
		!$omp end do nowait
		!$omp do private(i) reduction(+:vtotrandy)  
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            vtotrandy(ii,jj)= vtotrandy(ii,jj) +  vyrandB(i)
		enddo
		!$omp end do nowait
		!$omp do private(i)  reduction(+:massboxB) 
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            massboxB(ii,jj)=massboxB(ii,jj)+1.0d0
		enddo
		!$omp end do nowait
		!$omp do private(i)  reduction(+:mxy) 
        do i=1,npB
            ii=posB(1,i)
            jj=posB(2,i)
            mxy(1,ii,jj)=mxy(1,ii,jj) + vxyB(1,i)
            mxy(2,ii,jj)=mxy(2,ii,jj) + vxyB(2,i)
        enddo
        !$omp end do
		
        !box momenta calculation and differences
        !$omp do  private(ii,jj)
        do jj=1,nys
            do ii=1,nxs 
				vtotrandx(ii,jj)=vtotrandx(ii,jj)/(massboxR(ii,jj)+massboxB(ii,jj))
				vtotrandy(ii,jj)=vtotrandy(ii,jj)/(massboxR(ii,jj)+massboxB(ii,jj))
                if(massboxR(ii,jj).ge.1.0d0 .or. massboxB(ii,jj).ge.1.0d0 )then
                    mxy(1,ii,jj)=mxy(1,ii,jj)/(massboxR(ii,jj)+massboxB(ii,jj))
                    mxy(2,ii,jj)=mxy(2,ii,jj)/(massboxR(ii,jj)+massboxB(ii,jj))
                else
                    mxy(:,ii,jj)=0.0d0
                endif
                deltaP(ii,jj)=(massboxR(ii,jj)-massboxB(ii,jj))
            enddo
        enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
    subroutine avg_fields
    
    implicit none
    
    integer :: ll,i
        
        !if(step.le.nsamp) then
            !$omp do private(i)
            do i=1,nys
                ll=mod(count_samp-1,nsamp) +1
                mRsrd(:,i,ll)=massboxR(:,i)
                mBsrd(:,i,ll)=massboxB(:,i)
                usrd(:,i,ll)=mxy(1,:,i)
                vsrd(:,i,ll)=mxy(2,:,i)
            enddo
            !$omp enddo
            !$omp single
            count_samp=count_samp+1
            !$omp end single
    endsubroutine
    !**********************************************************!
    subroutine srd_sim_data
    
	implicit none
    
	integer :: ntot
    !srd data
    nxs=136
    nys=136
    radius=14
	ndrops=16
	part_dens=60
    k_int=1.0d0
    k_rep=-1.1d0
    xmind=1.0d0
    xmaxd=dfloat(nxs) 
    ymind=1.0d0
    ymaxd=dfloat(nys)
    deltx=1.0d0
    deltt=1.0d0
    nbox=((xmaxd-xmind)/nint(deltx))*((ymaxd-xmind)/nint(deltx))
	ntot= (nxs)*(nys)*part_dens
	npB=ntot-part_dens*nint(pi*(radius)**2)*ndrops
    npR=ntot-npB
    kbT=0.05d0
    allocate(fxyR_SRD(2),fxyB_SRD(2))
    fxyR_SRD=0.0d0
    fxyB_SRD=0.0d0
    !
    write(6,*) '********************SRD DATA*****************'
    write(6,*) 'nsamp_output=', nsamp
    write(6,*) 'kbT=',kbt
    write(6,*) 'nbox=',nbox 
	write(6,*) 'ntot=',ntot 
	write(6,*) 'nparticlesR=',npR
    write(6,*) 'nparticlesB=',npB
    write(6,*) 'average number of ptcls per cell=',dfloat(npR+npB)/dfloat(nxs*nys)
	write(6,*) 'speed of sound=',sqrt((5.0d0/3.0d0)*kbT)
    write(6,*) 'k_int=',k_int
    write(6,*) 'k_rep=',k_rep
    write(6,*) 'fxR,fyR,fxB,fyB=',fxyR_SRD(1),fxyR_SRD(2),fxyB_SRD(1),fxyB_SRD(2)
    write(6,*) '*********************************************'
    endsubroutine
    !**********************************************************!
    subroutine init_srd
    
        implicit none
        
        integer :: counterR,counterB,ii,jj,countdrop
        real*8 :: temprnd1,temprnd2,vtotx,vtoty,sigma
		integer, allocatable :: xcenter(:),ycenter(:),mask(:,:)
		
        !! inizializzazione posizione particlelle
		allocate(xcenter(ndrops),ycenter(ndrops),mask(nxs,nys))
		xcenter(1)=radius+6
		ycenter(1)=radius+6
		
		do ii=2,ndrops
			xcenter(ii)=xcenter(ii-1)  + 2*radius+4
			ycenter(ii)=ycenter(ii-1)
			if(xcenter(ii).gt.nxs-(radius+6))then
				ycenter(ii)=ycenter(ii-1)+2*radius+4
				xcenter(ii)=xcenter(1)
			endif
			
		enddo
		!
		mask = 1
		do countdrop=1, ndrops
			do jj=-radius,radius
				do ii=-radius,radius
					if(ii**2 + jj**2.le.radius**2)then
						mask(ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 0
					endif
				enddo
			enddo
        enddo
		counterR=0
		counterB=0
		do while(counterR.lt.npR)
		do ii=1,nxs
			do jj=1,nys
				if(mask(ii,jj)==0)then
					counterR=counterR+1
					if(counterR.gt.npR)then
						exit
					else
						call RANDOM_NUMBER(temprnd1)
						call RANDOM_NUMBER(temprnd2)
						sxyR(1,counterR)=(ii-0.5d0) + temprnd1
						sxyR(2,counterR)=(jj-0.5d0) + temprnd2
					endif
				endif
				if(counterR.eq.npR) exit
			enddo
			if(counterR.eq.npR) exit
		enddo
		if(counterR.eq.npR) exit
		enddo
		!
		do while(counterB.lt.npB)
		do ii=1,nxs
			do jj=1,nys
				if(mask(ii,jj)==1)then
					counterB=counterB+1
					if(counterB.gt.npB)then
						exit
					else
						call RANDOM_NUMBER(temprnd1)
						call RANDOM_NUMBER(temprnd2)
						sxyB(1,counterB)=(ii-0.5d0) + temprnd1
						sxyB(2,counterB)=(jj-0.5d0) + temprnd2
					endif
				endif
				if(counterB.eq.npB) exit
			enddo
			if(counterB.eq.npB) exit
		enddo
		if(counterB.eq.npB) exit
		enddo

		
		write(6,*) "counterR", counterR, "counterB", counterB
		!
        continue
        !! inizializzazione velocit� particelle
        sigma=dsqrt(kbT) !dsqrt(kbt)
        mfp=deltt*kbt**0.5d0
        do ii=1,npR
            vxyR(1,ii)=normrand(0.0d0,sigma)
            vxyR(2,ii)=normrand(0.0d0,sigma)
        enddo
        !
        do ii=1,npB
            vxyB(1,ii)=normrand(0.0d0,sigma)
            vxyB(2,ii)=normrand(0.0d0,sigma)
        enddo
        vtotx=sum(vxyR(1,:))
        vtoty=sum(vxyR(2,:))
        vtotx=vtotx/dfloat(npR)
        vtoty=vtoty/dfloat(npR)
        vxyR(1,:)=vxyR(1,:)-vtotx
        vxyR(2,:)=vxyR(2,:)-vtoty
        !
        vtotx=sum(vxyB(1,:))
        vtoty=sum(vxyB(2,:))
        vtotx=vtotx/dfloat(npB)
        vtoty=vtoty/dfloat(npB)
        vxyB(1,:)=vxyB(1,:)-vtotx
        vxyB(2,:)=vxyB(2,:)-vtoty
        !temperature of the system
        temperatureR=(1.0d0/(2.0d0*dfloat(npR)))*sum(dsqrt(vxyR(1,:)**2 + vxyR(2,:)**2))
        !
        temperatureB=(1.0d0/(2.0d0*dfloat(npB)))*sum(dsqrt(vxyB(1,:)**2 + vxyB(2,:)**2))
        !write(6,*) 'temperature=',temperature
        mxy=0.0d0
		deallocate(xcenter,ycenter,mask)
    endsubroutine
    
    !**********************************************************!
    subroutine alloc_srd
        implicit none

        allocate(sxyR(2,1:npR),vxyR(2,1:npR),vxrandR(1:npR),vyrandR(1:npR))
        allocate(sxyB(2,1:npB),vxyB(2,1:npB),vxrandB(1:npB),vyrandB(1:npB))
        nsamp=500
		allocate(mxy(2,1:nxs,1:nys),vsrd(1:nxs,1:nys,1:nsamp),usrd(1:nxs,1:nys,1:nsamp)) !,dens_fluct(1:nbox))
        allocate(uavg(1:nxs,1:nys),vavg(1:nxs,1:nys),massboxR(1:nxs,1:nys),massboxB(1:nxs,1:nys))
         allocate(mRavg(1:nxs,1:nys),mRsrd(1:nxs,1:nys,1:nsamp),mBavg(1:nxs,1:nys),mBsrd(1:nxs,1:nys,1:nsamp))
        allocate(vtotrandx(1:nxs,1:nys),vtotrandy(1:nxs,1:nys))
        allocate(force(2,1:nxs,1:nys),deltaP(1:nxs,1:nys))
        allocate(forcerep(2,1:nxs,1:nys))
        allocate(qRx(1:nxs,1:nys),qRy(1:nxs,1:nys),qBx(1:nxs,1:nys),qBy(1:nxs,1:nys),qxy(2,1:nxs,1:nys))
        allocate(dtemp1b(1:npB,rnrg),dtemp2b(1:npB,rnrg),dtemp1a(1:npR,rnrg),dtemp2a(1:npR,rnrg))
        allocate(isfluid(1:nxs,1:nys),posR(2,npR),posB(2,npB))
		allocate(omegaxxR(1:nxs,1:nys),omegaxyR(1:nxs,1:nys),omegaxxB(1:nxs,1:nys),omegaxyB(1:nxs,1:nys))
		allocate(p(0:8),ex(0:8),ey(0:8),dex(0:8),dey(0:8))
		!
        isfluid=1
		ex=(/0,1,0,-1,0,1,-1,-1,1/)
        ey=(/0,0,1,0,-1,1,1,-1,-1/)
        dex=dfloat(ex)
        dey=dfloat(ey)
        p=(/4.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0/)
        usrd=0.0d0
        vsrd=0.0d0
        mRsrd=0.0d0
        mBsrd=0.0d0
    endsubroutine
    !**********************************************************!
    subroutine rng_gen
        
        implicit none
        integer ::  mynpR, mynpB, thId, myStart,myEnd
		!$omp single
        if(step.eq.0)then
            !thId = omp_get_thread_num()
            count_rand=0
			threads=1
           ! !$omp end master
            mynpR = npR / threads
            myStart = 1 !mynpR * thId + 1
            myEnd =   npR !myStart + mynpR -1
            call RANDOM_NUMBER(dtemp1a(myStart:myEnd,:))
            call RANDOM_NUMBER(dtemp2a(myStart:myEnd,:))
            mynpB = npB / threads
            myStart = 1 !mynpB * thId + 1
            myEnd =   npB !myStart + mynpB -1
            call RANDOM_NUMBER(dtemp1b(myStart:myEnd,:))
            call RANDOM_NUMBER(dtemp2b(myStart:myEnd,:))
        endif
        if(mod(step,rnrg).eq.0)then
            
            count_rand=0
        endif
        count_rand=count_rand+1
		!$omp end single
    endsubroutine
    !**********************************************************!
    subroutine outsrd
    
        implicit none
        
        integer :: ii,jj
        !$omp single
        if(mod(step,stamp).eq.0)then
            uavg(:,:)=sum(usrd,dim=3)
            vavg(:,:)=sum(vsrd,dim=3)
            mRavg(:,:)=sum(mRsrd,dim=3)
            mBavg(:,:)=sum(mBsrd,dim=3)
            uavg=uavg/dfloat(nsamp)
            vavg=vavg/dfloat(nsamp)
            mRavg=mRavg/dfloat(nsamp)
            mBavg=mBavg/dfloat(nsamp)
            ! open wrt files
            open(unit=238,file='mx'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=239,file='my'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=240,file='mR'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=241,file='mB'//write_fmtnumb(step/stamp)//'.out',status='replace')
            do jj=1,nys
                do ii=1,nxs
                    write(238,*) uavg(ii,jj)
                    write(239,*) vavg(ii,jj)
                    write(240,*) mRavg(ii,jj)
                    write(241,*) mBavg(ii,jj)
                enddo    
            enddo
            close(238)
            close(239)
            close(240)
            close(241)
            write(6,*) 'printing SRD macros at step', step !
        endif
        if(mod(step,stamp_part).eq.0)then
            open(unit=292,file='sxR'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=293,file='syR'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=295,file='sxB'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=296,file='syB'//write_fmtnumb(step/stamp)//'.out',status='replace')
            do ii=1,npR
                write(292,*) sxyR(1,ii)
                write(293,*) sxyR(2,ii)
            enddo
            do ii=1,npB
                write(295,*) sxyB(1,ii)
                write(296,*) sxyB(2,ii)
            enddo
            close(292)
            close(293)
            close(295)
            close(296)
            write(6,*) 'printing SRD particles at step', step
        endif
        !$omp end single
            
    endsubroutine
	
    endmodule
    
