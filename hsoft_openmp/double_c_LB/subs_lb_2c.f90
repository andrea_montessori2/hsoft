    module subs_lb_2c
    
    use vars_lb_2c
    
    implicit none
    
    contains
    
    !**********************************************************!
    subroutine moments_2c
        implicit none
        integer :: mm,i,j
        !$omp do private(j) 
        do j=1,ny
            do i=1,nx
               if(isfluid_LB(i,j).eq.1)then
                   rho_c(1,i,j)=0.0d0
                   rho_c(2,i,j)=0.0d0
                   uxy(1,i,j)=0.0d0
                   uxy(2,i,j)=0.0d0
                   do mm=0,8
                       rho_c(1,i,j)=rho_c(1,i,j) + g(1,mm,i,j) 
                       rho_c(2,i,j)=rho_c(2,i,j) + g(2,mm,i,j)
                       uxy(1,i,j)=uxy(1,i,j) + g(1,mm,i,j)*dex(mm) + g(2,mm,i,j)*dex(mm) 
                       uxy(2,i,j)=uxy(2,i,j) + g(1,mm,i,j)*dey(mm) + g(2,mm,i,j)*dey(mm) 
                   enddo
                   uxy(1,i,j)=uxy(1,i,j)/(rho_c(1,i,j)+rho_c(2,i,j))
                   uxy(2,i,j)=uxy(2,i,j)/(rho_c(1,i,j)+rho_c(2,i,j))
               endif
            enddo
        enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
    subroutine collision_2c
        implicit none
        integer :: mm,i,j
        real*8,dimension(2,0:8) :: feq
        real*8 :: uu,udotc,nu_avg
        !
        !$omp do private(j)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                    uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
                    nu_avg=1.0d0/( (rho_c(1,i,j)/(rho_c(1,i,j)+ rho_c(2,i,j)))*one_ov_nuR + & 
                           (rho_c(2,i,j)/(rho_c(1,i,j)+ rho_c(2,i,j)))*one_ov_nuB)
                    omega_c(i,j)=2.0d0/(6.0d0*nu_avg + 1.0d0)
                    do mm=0,8
                        udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
                        feq(1,mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                        feq(2,mm)=p(mm)*rho_c(2,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                        g(1,mm,i,j)=g(1,mm,i,j) + omega_c(i,j)*(feq(1,mm) - g(1,mm,i,j))  
                        g(2,mm,i,j)=g(2,mm,i,j) + omega_c(i,j)*(feq(2,mm) - g(2,mm,i,j))
                    enddo
                endif
            enddo
        enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
    subroutine surface_tension_2c
    
        implicit none
        integer :: i,j,mm
        real*8 :: acoeff,gradRx,gradRy,gradBx,gradBy,phis,psix,psiy,psinorm,psinorm_sq,ckl_RB,temp,cosphi
        real*8,dimension(0:8) :: rhosum,fdum,feq,e_dot_psi
        !
        !
        !$omp do private(j,gradRx,gradRy,gradBx,gradBy)
        do j=1,ny
            do i=1,nx
                phis=(rho_c(1,i,j) - rho_c(2,i,j))/(rho_c(1,i,j) + rho_c(2,i,j))
                if(isfluid_LB(i,j)==1 .and. dabs(phis).lt.0.995)then
                    acoeff=(9.0d0/2.0d0)*omega_c(i,j)*surf_tens 
					! if(isnan(acoeff)) then
					
					! write(6,*) omega_c(i,j),surf_tens,
					! pause	

					! endif					
                    do mm=0,8
                        rhosum(mm)=  rho_c(1,i+ex(mm),j+ey(mm)) + rho_c(2,i+ex(mm),j+ey(mm))    
                    enddo
                    !
                    gradRx=0.0d0
                    gradBx=0.0d0
                    !
                    gradRy=0.0d0
                    gradBy=0.0d0
                    do mm=1,8
                        gradRx=gradRx + (p(mm)/cssq)*dex(mm)*(rho_c(1,i+ex(mm),j+ey(mm))/rhosum(mm))
                        gradBx=gradBx + (p(mm)/cssq)*dex(mm)*(rho_c(2,i+ex(mm),j+ey(mm))/rhosum(mm))
                        !
                        gradRy=gradRy + (p(mm)/cssq)*dey(mm)*(rho_c(1,i+ex(mm),j+ey(mm))/rhosum(mm))
                        gradBy=gradBy + (p(mm)/cssq)*dey(mm)*(rho_c(2,i+ex(mm),j+ey(mm))/rhosum(mm))
                    enddo
                    !
                    psix= (rho_c(2,i,j)/rhosum(0))*gradRx - (rho_c(1,i,j)/rhosum(0))*gradBx !!!!*3
                    psiy= (rho_c(2,i,j)/rhosum(0))*gradRy - (rho_c(1,i,j)/rhosum(0))*gradBy
                    !
                    psinorm=dsqrt(psix**2 + psiy**2)
                    psinorm_sq=psinorm**2	
                    dnx(i,j)=-(psix/psinorm) !uscente dalla goccia
                !
                    dny(i,j)=-(psiy/psinorm)
                    do mm=0,8
                        fdum(mm)=g(1,mm,i,j) + g(2,mm,i,j) 
                    enddo
                    !ckl_RB=min(1.d0,1.0d6*rho_c(1,i,j)*rho_c(2,i,j))
                    do mm=0,8
                        e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy
                        temp=psinorm*(p(mm)*e_dot_psi(mm)**2/psinorm_sq - b_l(mm))
                        if(isnan(temp)) temp=0.0d0
                        !perturbation step on the blind ditros
                        fdum(mm)=fdum(mm) + acoeff*temp!*ckl_RB 
                    enddo
                    do mm=0,8
                        feq(mm)=(rho_c(1,i,j) + rho_c(2,i,j))*p(mm)
                    enddo 
                    do mm=0,8
                        temp=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm
                        cosphi=e_dot_psi(mm)/temp
                        if(isnan(cosphi).or.dabs(cosphi).gt.10.0d0 ) cosphi=0.0d0
						
                        !
                        temp=beta_st*rho_c(1,i,j)*rho_c(2,i,j)*cosphi/rhosum(0)**2
                        g(1,mm,i,j)=fdum(mm)*rho_c(1,i,j)/rhosum(0) + temp*feq(mm)
                        g(2,mm,i,j)=fdum(mm)*rho_c(2,i,j)/rhosum(0) - temp*feq(mm) 
                    enddo
                endif
            enddo
        enddo
        !$omp enddo
    endsubroutine
    !**********************************************************!
    subroutine surface_tension_cap_stress_2c
    
        implicit none
        integer :: i,j,mm
        real*8 :: acoeff,phis,psix,psiy,psinorm,psinorm_sq,ckl_RB,temp,cosphi
        real*8,dimension(0:8) :: rhosum,rhodiff,fdum,feq,e_dot_psi
        !
        !
        !$omp do private(j,psix,psiy)
        do j=1,ny
            do i=1,nx
                phis=(rho_c(1,i,j) - rho_c(2,i,j))/(rho_c(1,i,j) + rho_c(2,i,j))
                if(isfluid_LB(i,j)==1 .and. dabs(phis).lt.0.999d0)then
                    acoeff=(9.0d0/4.0d0)*omega_c(i,j)*surf_tens   
                    do mm=0,8
                        rhosum(mm)=  rho_c(1,i+ex(mm),j+ey(mm)) + rho_c(2,i+ex(mm),j+ey(mm)) 
						rhodiff(mm)= rho_c(1,i+ex(mm),j+ey(mm)) - rho_c(2,i+ex(mm),j+ey(mm)) 						
                    enddo
                    !
                    psix=0.0d0
					psiy=0.0d0
                    do mm=1,8
                        psix=psix + (p(mm)/cssq)*dex(mm)*(rhodiff(mm)/rhosum(mm))
                        psiy=psiy + (p(mm)/cssq)*dey(mm)*(rhodiff(mm)/rhosum(mm))
                    enddo
                    !
                    psinorm=dsqrt(psix**2 + psiy**2)
                    psinorm_sq=psinorm**2
                    
                    dnx(i,j)=-(psix/psinorm) !uscente dalla goccia
                !
                    dny(i,j)=-(psiy/psinorm)
                    do mm=0,8
                        fdum(mm)=g(1,mm,i,j) + g(2,mm,i,j) 
                    enddo
                    !ckl_RB=min(1.d0,1.0d6*rho_c(1,i,j)*rho_c(2,i,j))
                    do mm=0,8
                        e_dot_psi(mm)=dex(mm)*psix + dey(mm)*psiy
                        temp=psinorm*(p(mm)*e_dot_psi(mm)**2/psinorm_sq - b_l(mm))
                        if(isnan(temp)) temp=0.0d0
                        !perturbation step on the blind ditros
                        fdum(mm)=fdum(mm) + acoeff*temp !*ckl_RB 
                    enddo
                    do mm=0,8
                        feq(mm)=rho_c(1,i,j)*p(mm) + rho_c(2,i,j)*p(mm)
                    enddo 
                    do mm=0,8
                        temp=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm
                        cosphi=e_dot_psi(mm)/temp
                        if(isnan(cosphi)) cosphi=0.0d0
                        !
                        temp=beta_st*rho_c(1,i,j)*rho_c(2,i,j)*cosphi/rhosum(0)**2
                        g(1,mm,i,j)=fdum(mm)*rho_c(1,i,j)/rhosum(0) + temp*feq(mm)
                        g(2,mm,i,j)=fdum(mm)*rho_c(2,i,j)/rhosum(0) - temp*feq(mm) 
                    enddo
                endif
            enddo
        enddo
        !$omp enddo
    endsubroutine
    !**********************************************************!
    subroutine disjoining_force_EDM_2c
    
    implicit none
    
    integer:: i,j,l,llx,lly,inx,iny,mm
    real*8:: phis,phisn,dnorm,uu,uus,udotc,udotcs
	real*8,dimension(0:8) :: feq_R,feqs_R
    !$omp do private(j,force_disj_sc)
    do j=1,ny
        do i=1,nx
             phis=(rho_c(1,i,j) - rho_c(2,i,j)-rho_c(3,i,j)) &
                        /(rho_c(1,i,j) + rho_c(2,i,j)+rho_c(3,i,j))
			 dnorm=dsqrt(dnx(i,j)**2 + dny(i,j)**2)
             !if(isfluid_LB(i,j)==1 .and. dabs(phis).lt.0.1d0)then !sono interfaccia
			 if(isfluid_LB(i,j)==1 .and. (phis).ge.-0.2d0 .and. (phis).le.0.0d0)then !sono interfaccia 
                !
		        inx=nint(dnx(i,j))
		        iny=nint(dny(i,j))          
                !
		 inner: do ss=3,6 
					llx=i+ss*inx 
					lly=j+ss*iny
					if(llx.lt.1) llx=1
					if(llx.gt.nx) llx=nx
					if(lly.gt.ny) lly=ny
					if(lly.lt.1) lly=1
					!
					!
					phisn=(rho_c(1,llx,lly)-rho_c(2,llx,lly)) /(rho_c(1,llx,lly)+rho_c(2,llx,lly))
					if(phisn.gt.phis)then
						force_disj_sc(1)=A_rep*dnx(i,j)
						force_disj_sc(2)=A_rep*dny(i,j)
						
						uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
						!
						uus=0.5d0*((uxy(1,i,j) + force_disj_sc(1))*(uxy(1,i,j) + force_disj_sc(1)) + &
								  (uxy(2,i,j) + force_disj_sc(2))*(uxy(2,i,j) + force_disj_sc(2)))/cssq
						do mm=0,8
							udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
							udotcs=((uxy(1,i,j)+force_disj_sc(1))*dex(mm) + (uxy(2,i,j)+force_disj_sc(2))*dey(mm))/cssq
							feq_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							!
							feqs_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
							g(1,mm,i,j)=g(1,mm,i,j) + (feqs_R(mm) - feq_R(mm))
							g(1,mm,llx,lly)=g(1,mm,llx,lly) - (feqs_R(mm) - feq_R(mm))
						enddo
						exit inner
					endif
				enddo inner
             endif
        enddo
    enddo
    !$omp enddo 
    endsubroutine
    !**********************************************************!
    subroutine streaming_opt_2c
        implicit none
        
        integer:: i,j,l
!1st distro
        !$omp do private(j) 
        do j=0,ny+1
            do i=0,nx+1
                if(isfluid_LB(i,j).ne.3) then ! stream solo se non � gas n� solido interno
                    do l=0,8 
                        fb(l,i+ex(l),j+ey(l)) = g(1,l,i,j)
                    enddo
                endif
            enddo      
        enddo
        !$omp end do
        !$omp do private(j) 
        do j=0,ny+1
            do i=0,nx+1
                do l=0,8 
                    g(1,l,i,j) = fb(l,i,j)
                enddo
            enddo      
        enddo
        !$omp enddo
!2nd distro
        !$omp do private(j) 
        do j=0,ny+1
            do i=0,nx+1
                if(isfluid_LB(i,j).ne.3) then ! stream solo se non � gas n� solido interno
                    do l=0,8 
                        fb(l,i+ex(l),j+ey(l)) = g(2,l,i,j)
                    enddo
                endif
            enddo      
        enddo
        !$omp end do
        !$omp do private(j) 
        do j=0,ny+1
            do i=0,nx+1
                do l=0,8 
                    g(2,l,i,j) = fb(l,i,j)
                enddo
            enddo      
        enddo
        !$omp enddo

    endsubroutine
    
    !**********************************************************!
    subroutine all_periodic_bcs_2c
        implicit none
		real*8,dimension(0:8) :: fdum
		integer :: l,i,j
        !$omp single
        !west
         g(:,:,0,1:ny)=g(:,:,nx,1:ny)
        !east
         g(:,:,nx+1,1:ny)= g(:,:,1,1:ny)
        !north
         g(:,:,1:nx,ny+1)= g(:,:,1:nx,1)
        ! south
         g(:,:,1:nx,0)= g(:,:,1:nx,ny)
        !corners
         g(:,:,0,0)= g(:,:,nx,ny)
         g(:,:,nx+1,0)= g(:,:,1,ny)
         g(:,:,0,ny+1)= g(:,:,nx,1)
         g(:,:,nx+1,ny+1)= g(:,:,1,1)
         !**density boundary conds**!
         !west
         rho_c(:,0,1:ny)=rho_c(:,nx,1:ny)
        !east
         rho_c(:,nx+1,1:ny)= rho_c(:,1,1:ny)
        !north
         rho_c(:,1:nx,ny+1)= rho_c(:,1:nx,1)
        ! south
         rho_c(:,1:nx,0)= rho_c(:,1:nx,ny)
        !corners
         rho_c(:,0,0)= rho_c(:,nx,ny)
         rho_c(:,nx+1,0)= rho_c(:,1,ny)
         rho_c(:,0,ny+1)= rho_c(:,nx,1)
         rho_c(:,nx+1,ny+1)= rho_c(:,1,1)
		 do i=0,nx+1
            do j=0,ny+1
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(l) = g(1,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(1,l,i,j) = fdum(l)
                    enddo
                    !
                    do l=0,8
                        fdum(l) = g(2,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(2,l,i,j) = fdum(l)
                    enddo  
                endif
            enddo
        enddo
        !$omp end single
    endsubroutine    
    !**********************************************************!
    subroutine mixed_bcs_2c
        implicit none
        real*8,dimension(0:8) :: fdum
		real*8 :: u_avg_x
        integer :: i,l,j
        !$omp single
		!north and south
		do i=1,nx
            do j=1,ny
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(l) = g(1,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(1,l,i,j) = fdum(l)
                    enddo
                    !
                    do l=0,8
                        fdum(l) = g(2,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(2,l,i,j) = fdum(l)
                    enddo  
                endif
            enddo
        enddo
		!north
         g(:,:,1:nx,ny+1)= g(:,:,1:nx,1)
        ! south
         g(:,:,1:nx,0)= g(:,:,1:nx,ny)
        !north
         rho_c(:,1:nx,ny+1)= rho_c(:,1:nx,1)
        ! south
         rho_c(:,1:nx,0)= rho_c(:,1:nx,ny)
		 !************************************!
        !west: false periodic bcs
		rho_c(1,0,2:ny-1)=rho_c(1,shift,2:ny-1)
        rho_c(2,0,2:ny-1)=rho_c(2,shift,2:ny-1)
		do j=1,ny
			do l=0,8
				g(1,l,0,j)= g(1,l,shift,j)  
				g(2,l,0,j)= g(2,l,shift,j)  
			enddo
		enddo

        !east: fixed discharge / 2nd order
		rho_c(1,nx+1,:)=rho_c(1,nx,:)
        rho_c(2,nx+1,:)=rho_c(2,nx,:)
		!u_avg_x=dabs(sum(uxy(1,100,2:ny-1))/dfloat(ny))
		!if(mod(step,stamp).eq.0) write(6,*) u_avg_x
		!if(step.lt.20000) u_avg_x=0.0d0
         do j=1,ny
			!if(isfluid_LB(nx,j).eq.1)then
				do l=0,8
					fdum(l) = g(1,opp(l),nx+1,j) + rho_c(1,nx,j)*6.0d0*0.03d0*dex(l)*p(l)
					!fdum(l) = g(1,opp(l),nx+1,j) + rho_c(1,nx,j)*6.0d0*6300d0*(-0.5d0/0.167)*(5.3d-9)*dfloat(j)*(dfloat(j)-40.0d0)*dex(l)*p(l)
				enddo
				do l=0,8
					g(1,l,nx+1,j) =fdum(l) 
				enddo
				do l=0,8
					fdum(l) = g(2,opp(l),nx+1,j) + rho_c(2,nx,j)*6.0d0*0.03*dex(l)*p(l)
					!fdum(l) = g(2,opp(l),nx+1,j) + rho_c(2,nx,j)*6.0d0*6300d0*(-0.5d0/0.167)*(5.3d-9)*dfloat(j)*(dfloat(j)-40.0d0)*dex(l)*p(l)
				enddo
				do l=0,8
					g(2,l,nx+1,j) = fdum(l)
				enddo
			!endif
        enddo 
        !
        !$omp end single
    endsubroutine
    !**********************************************************!
    subroutine constant_force_2c
    implicit none
    integer :: mm,i,j
    !$omp do  private(j)
    do j=1,ny
        do i=1,nx
            if(isfluid_LB(i,j).eq.1)then
                do mm=0,8
                    g(1,mm,i,j)=g(1,mm,i,j) + (p(mm)/cssq)*(fx_LB_2c(1)*dex(mm) +fy_LB_2c(1)*dey(mm))*rho_c(1,i,j)
                    g(2,mm,i,j)=g(2,mm,i,j) + (p(mm)/cssq)*(fx_LB_2c(2)*dex(mm) +fy_LB_2c(2)*dey(mm))*rho_c(2,i,j)
                enddo
            endif
        enddo
    enddo
    !$omp end do 
    endsubroutine
    !**********************************************************!
	subroutine constant_force_EDM_2c
    implicit none
    integer :: mm,i,j
	real*8,dimension(0:8) ::feqs_R,feq_R,feqs_B,feq_B
	real*8 :: udotc,udotcs,uu,uus
    !$omp do  private(j)
    do j=1,ny
        do i=1,nx
            if(isfluid_LB(i,j).eq.1)then
				uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
				!
				uus=0.5d0*((uxy(1,i,j) + fx_LB_2c(1))*(uxy(1,i,j) + fx_LB_2c(1)) + &
						  (uxy(2,i,j) + fy_LB_2c(1))*(uxy(2,i,j) + fy_LB_2c(1)))/cssq
                do mm=0,8
					udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
					udotcs=((uxy(1,i,j)+fx_LB_2c(1))*dex(mm) + (uxy(2,i,j)+fy_LB_2c(1))*dey(mm))/cssq
                    feq_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
					feq_B(mm)=p(mm)*rho_c(2,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
					!
					feqs_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
					feqs_B(mm)=p(mm)*rho_c(2,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
                    g(1,mm,i,j)=g(1,mm,i,j) + (feqs_R(mm) - feq_R(mm))
                    g(2,mm,i,j)=g(2,mm,i,j) + (feqs_B(mm) - feq_B(mm))
                enddo
            endif
        enddo
    enddo
    !$omp end do 
    endsubroutine
	!**********************************************************!
    subroutine LB_sim_data_2c
    
    implicit none
    real*8 :: visc_LB1,visc_LB2
    
    nx=750
    ny=416
    tau1=0.57d0
    tau2=0.57d0
    cssq=1.0d0/3.0d0
    visc_LB1=cssq*(tau1-0.5d0)
    visc_LB2=cssq*(tau2-0.5d0)
    one_ov_nuR=1.0d0/visc_LB1
    one_ov_nuB=1.0d0/visc_LB2
    allocate(fx_LB_2c(2),fy_LB_2c(2))
    fx_LB_2c(2)=0.0d-6
    fy_LB_2c(2)=0.0d-6
    fx_LB_2c(1)=0.0d-6
    fy_LB_2c(1)=0.0d0
    beta_st=0.999d0
    surf_tens=0.02d0
    A_rep=-0.02d0
    continue
    write(6,*) '*******************LB data*****************'
    write(6,*) 'nx',nx
    write(6,*) 'ny',ny
    write(6,*) 'tau_1',tau1
    write(6,*) 'tau_2',tau2
    write(6,*) 'visc_1',visc_LB1
    write(6,*) 'visc_2',visc_LB2
    write(6,*) 'fx_LB_1',fx_LB_2c(1)
    write(6,*) 'fy_LB_1',fy_LB_2c(1)
    write(6,*) 'fx_LB_2',fx_LB_2c(2)
    write(6,*) 'fy_LB_2',fy_LB_2c(2)
    write(6,*) 'beta',beta_st
    write(6,*) 'surface tension',surf_tens
    write(6,*) '*******************************************'
    endsubroutine
    !**********************************************************!
    subroutine init_LB_2c
    
    implicit none
    integer:: l,rad,ndrops,countdrop,ii,jj
    integer, allocatable :: xcenter(:),ycenter(:)
    uxy=0.0d0
    rho_c(1,:,:)=0.0d0
    rho_c(2,:,:)=1.0d0
    rad=32
    ndrops=6
    !shift=nx
    allocate(xcenter(ndrops),ycenter(ndrops))
	
	xcenter(1)=rad+5
	ycenter(1)=rad+7 !ny/2
	do l=2,ndrops
		xcenter(l)=xcenter(1)
		ycenter(l)=ycenter(l-1)+2*rad+4
	enddo
	!	
    do countdrop=1, ndrops
			do jj=-rad,rad
				do ii=-rad,rad
					if(ii**2 + jj**2.le.rad**2)then
						rho_c(1,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 1.0d0
                        rho_c(2,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 0.0d0
					endif
				enddo
			enddo
    enddo
    !
    do jj=1,ny
        do ii=1,nx
            if(isfluid_LB(ii,jj)==0)then
                rho_c(2,ii,jj)=1.0d0 
                rho_c(1,ii,jj)=0.0d0
            endif
            if(isfluid_LB(ii,jj)==3)then
                rho_c(2,ii,jj)=-1.0d0
                rho_c(1,ii,jj)=0.0d0
            endif
        enddo
    enddo
    !
    do l=0,8
        g(1,l,0:nx+1,0:ny+1)=p(l)*rho_c(1,0:nx+1,0:ny+1)
        g(2,l,0:nx+1,0:ny+1)=p(l)*rho_c(2,0:nx+1,0:ny+1) ! set distribution to zero vel maxwell eq.
    enddo
    endsubroutine
    !**********************************************************!
    subroutine geometry_2c
        implicit none
       integer :: i,j,ll,ii,jj
        integer :: h_c,L_inl,L_chamber,L_dr_in,h_dr_in
		real*8 :: angle_inl
        ! generic geometry module
        isfluid_LB=1
		isfluid_LB(1:nx,1:ny)=3
		!
		shift=70			
		!
		h_c=45
		angle_inl=pi_greek/6.0d0
		L_inl=nint((dfloat((ny-h_c)/2))/dtan(angle_inl))
		L_chamber=nx-200-L_inl
		L_dr_in=1 !200
		h_dr_in=60
		write(6,*) L_inl,dtan(angle_inl),L_inl+L_chamber
		!
		do i=1,nx
			isfluid_LB(i,ny/2-h_c/2:ny/2+h_c/2)=1
			isfluid_LB(i,1)=0
			isfluid_LB(i,ny)=0
		enddo
		!
		do i=L_inl,1,-1
			isfluid_LB(i+L_chamber,ny/2+h_c/2:ny/2+h_c/2+nint(dfloat(L_inl-i)*dtan(angle_inl)))=1
			isfluid_LB(i+L_chamber,ny/2-h_c/2-nint(dfloat(L_inl-i)*dtan(angle_inl)):ny/2+h_c/2)=1
		enddo
		!
		! do i=1,L_dr_in/2
			! isfluid_LB(i,ny/2-h_dr_in/2:ny/2+h_dr_in/2)=1
		! enddo
		! do i=L_dr_in/2+1,L_dr_in
			! isfluid_LB(i,ny/2-h_c/2:ny/2+h_c/2)=1
		! enddo
		!
		do i=L_dr_in,L_chamber
			isfluid_LB(i,2:ny-1)=1 
		enddo
!
		!Per mettere 0 sulle bound solido-liquido
		do i=1,nx
           do j=1,ny
               if(isfluid_LB(i,j).eq.3)then
                   do ll=1,8
                       ii=i+ex(ll)
                       jj=j+ey(ll)
                       if(ii.gt.0 .and. ii.lt.nx+1 .and. jj.gt.0 .and. jj.lt.ny+1)then
                           if(isfluid_LB(ii,jj).eq.1)then
                               isfluid_LB(i,j)=0
                           endif
                       endif
                   enddo
               endif
           enddo
		enddo
        !
    endsubroutine
	!**********************************************************!
    subroutine outLB_2c
    
        implicit none
        
        integer :: ii,jj
		integer,save :: iframe=0
        ! open wrt files
        !$omp single
        if(mod(step,stamp).eq.0)then
			iframe=iframe+1
            open(unit=300,file='u'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=301,file='v'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=302,file='rhoA'//write_fmtnumb(iframe)//'.out',status='replace')
            open(unit=304,file='rhoB'//write_fmtnumb(iframe)//'.out',status='replace')
            do jj=1,ny
                do ii=1,nx
                    write(300,*) uxy(1,ii,jj)
                    write(301,*) uxy(2,ii,jj)
                    write(302,*) rho_c(1,ii,jj)
                    write(304,*) rho_c(2,ii,jj)
                enddo    
            enddo
            close(300)
            close(301)
            close(302)
            close(304)
            write(6,*) 'printing LB at step', step
        endif
        !$omp end single
    endsubroutine
     !****************************************************************!
     subroutine alloc_LB_2c
        
     implicit none
        allocate(p(0:8),ex(0:8),ey(0:8),dex(0:8),dey(0:8),opp(0:8))
        allocate(g(2,0:8,-1:nx+2,-1:ny+2),fb(0:8,-1:nx+2,-1:ny+2),uxy(2,1:nx,1:ny),rho_c(2,0:nx+1,0:ny+1))
        allocate(isfluid_LB(0:nx+1,0:ny+1),omega_c(1:nx,1:ny),b_l(0:8))
        allocate(dnx(1:nx,1:ny),dny(1:nx,1:ny))
        allocate(force_disj_sc(2))
        isfluid_LB=1
        g=0.0d0
        uxy=0.0d0
        rho_c=0.0d0
        omega_c=0.0d0
        b_l=(/-4.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,5.0d0/108.0d0,5.0d0/108.0d0,5.0d0/108.0d0,5.0d0/108.0d0/)
		ex=(/0,1,0,-1,0,1,-1,-1,1/)
        ey=(/0,0,1,0,-1,1,1,-1,-1/)
        dex=dfloat(ex)
        dey=dfloat(ey)
        p=(/4.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0/)
        opp=(/0,3,4,1,2,7,8,5,6/)
	 endsubroutine
     !****************************************************************!
    endmodule