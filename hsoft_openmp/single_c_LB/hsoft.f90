!  hsoft.f90 
!
!****************************************************************************
!
!  PROGRAM: hsoft.f90 in commit 
!
!****************************************************************************
    program hsoft
    
#ifdef _OPENMP
    use omp_lib
#endif    
    !use subs
    !use subs_lb
    !use subs_lb_2c
    use subs_lb_3c
    !use subs_coupling
    !use vars
    !use vars_lb
    !use vars_lb_2c
    use vars_lb_3c
	!use subs_lb_2c_3d
	!use vars_lb_2c_3d

	
    implicit none
    real*8 :: ts1,ts2
    integer,dimension(8) :: dt
    integer :: ii,jj,kk
    !
    !
    ! Variables
	!**************************
    step=0
	!**************************
	!**************************
	!**************************
	!**************************
    stamp=1000
    !stamp_part=50000
    !rnrg=100
    !count_samp=1
    ! Body of srd
    !call srd_sim_data
    continue
    call LB_sim_data_3c
    continue
    !call alloc_srd
    continue
	!!$omp parallel
    !call rng_gen
	!!$omp end parallel
   continue
    call alloc_LB_3c
    continue
    !call lattice_structure
    continue
	!call init_srd
    continue
    !call part_pos
    continue
    call geometry_3c
    continue
    call init_LB_3c
    continue
    call outLB_3c !_downsample
    continue
    !
    
    open(unit=307,file='isf.out',status='replace')
	!do kk=1,nz
		do jj=0,ny+1
			do ii=0,nx+1
				write(307,*) isfluid_LB(ii,jj) !,kk)
			enddo    
		enddo
	!enddo
    close(307)
    continue
    !*********************************************************************!
    !*************************hsoft main *********************************!
    !*********************************************************************!   
    !ts1 = omp_get_wtime();
    call date_and_time(values=dt)
    print '(i4, 5(a, i2.2))', dt(1), '/', dt(2), '/', dt(3), ' ', &
                              dt(5), ':', dt(6), ':', dt(7)
    
    step=1
    
    !$omp parallel

    !omp conditional compilation
#ifdef _OPENMP
   !$omp single
   threads=OMP_GET_NUM_THREADS()
   call OMP_SET_NUM_THREADS(THREADS)
   write(*,*)  'running parallel code on',threads,'processors', '/',omp_get_num_threads()
   !$omp end single
#else
   write(*,*)  'running sequential code'
#endif  
100 continue
    
    !do step=1,500
        !LB
         call moments_3c
         continue
		 !call regularized
		 continue
         call collision_3c !collision_w_lim_2c !_entropic
         continue
		 !call ehrenfest_2c
		 continue
         call surface_tension_cap_stress_3c
         continue
         call disjoining_force_EDM_3c
         continue
		 ! if(step .gt. 5000)then
			! call constant_force_3c
		 ! endif
         continue
         call  mixed_bcs_3c
         continue
         call streaming_opt_3c
         continue
         call outLB_3c !_downsample
        continue
        continue
    !enddo
    !$omp single
        step = step +1 
    !$omp end single
        
    if (step <=1000000) goto 100
    !$omp end parallel
    !*********************************************************************!
    !*********************************************************************!
    !*********************************************************************! 
   ! ts2 = omp_get_wtime();
    write(6,*) ts2-ts1
    call date_and_time(values=dt)
    print '(i4, 5(a, i2.2))', dt(1), '/', dt(2), '/', dt(3), ' ', &
                              dt(5), ':', dt(6), ':', dt(7)
    !call outLB
    !call outsrd
    end program hsoft
    
    

