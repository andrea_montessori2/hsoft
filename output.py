import matplotlib.pyplot as plt
import numpy as np

#postprocess to simulation data: multicomponent flows

import numpy as np
nx=600
ny=1600
with open('rhoA%06d.out' %tt, 'rb') as fid:
    rhoA = np.fromfile(fid, np.double).reshape((-1, 1)).T
    rhoA=np.reshape(rhoA,(ny,nx))
	
plt.imshow(rhoA,cmap ='hot')	
plt.draw()
print(rhoA.shape)


nx=600
ny=1600
nstep=3
phis=np.zeros((nx,ny))
for tt in range(1,nstep):
    with open('rhoA%06d.out' %tt, 'rb') as fid:
    rhoA = np.fromfile(fid, np.double).reshape((-1, 1)).T
    rhoA=np.reshape(rhoA,(ny,nx))
	with open('rhoB%06d.out' %tt, 'rb') as fid:
    rhoB = np.fromfile(fid, np.double).reshape((-1, 1)).T
    rhoB=np.reshape(rhoB,(ny,nx))
	with open('rhoC%06d.out' %tt, 'rb') as fid:
    rhoC = np.fromfile(fid, np.double).reshape((-1, 1)).T
    rhoC=np.reshape(rhoB,(ny,nx))
	with open('u%06d.out' %tt, 'rb') as fid:
    u = np.fromfile(fid, np.double).reshape((-1, 1)).T
    u=np.reshape(u,(ny,nx))
	with open('v%06d.out' %tt, 'rb') as fid:
    v = np.fromfile(fid, np.double).reshape((-1, 1)).T
    v=np.reshape(v,(ny,nx))
	
	
    phis=(rhoA-rhoB)/(rhoA+rhoB+rhoC)
    u = np.loadtxt('u%06d.out' %tt)
    u=np.reshape(u,(ny,nx))
    v = np.loadtxt('v%06d.out' %tt)
    v=np.reshape(v,(ny,nx))
    mu=np.sqrt(u**2 + v**2)
    plt.imshow(mu,cmap ='hot')
    #
    plt.contour(phis,levels=[0.01],colors='red',linewidths=2)
    plt.contour(rhoB,[-0.9],colors='red',linewidths=3)
    #plt.contour(rhoC,[-0.97,-0.96],['green'])
    plt.draw()
	
    plt.waitforbuttonpress()
    #plt.pause(0.01)
    plt.clf()
	

     





