    module vars_lb_2c
    
    implicit none 
    
	real*8,parameter :: pi_greek=3.1415926535897932384626433d0
    real*8, allocatable,dimension(:):: p,dex,dey
    integer,allocatable,dimension(:) :: ex,ey,opp
	real*8, allocatable,dimension(:,:,:,:) :: g 
    real*8, allocatable,dimension(:,:,:) :: rho_c,uxy,fb
    real*8, allocatable,dimension(:,:) :: omega_c,dnx,dny
    integer,allocatable,dimension(:,:) :: isfluid_LB
    real*8, allocatable,dimension(:) :: fx_LB_2c,fy_LB_2c,b_l,force_disj_sc
    real*8 :: tau1,tau2,one_ov_nuR,one_ov_nuB,beta_st,surf_tens,A_rep,cssq
	integer :: nx,ny,step,stamp,d_drop
	integer :: threads
    integer :: shift
    contains
	function dimenumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the number of digits
    !     of an integer number
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************

      implicit none

      integer,intent(in) :: inum
      integer :: dimenumb
      integer :: i
      real*8 :: tmp

      i=1
      tmp=real(inum,kind=8)
      do
      if(tmp< 10.d0 )exit
        i=i+1
        tmp=tmp/ 10.0d0
      enddo

      dimenumb=i

      return

     end function dimenumb

    function write_fmtnumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the string of six characters
    !     with integer digits and leading zeros to the left
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************
 
    implicit none

    integer,intent(in) :: inum
    character(len=6) :: write_fmtnumb
    integer :: numdigit,irest
    !real*8 :: tmp
    character(len=22) :: cnumberlabel
    numdigit=dimenumb(inum)
    irest=6-numdigit
    if(irest>0)then
        write(cnumberlabel,"(a,i8,a,i8,a)")"(a",irest,",i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)repeat('0',irest),inum
    else
        write(cnumberlabel,"(a,i8,a)")"(i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)inum
    endif
 
    return
    end function write_fmtnumb   
   
    endmodule
    
    
    