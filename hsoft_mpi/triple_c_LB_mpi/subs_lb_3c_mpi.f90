    module subs_lb_3c
    
    use vars_lb_3c
    implicit none
    
    include 'mpif.h'
    contains
    
    !**********************************************************!
    subroutine moments_3c
        implicit none 
        integer :: mm,i,j
		!
        do j=ny_in,ny_end
            do i=1,nx
               if(isfluid_LB(i,j).eq.1)then
                   rho_c(1,i,j)=0.0d0
                   rho_c(2,i,j)=0.0d0
				   rho_c(3,i,j)=0.0d0
                   uxy(1,i,j)=0.0d0
                   uxy(2,i,j)=0.0d0
                   do mm=0,8
                       rho_c(1,i,j)=rho_c(1,i,j) + g(1,mm,i,j)
                       rho_c(2,i,j)=rho_c(2,i,j) + g(2,mm,i,j)
                       rho_c(3,i,j)=rho_c(3,i,j) + g(3,mm,i,j)
                       uxy(1,i,j)=uxy(1,i,j) + g(1,mm,i,j)*dex(mm) + g(2,mm,i,j)*dex(mm) + g(3,mm,i,j)*dex(mm)
                       uxy(2,i,j)=uxy(2,i,j) + g(1,mm,i,j)*dey(mm) + g(2,mm,i,j)*dey(mm) + g(3,mm,i,j)*dey(mm) 
                   enddo
                   uxy(1,i,j)=uxy(1,i,j)/(rho_c(1,i,j)+rho_c(2,i,j)+rho_c(3,i,j))
                   uxy(2,i,j)=uxy(2,i,j)/(rho_c(1,i,j)+rho_c(2,i,j)+rho_c(3,i,j))
               endif
            enddo
        enddo
    endsubroutine
    !**********************************************************!
    subroutine collision_3c
        implicit none
        integer :: mm,i,j
        real*8,dimension(3,0:8) :: feq
        real*8 :: uu,udotc,nu_avg
		integer :: ierr
        !
        do j=ny_in,ny_end
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                    uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
                    nu_avg=1.0d0/( (rho_c(1,i,j)/(rho_c(1,i,j)+ rho_c(2,i,j)+ rho_c(3,i,j)))*one_ov_nuR + & 
                           (rho_c(2,i,j)/(rho_c(1,i,j)+ rho_c(2,i,j)+ rho_c(3,i,j)))*one_ov_nuB + &
						   (rho_c(3,i,j)/(rho_c(1,i,j)+ rho_c(2,i,j)+ rho_c(3,i,j)))*one_ov_nuG)
                    omega_c(i,j)=2.0d0/(6.0d0*nu_avg + 1.0d0)
                    do mm=0,8
                        udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
                        feq(1,mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                        feq(2,mm)=p(mm)*rho_c(2,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
						feq(3,mm)=p(mm)*rho_c(3,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                        g(1,mm,i,j)=g(1,mm,i,j) + omega_c(i,j)*(feq(1,mm) - g(1,mm,i,j))  
                        g(2,mm,i,j)=g(2,mm,i,j) + omega_c(i,j)*(feq(2,mm) - g(2,mm,i,j))
						g(3,mm,i,j)=g(3,mm,i,j) + omega_c(i,j)*(feq(3,mm) - g(3,mm,i,j))
                    enddo
                endif
            enddo
        enddo
        call MPI_barrier(MPI_COMM_WORLD,ierr)
    endsubroutine
    !**********************************************************!
    subroutine surface_tension_cap_stress_3c
    
        implicit none
        integer :: i,j,mm,kk
        real*8 :: acoeff_RB,acoeff_RG,acoeff_GB,cklGB,cklRB,cklRG,rhosumT
		real*8 :: psix_RB,psiy_RB,psinorm_RB,psinorm_RB_sq,temp_RB,cosphi_RB
		real*8 :: psix_RG,psiy_RG,psinorm_RG,psinorm_RG_sq,temp_RG,cosphi_RG
		real*8 :: psix_GB,psiy_GB,psinorm_GB,psinorm_GB_sq,temp_GB,cosphi_GB
        real*8,dimension(0:8) :: fdum,feq,e_dot_psi_RB,e_dot_psi_GB,e_dot_psi_RG
		real*8,dimension(0:8) :: rhosum_RB,rhodiff_RB,rhosum_RG,rhodiff_RG,rhosum_GB,rhodiff_GB
		INTEGER :: status(MPI_STATUS_SIZE)
		integer :: ierr
        ! 
        ! pass missing info to buffers
        !buffer filling rhoABC
		buf_send_up_rhoc(1:3,0:nx+1,1:6)=rho_c(1:3,0:nx+1,ny_end-5:ny_end)
		buf_send_down_rhoc(1:3,0:nx+1,1:6)=rho_c(1:3,0:nx+1,ny_in:ny_in+5)
        !sendrecv
        call mpi_sendrecv(buf_send_up_rhoc,6*3*(nx+2), MPI_DOUBLE_PRECISION, top, 123, buf_rcv_up_rhoc, 6*3*(nx+2), MPI_DOUBLE_PRECISION, bottom, 123,MPI_COMM_WORLD, status, ierr)
        call mpi_sendrecv(buf_send_down_rhoc,6*3*(nx+2), MPI_DOUBLE_PRECISION, bottom, 456, buf_rcv_down_rhoc, 6*3*(nx+2), MPI_DOUBLE_PRECISION, top, 456,MPI_COMM_WORLD, status, ierr)
		!
		!cpopy to buffer
        if(idrank.ne.0)then
            do i=0,nx+1
                do mm=1,3
                    rho_c(mm,i,ny_in-6:ny_in-1)=buf_rcv_up_rhoc(mm,i,1:6)
                enddo
            enddo
        endif
        !
        if(idrank.ne.nprocss-1)then
            do i=0,nx+1
                do mm=1,3
                    rho_c(mm,i,ny_end+1:ny_end+6)=buf_rcv_down_rhoc(mm,i,1:6)
                enddo
            enddo
        endif
        !
        do j=ny_in,ny_end
            do i=1,nx
				if(isfluid_LB(i,j)==1)then
					acoeff_RB=(9.0d0/4.0d0)*omega_c(i,j)*surf_tens_RB  
					acoeff_RG=(9.0d0/4.0d0)*omega_c(i,j)*surf_tens_RG
					acoeff_GB=(9.0d0/4.0d0)*omega_c(i,j)*surf_tens_GB
					rhosumT=rho_c(1,i,j)+rho_c(2,i,j)+rho_c(3,i,j)
					do mm=0,8
						rhosum_RB(mm)=  rho_c(1,i+ex(mm),j+ey(mm)) + rho_c(2,i+ex(mm),j+ey(mm)) 
						rhodiff_RB(mm)= rho_c(1,i+ex(mm),j+ey(mm)) - rho_c(2,i+ex(mm),j+ey(mm))
 						
						rhosum_GB(mm)=  rho_c(3,i+ex(mm),j+ey(mm)) + rho_c(2,i+ex(mm),j+ey(mm)) 
						rhodiff_GB(mm)= rho_c(2,i+ex(mm),j+ey(mm)) - rho_c(3,i+ex(mm),j+ey(mm))
						
						rhosum_RG(mm)=  rho_c(1,i+ex(mm),j+ey(mm)) + rho_c(3,i+ex(mm),j+ey(mm)) 
						rhodiff_RG(mm)= rho_c(1,i+ex(mm),j+ey(mm)) - rho_c(3,i+ex(mm),j+ey(mm))
					enddo
					!
					psix_RB=0.0d0
					psiy_RB=0.0d0
					!
					psix_GB=0.0d0
					psiy_GB=0.0d0
					!
					psix_RG=0.0d0
					psiy_RG=0.0d0
					do mm=1,8
						psix_RB=psix_RB + (p(mm)/cssq)*dex(mm)*(rhodiff_RB(mm)/rhosum_RB(mm))
						psiy_RB=psiy_RB + (p(mm)/cssq)*dey(mm)*(rhodiff_RB(mm)/rhosum_RB(mm))
						!
						psix_GB=psix_GB + (p(mm)/cssq)*dex(mm)*(rhodiff_GB(mm)/rhosum_GB(mm))
						psiy_GB=psiy_GB + (p(mm)/cssq)*dey(mm)*(rhodiff_GB(mm)/rhosum_GB(mm))
						!
						psix_RG=psix_RG + (p(mm)/cssq)*dex(mm)*(rhodiff_RG(mm)/rhosum_RG(mm))
						psiy_RG=psiy_RG + (p(mm)/cssq)*dey(mm)*(rhodiff_RG(mm)/rhosum_RG(mm))
					enddo
					!
					psinorm_RB=dsqrt(psix_RB**2 + psiy_RB**2)
					psinorm_RB_sq=psinorm_RB**2 
					!
					psinorm_GB=dsqrt(psix_GB**2 + psiy_GB**2)
					psinorm_GB_sq=psinorm_GB**2
					!
					!
					psinorm_RG=dsqrt(psix_RG**2 + psiy_RG**2)
					psinorm_RG_sq=psinorm_RG**2
					!
					dnx(i,j)=-(psix_RB/psinorm_RB) !uscente dalla goccia
					!
					dny(i,j)=-(psiy_RB/psinorm_RB)
					!if(isnan(dnx(i,j))) dnx(i,j)=0.0d0
					!if(isnan(dny(i,j))) dny(i,j)=0.0d0
					!
					do mm=0,8
						fdum(mm)=g(1,mm,i,j) + g(2,mm,i,j) + g(3,mm,i,j)  
					enddo
					!
					cklRB=min(1.0d6*rho_c(1,i,j)*rho_c(2,i,j),1.0d0)
					cklGB=min(1.0d6*rho_c(3,i,j)*rho_c(2,i,j),1.0d0)
					cklRG=min(1.0d6*rho_c(1,i,j)*rho_c(3,i,j),1.0d0)
					do mm=0,8
						e_dot_psi_RB(mm)=dex(mm)*psix_RB + dey(mm)*psiy_RB
						temp_RB=psinorm_RB*(p(mm)*e_dot_psi_RB(mm)**2/psinorm_RB_sq - b_l(mm))
						e_dot_psi_GB(mm)=dex(mm)*psix_GB + dey(mm)*psiy_GB
						temp_GB=psinorm_GB*(p(mm)*e_dot_psi_GB(mm)**2/psinorm_GB_sq - b_l(mm))
						e_dot_psi_RG(mm)=dex(mm)*psix_RG + dey(mm)*psiy_RG
						temp_RG=psinorm_RG*(p(mm)*e_dot_psi_RG(mm)**2/psinorm_RG_sq - b_l(mm))
						if(isnan(temp_RB)) temp_RB=0.0d0
						if(isnan(temp_GB)) temp_GB=0.0d0
						if(isnan(temp_RG)) temp_RG=0.0d0
						!perturbation step on the blind distros
						fdum(mm)=fdum(mm) + acoeff_RB*temp_RB*cklRB + acoeff_RG*temp_GB*cklGB &
											+acoeff_GB*temp_RG*cklRG
						!
					enddo
					
					do mm=0,8
						feq(mm)=(rho_c(1,i,j) + rho_c(2,i,j) +  rho_c(3,i,j))*p(mm) 
					enddo 
					do mm=0,8
						temp_RB=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm_RB
						cosphi_RB=e_dot_psi_RB(mm)/temp_RB
						if(isnan(cosphi_RB)) cosphi_RB=0.0d0
						!
						temp_GB=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm_GB
						cosphi_GB=e_dot_psi_GB(mm)/temp_GB
						if(isnan(cosphi_GB)) cosphi_GB=0.0d0
						!
						temp_RG=dsqrt(dex(mm)**2 + dey(mm)**2)*psinorm_RG
						cosphi_RG=e_dot_psi_RG(mm)/temp_RG
						if(isnan(cosphi_RG)) cosphi_RG=0.0d0
						!
						temp_RB=beta_st*rho_c(1,i,j)*rho_c(2,i,j)*cosphi_RB/rhosumT**2
						temp_GB=beta_st*rho_c(3,i,j)*rho_c(2,i,j)*cosphi_GB/rhosumT**2
						temp_RG=beta_st*rho_c(1,i,j)*rho_c(3,i,j)*cosphi_RG/rhosumT**2
						g(1,mm,i,j)=fdum(mm)*rho_c(1,i,j)/rhosumT + temp_RB*feq(mm) + temp_RG*feq(mm) 
						g(2,mm,i,j)=fdum(mm)*rho_c(2,i,j)/rhosumT - temp_RB*feq(mm) + temp_GB*feq(mm)
						g(3,mm,i,j)=fdum(mm)*rho_c(3,i,j)/rhosumT - temp_RG*feq(mm) - temp_GB*feq(mm)
					enddo
				endif
            enddo 
        enddo
        call MPI_barrier(MPI_COMM_WORLD,ierr)
    endsubroutine
    !**********************************************************!
	subroutine disjoining_force_EDM_3c
    
    implicit none
    
    integer:: i,j,l,llx,lly,inx,iny,mm,ss
    real*8:: phis,phisn,dnorm,uu,uus,udotc,udotcs
	INTEGER :: status(MPI_STATUS_SIZE)
	real*8,dimension(0:8) :: feq_R,feqs_R
	real*8,dimension(0:8,1:nx,1:6) :: buf_delta_rcv_down,buf_delta_rcv_up
	integer :: ierr
	!
	!sendreceive dei buffer di distribuzioni
	buf_send_up(1:3,0:8,0:nx+1,1:6)=g(1:3,0:8,0:nx+1,ny_end-5:ny_end)
	buf_send_down(1:3,0:8,0:nx+1,1:6)=g(1:3,0:8,0:nx+1,ny_in:ny_in+5)
	call mpi_sendrecv(buf_send_up, 6*3*9*(nx+2), MPI_double_precision, top, 123, buf_rcv_up, 6*3*9*(nx+2), MPI_double_precision, bottom, 123,MPI_COMM_WORLD, status, ierr)
	call mpi_sendrecv(buf_send_down, 6*3*9*(nx+2), MPI_double_precision, bottom, 456, buf_rcv_down, 6*3*9*(nx+2), MPI_double_precision, top, 456,MPI_COMM_WORLD, status, ierr)
	
	
	if(idrank.ne.0)then
		do i=0,nx+1
			do l=0,8
				g(1,l,i,ny_in-6:ny_in-1)=buf_rcv_up(1,l,i,1:6)
			enddo
		enddo
	endif
	!
	if(idrank.ne.nprocss-1)then
		do i=0,nx+1
			do l=0,8
				g(1,l,i,ny_end+1:ny_end+6)=buf_rcv_down(1,l,i,1:6)
			enddo
		enddo
	endif
	!
	delta_nci=0
	delta_nci_opp=0.0d0
    do j=ny_in,ny_end
        do i=1,nx
             phis=(rho_c(1,i,j) - rho_c(2,i,j)-rho_c(3,i,j)) &
                        /(rho_c(1,i,j) + rho_c(2,i,j)+rho_c(3,i,j))
			 dnorm=dsqrt(dnx(i,j)**2 + dny(i,j)**2)
             !if(isfluid_LB(i,j)==1 .and. dabs(phis).lt.0.1d0)then !sono interfaccia
			 if(isfluid_LB(i,j)==1 .and. (phis).ge.-0.20d0 .and. (phis).le.0.0d0)then !sono interfaccia 
                !
		        inx=nint(dnx(i,j))
		        iny=nint(dny(i,j))          
                !
		 inner: do ss=3,6 
					llx=i+ss*inx 
					lly=j+ss*iny
					if(llx.lt.1) llx=1
					if(llx.gt.nx) llx=nx
					if(idrank.eq.nprocss-1)then
						if(lly.gt.ny_end) lly=ny_end
					endif
					if(idrank.eq.0)then
				    	if(lly.lt.ny_in) lly=ny_in
					endif
					!
					!
					phisn=(rho_c(1,llx,lly)-rho_c(2,llx,lly)) /(rho_c(1,llx,lly)+rho_c(2,llx,lly))
					if(phisn.gt.phis)then
						force_disj_sc(1)=A_rep*dnx(i,j)
						force_disj_sc(2)=A_rep*dny(i,j)
						!
						uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
						!
						uus=0.5d0*((uxy(1,i,j) + force_disj_sc(1))*(uxy(1,i,j) + force_disj_sc(1)) + &
								  (uxy(2,i,j) + force_disj_sc(2))*(uxy(2,i,j) + force_disj_sc(2)))/cssq
						pos_nci(1,i,j)=ss*inx
						pos_nci(2,i,j)=ss*iny
						do mm=0,8
							udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
							udotcs=((uxy(1,i,j)+force_disj_sc(1))*dex(mm) + (uxy(2,i,j)+force_disj_sc(2))*dey(mm))/cssq
							feq_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
							!
							feqs_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
							!
							g(1,mm,i,j)=g(1,mm,i,j) + (feqs_R(mm) - feq_R(mm))
							if(lly.le.ny_end .and. lly.ge.ny_in)then
								g(1,mm,llx,lly)=g(1,mm,llx,lly) - (feqs_R(mm) - feq_R(mm))
							else
								delta_nci_opp(mm,llx,lly)=delta_nci_opp(mm,llx,lly) -(feqs_R(mm) - feq_R(mm)) ! reazione che devo mandare al rank sopra o sotto 
							endif
						enddo 
						exit inner
					endif
				enddo inner 
             endif
        enddo
    enddo
	!
	call mpi_sendrecv(delta_nci_opp(0:8,1:nx,ny_end+1:ny_end+6), 6*9*(nx), MPI_double_precision, top, 2344, buf_delta_rcv_up, 6*9*(nx), MPI_double_precision, bottom, 2344,MPI_COMM_WORLD, status, ierr)
    call mpi_sendrecv(delta_nci_opp(0:8,1:nx,ny_in-6:ny_end-1), 6*9*(nx), MPI_double_precision, bottom, 4445, buf_delta_rcv_down, 6*9*(nx), MPI_double_precision, top, 4445,MPI_COMM_WORLD, status, ierr)
	!
	if(idrank.ne.0)then
		do i=1,nx
			do l=0,8
				g(1,l,i,ny_in:ny_in+5)=g(1,l,i,ny_in:ny_in+5) + buf_delta_rcv_up(l,i,1:6)
			enddo
		enddo
	endif
	!
	if(idrank.ne.nprocss-1)then
		do i=1,nx
			do l=0,8
				g(1,l,i,ny_end-5:ny_end)=g(1,l,i,ny_end-5:ny_end) + buf_delta_rcv_down(l,i,1:6)
			enddo
		enddo
	endif
	call MPI_barrier(MPI_COMM_WORLD,ierr)
    endsubroutine
    !**********************************************************!
	subroutine streaming_opt_3c
        implicit none
        
        integer:: i,j,l
		INTEGER :: status(MPI_STATUS_SIZE)
		integer :: ierr
        ! pass missing infos to buffer
		buf_send_up(1:3,0:8,0:nx+1,1:6)=g(1:3,0:8,0:nx+1,ny_end-5:ny_end)
		buf_send_down(1:3,0:8,0:nx+1,1:6)=g(1:3,0:8,0:nx+1,ny_in:ny_in+5)
		!
        call mpi_sendrecv(buf_send_up,6*3*9*(nx+2), MPI_double_precision, top, 334, buf_rcv_up, 6*3*9*(nx+2), MPI_double_precision, bottom, 334,MPI_COMM_WORLD, status, ierr)
        call mpi_sendrecv(buf_send_down,6*3*9*(nx+2), MPI_double_precision, bottom, 564, buf_rcv_down, 6*3*9*(nx+2), MPI_double_precision, top, 564,MPI_COMM_WORLD, status, ierr)
		!
		if(idrank.ne.0)then
            do i=0,nx+1
                do l=0,8
                    g(1,l,i,ny_in-6:ny_in-1)=buf_rcv_up(1,l,i,1:6)
                    g(2,l,i,ny_in-6:ny_in-1)=buf_rcv_up(2,l,i,1:6)
                    g(3,l,i,ny_in-6:ny_in-1)=buf_rcv_up(3,l,i,1:6)
                enddo
            enddo
        endif
        !
        if(idrank.ne.nprocss-1)then
            do i=0,nx+1
                do l=0,8
                    g(1,l,i,ny_end+1:ny_end+6)=buf_rcv_down(1,l,i,1:6)
                    g(2,l,i,ny_end+1:ny_end+6)=buf_rcv_down(2,l,i,1:6)
                    g(3,l,i,ny_end+1:ny_end+6)=buf_rcv_down(3,l,i,1:6)
                enddo
            enddo
        endif
! distro streaming
        do j=ny_in-1,ny_end+1
            do i=0,nx+1
                if((i.ge.1 .and. i.le.nx) .and. (j.ge.ny_in .and. j.le.ny_end))then ! stream solo se non � gas n� solido interno
                    if(isfluid_LB(i,j).ne.3)then
						do l=0,8 
							fb(1,l,i+ex(l),j+ey(l)) = g(1,l,i,j)
							fb(2,l,i+ex(l),j+ey(l)) = g(2,l,i,j)
							fb(3,l,i+ex(l),j+ey(l)) = g(3,l,i,j)
						enddo
					endif
                else
                    do l=0,8 
                        fb(1,l,i+ex(l),j+ey(l)) = g(1,l,i,j)
                        fb(2,l,i+ex(l),j+ey(l)) = g(2,l,i,j)
                        fb(3,l,i+ex(l),j+ey(l)) = g(3,l,i,j)
                    enddo
                endif
            enddo      
        enddo
        do j=ny_in-1,ny_end+1
            do i=0,nx+1
                do l=0,8 
                    g(1,l,i,j) = fb(1,l,i,j)
                    g(2,l,i,j) = fb(2,l,i,j)
                    g(3,l,i,j) = fb(3,l,i,j)
                enddo
            enddo      
        enddo     		
    endsubroutine
    !**********************************************************!
	subroutine EDM_force_3c
	
        implicit none
		
		integer :: i,j,mm
		real*8 :: uu,uus,udotc,udotcs
		real*8, dimension(0:8) :: feq_R,feqs_R,feq_B,feqs_B,feq_G,feqs_G
		do j=ny_in,ny_end
			do i=1,nx
				uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
						!
				uus=0.5d0*((uxy(1,i,j) + fx_LB_2c(1))*(uxy(1,i,j) + fx_LB_2c(1)) + &
						  (uxy(2,i,j) + fy_LB_2c(1))*(uxy(2,i,j) + fy_LB_2c(1)))/cssq
				do mm=0,8
					udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
					udotcs=((uxy(1,i,j)+fx_LB_2c(1))*dex(mm) + (uxy(2,i,j)+fy_LB_2c(1))*dey(mm))/cssq
					feq_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
					!
					feqs_R(mm)=p(mm)*rho_c(1,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
					!
					!
					feq_B(mm)=p(mm)*rho_c(2,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
					!
					feqs_B(mm)=p(mm)*rho_c(2,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
					!
					!
					feq_G(mm)=p(mm)*rho_c(3,i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
					!
					feqs_G(mm)=p(mm)*rho_c(3,i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
					!
					!
					g(1,mm,i,j)=g(1,mm,i,j) + (feqs_R(mm) - feq_R(mm))
					g(2,mm,i,j)=g(2,mm,i,j) + (feqs_B(mm) - feq_B(mm))
					g(3,mm,i,j)=g(3,mm,i,j) + (feqs_G(mm) - feq_G(mm))
				enddo
			enddo
		enddo
		
	endsubroutine	
	!**********************************************************!
    subroutine all_periodic_bcs_3c
        implicit none
		real*8,dimension(3,0:8) :: fdum
		INTEGER :: status(MPI_STATUS_SIZE)
		integer :: ierr
		integer :: l,i,j
		!
		buf_s_to_n(1:3,0:8,0:nx+1)=g(1:3,0:8,0:nx+1,ny_in)
		buf_n_to_s(1:3,0:8,0:nx+1)=g(1:3,0:8,0:nx+1,ny_end)
		buf_rho_s_to_n(1:3,0:nx+1)=rho_c(1:3,0:nx+1,ny_in)
		buf_rho_n_to_s(1:3,0:nx+1)=rho_c(1:3,0:nx+1,ny_end)
		
	    call MPI_BCAST(buf_s_to_n, 3*9*(nx+2), MPI_double_precision, 0, MPI_COMM_WORLD, ierr)
		call MPI_BCAST(buf_n_to_s, 3*9*(nx+2), MPI_double_precision, nprocss-1, MPI_COMM_WORLD, ierr)
		call MPI_BCAST(buf_rho_s_to_n, 3*(nx+2), MPI_double_precision, 0, MPI_COMM_WORLD, ierr)
		call MPI_BCAST(buf_rho_n_to_s, 3*(nx+2), MPI_double_precision, nprocss-1, MPI_COMM_WORLD, ierr)
        !
		do j=ny_in,ny_end
            do i=1,nx
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(1,l) = g(1,opp(l),i,j)
						fdum(2,l) = g(2,opp(l),i,j)
						fdum(3,l) = g(3,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(1,l,i,j) = fdum(1,l)
						g(2,l,i,j) = fdum(2,l)
						g(3,l,i,j) = fdum(3,l)
                    enddo
                endif
            enddo
        enddo
		!
		if(idrank.eq.0)then
			do i=1,nx
				do l=0,8
					g(1:3,l,i,ny_in-1)=buf_s_to_n(1:3,l,i)
				enddo
			enddo
		elseif(idrank.eq.nprocss-1)then
			do i=1,nx
				do l=0,8
					g(1:3,l,i,ny_end+1)=buf_n_to_s(1:3,l,i)
				enddo
			enddo
		endif
        !
        do j=ny_in,ny_end
            do l=0,8
                g(1:3,l,0,j)=g(1:3,l,nx,j)
                g(1:3,l,nx+1,j)=g(1:3,l,1,j)
            enddo
        enddo
        !
        if(idrank.eq.0)then
            do l=0,8
                g(1:3,l,0,ny_in-1)=g(1:3,l,0,ny_in)
                g(1:3,l,nx+1,ny_in-1)=g(1:3,l,nx+1,ny_in)
            enddo
        endif
        if(idrank.eq.nprocss-1)then
            do l=0,8
                g(1:3,l,0,ny_in+nyrank)=g(1:3,l,0,ny_in+nyrank-1)
                g(1:3,l,nx+1,ny_in+nyrank)=g(1:3,l,nx+1,ny_in+nyrank-1)
            enddo
        endif
        ! 
		if(idrank.eq.0)then
			do i=1,nx
                rho_c(1:3,i,ny_in-1)=buf_rho_s_to_n(1:3,i)
			enddo
		elseif(idrank.eq.nprocss-1)then
			do i=1,nx
                rho_c(1:3,i,ny_end+1)=buf_rho_n_to_s(1:3,i)
			enddo
		endif
        ! !east and west
        do j=ny_in,ny_end
            rho_c(1:3,0,j)=rho_c(1:3,nx,j)
            rho_c(1:3,nx+1,j)=rho_c(1:3,1,j)
        enddo
        ! !corners nx+1,0 :: 0,0 :: nx+1,ny+1 :: 0, ny+1
        if(idrank.eq.0)then
            rho_c(1:3,0,ny_in-1)=rho_c(1:3,0,ny_in)
            rho_c(1:3,nx+1,ny_in-1)=rho_c(1:3,nx+1,ny_in)
        endif
        if(idrank.eq.nprocss-1)then
            rho_c(1:3,0,ny_in+nyrank)=rho_c(1:3,0,ny_in+nyrank-1)
            rho_c(1:3,nx+1,ny_in+nyrank)=rho_c(1:3,nx+1,ny_in+nyrank-1)
        endif
        call MPI_Barrier(MPI_COMM_WORLD,ierr)
    endsubroutine    
    !**********************************************************!
    subroutine all_bb_bcs_3c
        implicit none
		real*8,dimension(3,0:8) :: fdum
		integer :: l,i,j
		integer :: ierr
	    !
		do j=ny_in,ny_end
            do i=1,nx
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(1,l) = g(1,opp(l),i,j)
						fdum(2,l) = g(2,opp(l),i,j)
						fdum(3,l) = g(3,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(1,l,i,j) = fdum(1,l)
						g(2,l,i,j) = fdum(2,l)
						g(3,l,i,j) = fdum(3,l)
                    enddo
                endif
            enddo
        enddo
        !
        call MPI_Barrier(MPI_COMM_WORLD,ierr)
    endsubroutine    
	!**********************************************************!
    subroutine mixed_bcs_3c
        implicit none
        real*8,dimension(1:3,0:8) :: fdum
		real*8 :: u_boundary
        integer :: i,l,j
		integer :: ierr
		!solid bcs: no slip
		do j=ny_in,ny_end
            do i=1,nx
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(1,l) = g(1,opp(l),i,j)
						fdum(2,l) = g(2,opp(l),i,j)
						fdum(3,l) = g(3,opp(l),i,j)
                    enddo
                    do l=0,8
                        g(1,l,i,j) = fdum(1,l)
						g(2,l,i,j) = fdum(2,l)
						g(3,l,i,j) = fdum(3,l)
                    enddo
                endif
            enddo
        enddo
		!
		if(step.le.5000) then
			u_boundary=0.0d0
		else
			u_boundary=0.01d0
		endif
		!	
		 if(idrank.eq.0)then
			 do i=1,nx
				do l=0,8
					fdum(1,l) = g(1,opp(l),i,ny_in-1) + rho_c(1,i,ny_in)*6.0d0*u_boundary*dey(l)*p(l)
					fdum(2,l) = g(2,opp(l),i,ny_in-1) + rho_c(2,i,ny_in)*6.0d0*u_boundary*dey(l)*p(l)
					fdum(3,l) = g(3,opp(l),i,ny_in-1) + rho_c(3,i,ny_in)*6.0d0*u_boundary*dey(l)*p(l)
				enddo
				do l=0,8
					g(1,l,i,ny_in-1) =fdum(1,l) 
					g(2,l,i,ny_in-1) =fdum(2,l) 
					g(3,l,i,ny_in-1) =fdum(3,l) 
				enddo
			enddo 
			!
			rho_c(1:3,0:nx+1,ny_in-1)=rho_c(1:3,0:nx+1,ny_in)
		elseif(idrank.eq.nprocss-1)then
			do i=1,nx
				do l=0,8
					g(1,l,i,ny_end+1)=g(1,l,i,ny_end)
					g(2,l,i,ny_end+1)=g(2,l,i,ny_end)
					g(3,l,i,ny_end+1)=g(3,l,i,ny_end)
				enddo
			enddo 
			rho_c(1:3,0:nx+1,ny_end+1)=rho_c(1:3,0:nx+1,ny_end)
		endif
		call MPI_Barrier(MPI_COMM_WORLD,ierr)
    endsubroutine 
    !**********************************************************!
    subroutine LB_sim_data_3c
    
    implicit none
    real*8 :: visc_LB1,visc_LB2,visc_LB3
	integer :: ierr
        nx=600
        ny=1600
        tau1=0.65d0
        tau2=1.0d0
	    tau3=1.0d0
        cssq=1.0d0/3.0d0
        visc_LB1=cssq*(tau1-0.5d0)
        visc_LB2=cssq*(tau2-0.5d0)
        visc_LB3=cssq*(tau3-0.5d0)
        one_ov_nuR=1.0d0/visc_LB1
        one_ov_nuB=1.0d0/visc_LB2
        one_ov_nuG=1.0d0/visc_LB3
        allocate(fx_LB_2c(3),fy_LB_2c(3))
        fx_LB_2c(3)=0.0d-6
        fy_LB_2c(3)=0.0d-6
	    fx_LB_2c(2)=0.0d-6
        fy_LB_2c(2)=0.0d-6
        fx_LB_2c(1)=0.0d-5
        fy_LB_2c(1)=0.0d-5
        beta_st=0.999d0
        surf_tens_RB=0.02d0
	    surf_tens_GB=0.05d0
	    surf_tens_RG=0.01d0
        A_rep=-0.06d0
        continue
        call MPI_barrier(MPI_COMM_WORLD,ierr)
    if(idrank.eq.0)then
        write(6,*) '*******************LB data*****************'
        write(6,*) 'nx',nx
        write(6,*) 'ny',ny
        write(6,*) 'tau_1',tau1
        write(6,*) 'tau_2',tau2 
	    write(6,*) 'tau_3',tau3
        write(6,*) 'visc_1',visc_LB1
        write(6,*) 'visc_2',visc_LB2
        write(6,*) 'visc_3',visc_LB3
        write(6,*) 'fx_LB_1',fx_LB_2c(1)
        write(6,*) 'fy_LB_1',fy_LB_2c(1)
        write(6,*) 'fx_LB_2',fx_LB_2c(2)
        write(6,*) 'fy_LB_2',fy_LB_2c(2)
	    write(6,*) 'fx_LB_3',fx_LB_2c(3)
        write(6,*) 'fy_LB_3',fy_LB_2c(3)
        write(6,*) 'beta',beta_st
        write(6,*) 'surface tension RB',surf_tens_RB
	    write(6,*) 'surface tension GB',surf_tens_GB
	    write(6,*) 'surface tension RG',surf_tens_RG
    write(6,*) '*******************************************'
    endif
    endsubroutine 
    !**********************************************************!
    subroutine init_LB_3c
    
    implicit none
    integer:: l,rad,rad_in,ndrops,countdrop,ii,jj,le_1,ndrops_in,xcc
    integer, allocatable :: xcenter(:),ycenter(:)
	INTEGER :: status(MPI_STATUS_SIZE)
	integer :: ierr
	!thread per thread
    do jj=ny_in,ny_end
        do ii=1,nx
            uxy(1,ii,jj)=0.0d0
            uxy(2,ii,jj)=0.0d0
        enddo
    enddo
    ! thread per thread
    do jj=ny_in-1,ny_end+1
        do ii=1,nx
            rho_c(1,ii,jj)=0.0d0
            rho_c(2,ii,jj)=0.0d0 
	        rho_c(3,ii,jj)=1.0d0 
        enddo
    enddo
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    ! solo su root
    if(idrank.eq.0)then
        rho_c_gathered(1,1:nx,1:ny)=0.0d0
        rho_c_gathered(2,1:nx,1:ny)=0.0d0 
	    rho_c_gathered(3,1:nx,1:ny)=1.0d0
        rad=170
	    rad_in=d_droplets/2
        ndrops=1
	    ndrops_in=49
	    le_1=7
	    xcc=300
        !shift=nx
        allocate(xcenter(ndrops),ycenter(ndrops))
	    xcenter(1)=nx/2  !nx/2 !nx/3
	    ycenter(1)=xcc
	    !	
        do countdrop=1, ndrops
		    do jj=-rad,rad
			    do ii=-rad,rad
				    if(ii<=rad .or.ii>=-rad .and. jj<=rad .or. jj>=-rad )then
					    rho_c_gathered(1,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 0.0d0
					    rho_c_gathered(2,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 1.0d0
					    rho_c_gathered(3,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 0.0d0
				    endif
			    enddo
		    enddo
        enddo
	    deallocate(xcenter,ycenter)
	    !
	    allocate(xcenter(ndrops_in),ycenter(ndrops_in))
	    !xcenter(1)=nx/2 - rad + rad_in + le_1
	    !ycenter(1)=ny/2 - rad + rad_in + le_1
	    xcenter(1)=nx/2 - rad + rad_in + le_1 ! xcc - rad + rad_in + le_1
	    ycenter(1)= xcc - rad + rad_in + le_1 !100 !ny/2 - rad + rad_in + le_1
	    do countdrop=2, ndrops_in
		    xcenter(countdrop)=xcenter(countdrop-1) + 2*rad_in+ le_1
		    ycenter(countdrop)=ycenter(countdrop-1) 
		    !if(xcenter(countdrop)>nx/2+ rad-le_1-rad_in)then
		    if(xcenter(countdrop)>nx/2 + rad-le_1-rad_in)then
			    ycenter(countdrop)=ycenter(countdrop-1) + 2*rad_in+le_1
			    xcenter(countdrop)=xcenter(1) 
		    endif
	    enddo
	    !	
        do countdrop=1, ndrops_in
			do jj=-rad_in,rad_in
				do ii=-rad_in,rad_in
					if(ii<=rad_in .or.ii>=-rad_in .and. jj<=rad_in .or. jj>=-rad_in)then
						rho_c_gathered(1,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 1.0d0
                        rho_c_gathered(2,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 0.0d0
						rho_c_gathered(3,ii + xcenter(countdrop),jj + ycenter(countdrop) ) = 0.0d0
					endif
				enddo
			enddo
        enddo
	    deallocate(xcenter,ycenter)
        !
        do jj=1,ny
            do ii=1,nx
                if(isfluid_LB_gathered(ii,jj)==0)then
                    rho_c_gathered(3,ii,jj)=1.0d0 
                    rho_c_gathered(1,ii,jj)=0.0d0
                    rho_c_gathered(2,ii,jj)=0.0d0
                endif
                if(isfluid_LB_gathered(ii,jj)==3)then
                    rho_c_gathered(3,ii,jj)=-1.0d0
                    rho_c_gathered(1,ii,jj)=0.0d0
				    rho_c_gathered(2,ii,jj)=0.0d0
                endif
            enddo
        enddo
    endif
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    !scatter to procs
    CALL MPI_SCATTER(rho_c_gathered(:,1:nx,1:ny), 3*nx*(ny_end-ny_in+1), MPI_DOUBLE_PRECISION, rho_c(:,1:nx,ny_in:ny_end), 3*nx*(ny_end-ny_in+1), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    ! pass missing info to buffers
    !buffer filling rhoABC
    buf_send_up_rhoc(1:3,0:nx+1,1:6)=rho_c(1:3,0:nx+1,ny_end-5:ny_end)
    buf_send_down_rhoc(1:3,0:nx+1,1:6)=rho_c(1:3,0:nx+1,ny_in:ny_in+5)
    !sendrecv
    call mpi_sendrecv(buf_send_up_rhoc, 6*3*(nx+2), MPI_DOUBLE_PRECISION, top, 123, buf_rcv_up_rhoc, 6*3*(nx+2), MPI_DOUBLE_PRECISION, bottom, 123,MPI_COMM_WORLD, status, ierr)
	call mpi_sendrecv(buf_send_down_rhoc, 6*3*(nx+2), MPI_DOUBLE_PRECISION, bottom, 456, buf_rcv_down_rhoc, 6*3*(nx+2), MPI_DOUBLE_PRECISION, top, 456,MPI_COMM_WORLD, status, ierr)
	!copy to buffer
	if(idrank.ne.0)then
        do ii=0,nx+1
            do l=1,3
                rho_c(l,ii,ny_in-6:ny_in-1)=buf_rcv_up_rhoc(l,ii,1:6)
            enddo
        enddo
     endif
     !
    if(idrank.ne.nprocss-1)then
        do ii=0,nx+1
            do l=1,3
                rho_c(l,ii,ny_end+1:ny_end+6)=buf_rcv_down_rhoc(l,ii,1:6)
            enddo
        enddo
    endif
    !
    call MPI_barrier(MPI_COMM_WORLD,ierr)
    do l=0,8
        g(1,l,0:nx+1,ny_in-1:ny_end+1)=p(l)*rho_c(1,0:nx+1,ny_in-1:ny_end+1)
        g(2,l,0:nx+1,ny_in-1:ny_end+1)=p(l)*rho_c(2,0:nx+1,ny_in-1:ny_end+1) ! set distribution to zero vel maxwell eq.
		g(3,l,0:nx+1,ny_in-1:ny_end+1)=p(l)*rho_c(3,0:nx+1,ny_in-1:ny_end+1)
    enddo
    endsubroutine 
    !**********************************************************!
    subroutine geometry_3c
        implicit none
        integer :: i,j,ll,ii,jj
	    integer :: h_c,L_inl,L_chamber,L_c,root
		INTEGER :: status(MPI_STATUS_SIZE)
		real*8 :: angle_inl
		integer :: ierr
		! gather data in 0
		!
		d_droplets=40
        if(idrank.eq.0)then
            ! generic geometry module
            isfluid_LB_gathered=1
			isfluid_LB_gathered(:,1)=1
			isfluid_LB_gathered(:,ny)=1
			isfluid_LB_gathered(1,:)=0
			isfluid_LB_gathered(nx,:)=0
		    !isfluid_LB_gathered(1:nx,1:ny)=3
		    !!
		    h_c=nint(d_droplets*3.0d0)
		    L_c=nint(d_droplets*4.0d0)
		    !angle_inl=pi_greek/6.0d0
		    !L_inl=nint((dfloat((ny-h_c)/2))/dtan(angle_inl))
		    !L_chamber=ny
		    !!tutti 
			
			
		    do j=ny/2-L_c/2,ny/2+L_c/2
			    isfluid_LB_gathered(1:nx,j)=3
            enddo
			
			do j=ny/2-L_c/2,ny/2+L_c/2
			    isfluid_LB_gathered(nx/2-h_c/2:nx/2+h_c/2,j)=1
            enddo
			
			
			
		    !!
		    !do i=L_inl,1,-1
			   ! isfluid_LB_gathered(i+L_chamber,ny/2+h_c/2:ny/2+h_c/2+nint(dfloat(L_inl-i)*dtan(angle_inl)))=1
			   ! isfluid_LB_gathered(i+L_chamber,ny/2-h_c/2-nint(dfloat(L_inl-i)*dtan(angle_inl)):ny/2+h_c/2)=1
		    !enddo
		    !!
		    !do i=1,L_chamber
			   ! isfluid_LB_gathered(i,2:ny-1)=1 
		    !enddo
		    !!
		    !do i=1,L_inl
			   ! isfluid_LB_gathered(i+L_chamber+L_inl+L_c,ny/2+h_c/2:ny/2+h_c/2+nint(dfloat(i)*dtan(angle_inl)))=1
			   ! isfluid_LB_gathered(i+L_chamber+L_inl+L_c,ny/2-h_c/2-nint(dfloat(i)*dtan(angle_inl)):ny/2+h_c/2)=1
		    !enddo
		    !!
		    !do i=L_inl+L_chamber+L_inl+L_c,nx
			   ! isfluid_LB_gathered(i,2:ny-1)=1 
		    !enddo
		    do i=1,nx
               do j=1,ny
                   if(isfluid_LB_gathered(i,j).eq.3)then
                       do ll=1,8
                           ii=i+ex(ll)
                           jj=j+ey(ll)
                           if(ii.gt.0 .and. ii.lt.nx+1 .and. jj.gt.0 .and. jj.lt.ny+1)then
                               if(isfluid_LB_gathered(ii,jj).eq.1)then
                                   isfluid_LB_gathered(i,j)=0
                               endif
                           endif
                       enddo
                   endif
               enddo
            enddo
        endif
        !scatter to nprocss
        CALL MPI_SCATTER(isfluid_LB_gathered, nx*(ny_end-ny_in+1), MPI_INT, isfluid_LB, nx*(ny_end-ny_in+1), MPI_INT, 0, MPI_COMM_WORLD, ierr)
        !
    endsubroutine
    !**********************************************************!
    subroutine outLB_3c
    
        implicit none
        
        integer :: ii,jj
		integer,save :: iframe=0
		INTEGER :: status(MPI_STATUS_SIZE)
		integer :: ierr
        ! open wrt files
        !gather
		if(mod(step,stamp).eq.0)then
			call mpi_gather(uxy,2*nx*(ny_end-ny_in+1),MPI_DOUBLE_PRECISION,out_data_vel,2*nx*(ny_end-ny_in+1),MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
			call mpi_gather(rho_C(:,1:nx,ny_in:ny_end),3*nx*(ny_end-ny_in+1),MPI_DOUBLE_PRECISION,out_data_rho,3*nx*(ny_end-ny_in+1),MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
        endif
		!
		
        if(idrank.eq.0)then
            if(mod(step,stamp).eq.0)then
			    iframe=iframe+1
                open(unit=300,file='u'//write_fmtnumb(iframe)//'.out',status='replace')
                open(unit=301,file='v'//write_fmtnumb(iframe)//'.out',status='replace')
                open(unit=302,file='rhoA'//write_fmtnumb(iframe)//'.out',status='replace')
                open(unit=304,file='rhoB'//write_fmtnumb(iframe)//'.out',status='replace')
			    open(unit=404,file='rhoC'//write_fmtnumb(iframe)//'.out',status='replace')
                do jj=1,ny
                    do ii=1,nx
                        write(300,*) out_data_vel(1,ii,jj)
                        write(301,*) out_data_vel(2,ii,jj)
                        write(302,*) out_data_rho(1,ii,jj)
                        write(304,*) out_data_rho(2,ii,jj)
					    write(404,*) out_data_rho(3,ii,jj)
                    enddo    
                enddo
                close(300)
                close(301)
                close(302)
                close(304)
			    close(404)
                write(6,*) 'printing LB at step', step
            endif
        endif
    endsubroutine
	!**********************************************************!
	 subroutine outLB_mpi_3c
    
        implicit none
		
		integer :: mpi_integer_size,fu,count,ierr
		integer (kind=MPI_OFFSET_KIND) :: offset
		INTEGER :: status(MPI_STATUS_SIZE)
		integer,save :: iframe=0
		
		if(mod(step,stamp).eq.0)then
			iframe=iframe+1
			!rhoA
			call MPI_FILE_OPEN(MPI_COMM_WORLD, 'rhoA'//write_fmtnumb(iframe)//'.out', &
			MPI_MODE_CREATE + MPI_MODE_WRONLY, &
			MPI_INFO_NULL, fu, ierr)
			!
			call MPI_TYPE_SIZE(MPI_double_precision, mpi_integer_size, ierr)
			!
			offset = idrank * nx*(ny_end - ny_in +1) * mpi_integer_size
			!
			call MPI_FILE_SEEK(fu, offset, MPI_SEEK_SET, ierr)
			!
			call MPI_FILE_WRITE(fu, rho_c(1,1:nx,ny_in:ny_end), nx*(ny_end-ny_in+1), MPI_double_precision, status, ierr)
			
			call MPI_GET_COUNT(status, MPI_INTEGER, count, ierr)
			call MPI_FILE_CLOSE(fu, ierr)
			
			!rhoB
			call MPI_FILE_OPEN(MPI_COMM_WORLD, 'rhoB'//write_fmtnumb(iframe)//'.out', &
			MPI_MODE_CREATE + MPI_MODE_WRONLY, &
			MPI_INFO_NULL, fu, ierr)
			!
			call MPI_TYPE_SIZE(MPI_double_precision, mpi_integer_size, ierr)
			!
			offset = idrank * nx*(ny_end - ny_in +1) * mpi_integer_size
			!
			call MPI_FILE_SEEK(fu, offset, MPI_SEEK_SET, ierr)
			!
			call MPI_FILE_WRITE(fu, rho_c(2,1:nx,ny_in:ny_end), nx*(ny_end-ny_in+1), MPI_double_precision, status, ierr)
			
			call MPI_GET_COUNT(status, MPI_INTEGER, count, ierr)
			call MPI_FILE_CLOSE(fu, ierr)
			
			!rhoC
			call MPI_FILE_OPEN(MPI_COMM_WORLD, 'rhoC'//write_fmtnumb(iframe)//'.out', &
			MPI_MODE_CREATE + MPI_MODE_WRONLY, &
			MPI_INFO_NULL, fu, ierr)
			!
			call MPI_TYPE_SIZE(MPI_double_precision, mpi_integer_size, ierr)
			!
			offset = idrank * nx*(ny_end - ny_in +1) * mpi_integer_size
			!
			call MPI_FILE_SEEK(fu, offset, MPI_SEEK_SET, ierr)
			!
			call MPI_FILE_WRITE(fu, rho_c(3,1:nx,ny_in:ny_end), nx*(ny_end-ny_in+1), MPI_double_precision, status, ierr)
			
			call MPI_GET_COUNT(status, MPI_INTEGER, count, ierr)
			call MPI_FILE_CLOSE(fu, ierr)
			
			!u
			call MPI_FILE_OPEN(MPI_COMM_WORLD, 'u'//write_fmtnumb(iframe)//'.out', &
			MPI_MODE_CREATE + MPI_MODE_WRONLY, &
			MPI_INFO_NULL, fu, ierr)
			!
			call MPI_TYPE_SIZE(MPI_double_precision, mpi_integer_size, ierr)
			!
			offset = idrank * nx*(ny_end - ny_in +1) * mpi_integer_size
			!
			call MPI_FILE_SEEK(fu, offset, MPI_SEEK_SET, ierr)
			!
			call MPI_FILE_WRITE(fu, uxy(1,1:nx,ny_in:ny_end), nx*(ny_end-ny_in+1), MPI_double_precision, status, ierr)
			
			call MPI_GET_COUNT(status, MPI_INTEGER, count, ierr)
			call MPI_FILE_CLOSE(fu, ierr)
			
			!v
			call MPI_FILE_OPEN(MPI_COMM_WORLD, 'v'//write_fmtnumb(iframe)//'.out', &
			MPI_MODE_CREATE + MPI_MODE_WRONLY, &
			MPI_INFO_NULL, fu, ierr)
			!
			call MPI_TYPE_SIZE(MPI_double_precision, mpi_integer_size, ierr)
			!
			offset = idrank * nx*(ny_end - ny_in +1) * mpi_integer_size
			!
			call MPI_FILE_SEEK(fu, offset, MPI_SEEK_SET, ierr)
			!
			call MPI_FILE_WRITE(fu, uxy(2,1:nx,ny_in:ny_end), nx*(ny_end-ny_in+1), MPI_double_precision, status, ierr)
			
			call MPI_GET_COUNT(status, MPI_INTEGER, count, ierr)
			call MPI_FILE_CLOSE(fu, ierr)
			if(idrank.eq.0) write(6,*) 'printing LB at step', step
		endif
	endsubroutine
	
	!**********************************************************!
     subroutine saveLB_3c
    
        ! implicit none
        
        ! integer :: ii,jj,ll
        ! ! open wrt files
        ! !$omp single
        ! if(mod(step,stamp_save).eq.0)then
            ! open(unit=600,file='u_saved.out',status='replace')
            ! open(unit=601,file='v_saved.out',status='replace')
            ! open(unit=602,file='rhoA_saved.out',status='replace')
            ! open(unit=604,file='rhoB_saved.out',status='replace')
			! open(unit=605,file='rho_saved.out',status='replace')
			! open(unit=606,file='fa_saved.out',status='replace')
			! open(unit=607,file='fb_saved.out',status='replace')
			! open(unit=608,file='fc_saved.out',status='replace')
            ! do jj=1,ny
                ! do ii=1,nx
                    ! write(600,*) uxy(1,ii,jj)
                    ! write(601,*) uxy(2,ii,jj)
                    ! write(602,*) rho_c(1,ii,jj)
                    ! write(604,*) rho_c(2,ii,jj)
					! write(605,*) rho_c(3,ii,jj)
					! do ll=0,8
						! write(606,*) g(1,ll,ii,jj)
						! write(607,*) g(2,ll,ii,jj)
						! write(608,*) g(3,ll,ii,jj)
					! enddo
                    ! !write(307,*) dnx(ii,jj)
                    ! !write(309,*) dny(ii,jj)
                ! enddo    
            ! enddo
            ! close(600)
            ! close(601)
            ! close(602)
            ! close(604)
			! close(605)
			! close(606)
			! close(607)
			! close(608)
            ! write(6,*) 'printing fields at', step
        ! endif
        ! !$omp end single
     endsubroutine
	!**********************************************************!
    subroutine readLB_3c
    
        implicit none
        
        integer :: ii,jj,ll
		integer,save :: iframe=0
		
		do jj=1,ny
			do ii=1,nx
				read(600,*) uxy(1,ii,jj)
				read(601,*) uxy(2,ii,jj)
				read(602,*) rho_c(1,ii,jj)
				read(604,*) rho_c(2,ii,jj)
				read(605,*) rho_c(3,ii,jj)
				do ll=0,8
					read(606,*) g(1,ll,ii,jj)
					read(607,*) g(2,ll,ii,jj)
					read(608,*) g(3,ll,ii,jj)
				enddo
			enddo
		enddo
		
	endsubroutine
     !****************************************************************!
     subroutine alloc_LB_3c
        
     implicit none
	 integer :: ierr
	 !
        allocate(p(0:8),ex(0:8),ey(0:8),dex(0:8),dey(0:8),opp(0:8))
	    allocate(g(1:3,0:8,-1:nx+2,ny_in-6:ny_end+6),fb(1:3,0:8,-1:nx+2,ny_in-2:ny_end+2),uxy(1:2,1:nx,ny_in:ny_end),&
            rho_c(1:3,0:nx+1,ny_in-6:ny_end+6))
		allocate(isfluid_LB(1:nx,ny_in:ny_end),omega_c(1:nx,ny_in:ny_end),b_l(0:8))
		allocate(buf_s_to_n(1:3,0:8,0:nx+1),buf_n_to_s(1:3,0:8,0:nx+1),buf_rho_s_to_n(1:3,0:nx+1),buf_rho_n_to_s(1:3,0:nx+1))
        allocate(delta_nci(0:8,1:nx,ny_in:ny_end),pos_nci(1:2,1:nx,ny_in:ny_end),delta_nci_opp(0:8,1:nx,ny_in-6:ny_end+6))
		!if(idrank.eq.0) then
		allocate(isfluid_LB_gathered(1:nx,1:ny),rho_c_gathered(1:3,1:nx,1:ny),out_data_vel(1:2,1:nx,1:ny),out_data_rho(1:3,1:nx,1:ny),&
		g_gathered(0:8,1:nx,1:ny))
		isfluid_LB_gathered=1
		rho_c_gathered=0.0d0
		out_data_vel=0.0d0
		out_data_rho=0.0d0
		g_gathered=0.0d0
        !endif
        allocate(dnx(1:nx,ny_in:ny_end),dny(1:nx,ny_in:ny_end))
        allocate(force_disj_sc(1:2)) 
        allocate(buf_send_down_rhoc(1:3,0:nx+1,1:6),buf_send_up_rhoc(1:3,0:nx+1,1:6),&
                buf_rcv_down_rhoc(1:3,0:nx+1,1:6),buf_rcv_up_rhoc(1:3,0:nx+1,1:6),&
                buf_send_down(1:3,0:8,0:nx+1,1:6),buf_send_up(1:3,0:8,0:nx+1,1:6), &
                buf_rcv_down(1:3,0:8,0:nx+1,1:6),buf_rcv_up(1:3,0:8,0:nx+1,1:6))
        isfluid_LB=1
        g=0.0d0
        uxy=0.0d0
        rho_c=0.0d0
        omega_c=0.0d0
        b_l=(/-4.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,2.0d0/27.0d0,5.0d0/108.0d0,5.0d0/108.0d0,5.0d0/108.0d0,5.0d0/108.0d0/)
		ex=(/0,1,0,-1,0,1,-1,-1,1/)
        ey=(/0,0,1,0,-1,1,1,-1,-1/)
        dex=dfloat(ex)
        dey=dfloat(ey)
        p=(/4.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0/)
        opp=(/0,3,4,1,2,7,8,5,6/)
        call MPI_barrier(MPI_COMM_WORLD,ierr)
     endsubroutine
     !****************************************************************!
     subroutine MPI_calls
        !initialise MPI
        implicit none 
		integer :: ierr
		!
        call MPI_INIT( ierr ) 
        !save rank of this process
        call MPI_COMM_RANK( MPI_COMM_WORLD, idrank, ierr ) 
        ! save number of processes
        call MPI_COMM_SIZE( MPI_COMM_WORLD, nprocss, ierr )
        !finalize mpi
        if(idrank.eq.0)then
            write(6,*) 'rank 0','processes of',nprocss, 'processes'
        endif
     endsubroutine
     !****************************************************************!
     subroutine MPI_1D_domain_deco
        !initialise MPI
        implicit none 
        if(idrank<mod(ny,nprocss))then
            nyrank=ny/nprocss+1
            ny_in=idrank*nyrank + 1
        else
            nyrank=ny/nprocss
            ny_in=ny-(nprocss-idrank)*nyrank+1 
        endif
        ny_end=ny_in+nyrank-1
       !extrema for message passing 
        top = mod((idrank + 1) , nprocss)
        bottom = idrank - 1;
        if (bottom < 0) bottom = nprocss-1 !periodicity
		write(6,*) 'idrank',idrank,'ny_in',ny_in,'ny_end',ny_end,'top',top,'bottom',bottom
     endsubroutine
     !
    endmodule
