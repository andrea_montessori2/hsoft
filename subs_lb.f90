    module subs_lb
    
    use vars_lb
    
    implicit none
    
    contains
    
    !**********************************************************!
    subroutine moments
        implicit none
        integer :: mm,i,j
        !$omp do private(j) 
        do j=1,ny
            do i=1,nx
               if(isfluid_LB(i,j).eq.1)then
                   rho(i,j)=0.0d0
                   uxy(1,i,j)=0.0d0
                   uxy(2,i,j)=0.0d0
                   do mm=0,8
                       rho(i,j)=rho(i,j) + f(mm,i,j)
                       uxy(1,i,j)=uxy(1,i,j) + f(mm,i,j)*dex(mm)
                       uxy(2,i,j)=uxy(2,i,j) + f(mm,i,j)*dey(mm)
                   enddo
                   uxy(1,i,j)=uxy(1,i,j)/rho(i,j)
                   uxy(2,i,j)=uxy(2,i,j)/rho(i,j)
               endif
            enddo
        enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
    subroutine collision
        implicit none
        integer :: mm,i,j
        real*8,dimension(0:8) :: feq
        real*8 :: uu,udotc
        !
        !$omp do private(j)
        do j=1,ny
            do i=1,nx
                if(isfluid_LB(i,j).eq.1)then
                    uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
                    do mm=0,8
                        udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
                        feq(mm)=p(mm)*rho(i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
                        f(mm,i,j)=f(mm,i,j) + omega*(feq(mm) - f(mm,i,j))   
                    enddo
                endif
            enddo
        enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
    subroutine collision_w_lim
        implicit none
        integer :: mm,i,j
        real*8,dimension(0:8) :: feq
        real*8 :: uu,udotc,nu_avg,feqtot,ftot
        !
		!c_entropy=1
        !$omp do private(j)
		do j=1,ny
			do i=1,nx
				if(isfluid_LB(i,j).eq.1)then
					uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
					deltaS(i,j)=0.0d0
					do mm=0,8
						udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
						feq(mm)=p(mm)*rho(i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
						!
						deltaS(i,j)= deltaS(i,j) + f(mm,i,j)*(dlog(f(mm,i,j)/feq(mm))) !feqtot*(dlog(feqtot)/p(mm)) - ftot*(dlog(ftot)/p(mm))
						!
						f(mm,i,j)=f(mm,i,j) + omega*(feq(mm) - f(mm,i,j))
						!
					enddo							
				endif 
			enddo
		enddo
        !$omp end do
    endsubroutine
    !**********************************************************!
	  subroutine ehrenfest
	    implicit none
        integer :: i,mm
		real*8 :: udotc,uu,feq1
		integer,dimension(2) :: limres

		!$omp do private(i)
		do i=1,500
		  limres = maxloc(deltas,mask=deltas>1.0d-4) 
		  if (sum(limres).ne.0) then
			!
			uu=0.5d0*(uxy(1,limres(1),limres(2))*uxy(1,limres(1),limres(2)) &
			+ uxy(2,limres(1),limres(2))*uxy(2,limres(1),limres(2)))/cssq
			do mm=0,8
				udotc=(uxy(1,limres(1),limres(2))*dex(mm) + uxy(2,limres(1),limres(2))*dey(mm))/cssq
				feq1=p(mm)*rho(limres(1),limres(2))*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
				!
				!
				f(mm,limres(1),limres(2))= feq1
			enddo
			! blank out DStmp so we don't find same realisation again
			deltaS(limres(1),limres(2)) = 0.0d0
			
		  endif
		end do
		!$omp end do	
	  end subroutine 
    !**********************************************************!
	subroutine regularized
	
	integer:: i,j,k,l
	real*8 :: pxxR,pyyR,pxyR
	real*8 :: fneqR,oneovcssq_dum,uu,udotc
	real*8, dimension(0:8) :: feq
	
    !$omp do private(j)
	do j=1,ny
		do i=1,nx
			if(isfluid_LB(i,j).eq.1)then
				pxxR=0.0d0
				pyyR=0.0d0
				pxyR=0.0d0
				!
				
				uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
				do l=0,8
					udotc=(uxy(1,i,j)*dex(l) + uxy(2,i,j)*dey(l))/cssq		
					feq(l)=p(l)*rho(i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
				enddo
				
				!
				do l=0,8
					fneqR= f(l,i,j) - feq(l)
					pxxR=pxxR + (dex(l)*dex(l) - cssq)*fneqR
					pyyR=pyyR + (dey(l)*dey(l) - cssq)*fneqR
					pxyR=pxyR +  dex(l)*dey(l)*fneqR
				enddo
				do l=0,8
					f(l,i,j)= feq(l) + ((0.5d0*p(l))/(cssq**2))*((dex(l)*dex(l) - cssq)*pxxR + &
								(dey(l)*dey(l) - cssq)*pyyR+ 2.0d0*dex(l)*dey(l)*pxyR)
				enddo
			endif
		enddo
	enddo
	!$omp enddo
	endsubroutine
	!**********************************************************!
    subroutine streaming_opt
        implicit none
        
        integer:: i,j,l
!		
        !$omp do private(j) 
        do j=0,ny+1
            do i=0,nx+1
                if(isfluid_LB(i,j).ne.3) then !stream solo se non � gas n� solido interno
                    do l=0,8 
                        fb(l,i+ex(l),j+ey(l)) = f(l,i,j)
                    enddo
                endif
            enddo      
        enddo
        !$omp end do
        !$omp do private(j) 
        do j=0,ny+1
            do i=0,nx+1
                do l=0,8 
                    f(l,i,j) = fb(l,i,j)
                enddo
            enddo      
        enddo
        !$omp enddo
    endsubroutine
    
    !**********************************************************!
    subroutine all_periodic_bcs
        implicit none
		real*8,dimension(0:8) :: fdum
		integer :: i,l,j
        !$omp single
        !west
        f(:,0,1:ny)=f(:,nx,1:ny)
        !east
        f(:,nx+1,1:ny)=f(:,1,1:ny)
        !north
        f(:,1:nx,ny+1)=f(:,1:nx,1)
        ! north
        f(:,1:nx,0)=f(:,1:nx,ny)
        !corners
        f(:,0,0)=f(:,nx,ny)
        f(:,nx+1,0)=f(:,1,ny)
        f(:,0,ny+1)=f(:,nx,1)
        f(:,nx+1,ny+1)=f(:,1,1)
		!
		do i=0,nx+1
            do j=0,ny+1
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(l) = f(opp(l),i,j)
                    enddo
                    do l=0,8
                        f(l,i,j) = fdum(l)
                    enddo
                endif
            enddo
        enddo
        !$omp end single
    endsubroutine    
    !**********************************************************!
    subroutine mixed_bcs
        implicit none
        real*8,dimension(0:8) :: fdum
		real*8 :: u_avg_x
        integer :: i,l,j
        !$omp single
		!north and south
		!north
         f(:,1:nx,ny+1)= f(:,1:nx,1)
        ! south
         f(:,1:nx,0)= f(:,1:nx,ny)

		 !************************************!
		!
		do j=1,ny
			!if(isfluid_LB(nx,j).eq.1)then
				do l=0,8
					fdum(l) = f(opp(l),0,j) + rho(1,j)*6.0d0*0.01d0*dex(l)*p(l)
				enddo
				do l=0,8 
					f(l,0,j) = fdum(l) 
				enddo
			!endif 
        enddo
		!
		do j=0,ny+1
			do l=0,8
				f(l,nx+1,j)= f(l,nx,j)    
			enddo 
		enddo
        !
		f(:,0,0)=f(:,1,0)
        f(:,0,ny+1)=f(:,1,ny+1)
		!
        do i=0,nx+1
            do j=0,ny+1
                if(isfluid_LB(i,j).eq.0)then
                    do l=0,8
                        fdum(l) = f(opp(l),i,j)
                    enddo
                    do l=0,8
                        f(l,i,j) = fdum(l)
                    enddo
                endif
            enddo
        enddo
        !$omp end single
    endsubroutine 
    !**********************************************************!
    subroutine constant_force
    implicit none
    integer :: mm,i,j
    !$omp do private(j)
    do j=1,ny
        do i=1,nx
            if(isfluid_LB(i,j).eq.1)then
                do mm=0,8
                    f(mm,i,j)=f(mm,i,j) + (p(mm)/cssq)*(fx_LB*dex(mm) +fy_LB*dey(mm))
                enddo
            endif
        enddo
    enddo
    !$omp end do 
    endsubroutine
    !**********************************************************!
	subroutine constant_force_EDM
    implicit none
    integer :: mm,i,j
	real*8,dimension(0:8) ::feqs_R,feq_R
	real*8 :: udotc,udotcs,uu,uus
    !$omp do  private(j)
    do j=1,ny
        do i=1,nx
            if(isfluid_LB(i,j).eq.1)then
				uu=0.5d0*(uxy(1,i,j)*uxy(1,i,j) + uxy(2,i,j)*uxy(2,i,j))/cssq
				!
				uus=0.5d0*((uxy(1,i,j) + fx_LB)*(uxy(1,i,j) + fx_LB) + &
						  (uxy(2,i,j) + fy_LB)*(uxy(2,i,j) + fy_LB))/cssq
                do mm=0,8
					udotc=(uxy(1,i,j)*dex(mm) + uxy(2,i,j)*dey(mm))/cssq
					udotcs=((uxy(1,i,j)+fx_LB)*dex(mm) + (uxy(2,i,j)+fy_LB)*dey(mm))/cssq
                    feq_R(mm)=p(mm)*rho(i,j)*(1.0d0 + udotc + 0.5d0*udotc*udotc - uu)
					!
					feqs_R(mm)=p(mm)*rho(i,j)*(1.0d0 + udotcs + 0.5d0*udotcs*udotcs - uus)
					f(mm,i,j)=f(mm,i,j) + (feqs_R(mm) - feq_R(mm))
               
                enddo
            endif
        enddo
    enddo
    !$omp end do  
    endsubroutine
	!*****************************************************!
    subroutine LB_sim_data
    
    implicit none
    
    nx=1000
    ny=300 
    tau=0.50001d0 
    omega=1.0d0/tau
    cssq=1.0d0/3.0d0
    visc_LB=cssq*(tau-0.5d0)
    fx_LB=0.0d0
    fy_LB=0.0d0 
	shift=100
    
    write(6,*) '*******************LB data*****************'
    write(6,*) 'nx',nx
    write(6,*) 'ny',ny
    write(6,*) 'tau',tau
    write(6,*) 'visc',visc_LB
    write(6,*) 'fx_LB',fx_LB
    write(6,*) 'fy_LB',fy_LB
    write(6,*) '*******************************************'
    endsubroutine
    !**********************************************************!
    subroutine init_LB
    
    implicit none
    integer:: l
    uxy=0.0d0
    rho=1.0d0
    do l=0,8
        f(l,:,:)=p(l)
		fb(l,:,:)=p(l) ! set distribution to zero vel maxwell eq.
    enddo
    endsubroutine
    !**********************************************************!
    subroutine geometry 
        implicit none
		
        integer :: i,j,ll,ii,jj,kk,npoints_naca,xini,yini,length_naca
		real*8,allocatable :: x_naca(:),y_naca(:),xL_naca(:),yL_naca(:)
		real*8 :: t_naca
        ! generic geometry module
		!isfluid_LB(1:nx,1:ny)=3
		!naca profile symmetric
		t_naca=0.3;
		npoints_naca=100
		allocate(x_naca(1:npoints_naca+1),y_naca(1:npoints_naca+1),xL_naca(1:npoints_naca+1),yL_naca(1:npoints_naca+1))
		x_naca(1)=0.0d0
		
		do ii=2,npoints_naca+1
			x_naca(ii)=x_naca(ii-1) + 1.0d0/dfloat(npoints_naca) !reshape([0:1/npoints:1],npoints+1,1);
		enddo
		
		do jj=1,npoints_naca+1
			y_naca(jj)=5.0d0*t_naca*(0.2969*dsqrt(x_naca(jj))-0.1260*x_naca(jj) - 0.3516*x_naca(jj)**2 + 0.2843*x_naca(jj)**3 - 0.1015*x_naca(jj)**4)
		enddo
		
		yini=ny/2
		xini=400
		length_naca=100
		yL_naca=y_naca*dfloat(length_naca)  + dfloat(yini)
		xL_naca=x_naca*dfloat(length_naca)  + dfloat(xini)
		!
		!
        isfluid_LB=1
		!isfluid_LB(1:nx,1)=0
		!isfluid_LB(1:nx,ny)=0
		!isfluid_LB(1,1:ny)=0
		!isfluid_LB(nx,1:ny)=0
		!
		do ii=1,npoints_naca+1
			kk=nint(xL_naca(ii))
			do jj=yini,ny
				if(jj<=yL_naca(ii))then
					isfluid_LB(kk,jj)=3 
				endif
			enddo 
		enddo
		isfluid_LB(1:nx,1:ny/2)=isfluid_LB(1:nx,ny:ny/2+1:-1)
		!
        do i=1,nx
            do j=1,ny
                if(isfluid_LB(i,j).eq.3)then
                    do ll=1,8
                        ii=i+ex(ll)
                        jj=j+ey(ll)
                        if(ii.gt.0 .and. ii.lt.nx+1 .and. jj.gt.0 .and. jj.lt.ny+1)then
                            if(isfluid_LB(ii,jj).eq.1)then
                                isfluid_LB(i,j)=0
                            endif
                        endif
                    enddo
                endif
            enddo
        enddo
        isfluid_LB(0,:)=isfluid_LB(1,:)
		!isfluid_LB(nx+1,:)=isfluid_LB(nx,:)
		isfluid_LB(:,0)=isfluid_LB(:,1)
		isfluid_LB(:,ny+1)=isfluid_LB(:,ny)
    endsubroutine
    !**********************************************************!
    subroutine outLB
    
        implicit none
        
        integer :: ii,jj
        ! open wrt files
        !$omp single
        if(mod(step,stamp).eq.0)then
            open(unit=300,file='u'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=301,file='v'//write_fmtnumb(step/stamp)//'.out',status='replace')
            open(unit=302,file='rho'//write_fmtnumb(step/stamp)//'.out',status='replace')
            do jj=1,ny
                do ii=1,nx
                    write(300,*) uxy(1,ii,jj)
                    write(301,*) uxy(2,ii,jj)
                    write(302,*) rho(ii,jj)
                enddo    
            enddo
            close(300)
            close(301)
            close(302)
            write(6,*) 'printing LB at step',step
        endif
		
        !$omp end single
    endsubroutine
     !****************************************************************!
     subroutine alloc_LB
        
     implicit none
        allocate(p(0:8),ex(0:8),ey(0:8),dex(0:8),dey(0:8),opp(0:8))
        allocate(f(0:8,-1:nx+2,-1:ny+2),fb(0:8,-1:nx+2,-1:ny+2),uxy(2,1:nx,1:ny),rho(1:nx,1:ny),deltaS(1:nx,1:ny))
        allocate(isfluid_LB(0:nx+1,0:ny+1))
        isfluid_LB=1
		ex=(/0,1,0,-1,0,1,-1,-1,1/)
        ey=(/0,0,1,0,-1,1,1,-1,-1/)
        dex=dfloat(ex)
        dey=dfloat(ey)
        p=(/4.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/9.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0,1.0d0/36.0d0/)
        opp=(/0,3,4,1,2,7,8,5,6/)
        f=0.0d0
        uxy=0.0d0
        rho=0.0d0
     endsubroutine
     !****************************************************************!
    endmodule
    