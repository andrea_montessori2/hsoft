!  hsoft.f90 
!
!****************************************************************************
!
!  PROGRAM: hsoft.f90 in commit 
!
!****************************************************************************
    program hsoft
    
#ifdef _OPENMP
    use omp_lib
#endif    
    use subs_lb_3c
    use vars_lb_3c
	!
    implicit none
    real*8 :: ts1,ts2
    integer,dimension(8) :: dt
    integer :: ii,jj,kk
	integer :: ierr
    !
    !
    ! Variables
	!**************************
    step=0
	!**************************
	!**************************
	!**************************
	!**************************
    stamp=1000
    !
    continue
    call MPI_calls
    continue
    call LB_sim_data_3c
    continue
    call MPI_1D_domain_deco
    continue
    call alloc_LB_3c
    continue
    call geometry_3c
    continue
    call init_LB_3c
    continue
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    call outLB_mpi_3c !outLB_3c !_downsample
    continue
    !
	if(idrank.eq.0)then
        open(unit=307,file='isf.out',status='replace')
	    !do kk=1,nz
		    do jj=1,ny
			    do ii=1,nx
				    write(307,*) isfluid_LB_gathered(ii,jj) !,kk)
			    enddo    
		    enddo
	    !enddo
        close(307)
    endif
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if(idrank.eq.0) call cpu_time(ts1)
   do step=1,100000
        !LB
         call moments_3c
         continue
		 !call saveLB_3c ! save f,fb,u,v,w,rho evrywhere
		 continue
         call collision_3c 
		 continue
         call surface_tension_cap_stress_3c
         continue
         call disjoining_force_EDM_3c
         continue
         call  mixed_bcs_3c
         continue
         call streaming_opt_3c
         continue
		 call outLB_mpi_3c
   enddo
    if(idrank.eq.0) then
	 	call cpu_time(ts2)
		write(6,*) ts2-ts1
	endif
   
   if(idrank.eq.0) write(6,*) 'end'
    CALL MPI_FINALIZE(ierr) 
    end program hsoft
    
    

