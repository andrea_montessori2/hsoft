    module subs_coupling
    
    use vars
    use vars_lb
    
    implicit none
    
    contains
    !This module must be modified to be adapted to the specific coupling case
    !**********************************************************!    
    subroutine LB_to_MP_coupling
    ! generic interface to  implement the lb to mp coupling
        implicit none
        integer :: i,j,ii
        real*8 :: sigma
        !particles velocity init
        sigma=sqrt(kbt)
        do ii=1,npB
            i=nint(sxyB(1,ii))
            j=nint(sxyB(2,ii))
            if(j.eq.nys)then ! top boundary coupling
                vxyB(1,ii)=normrand(mxy(1,i,j),sigma)
                vxyB(2,ii)=normrand(mxy(2,i,j),sigma)
            endif
        enddo
        !
        do ii=1,npR
            i=nint(sxyR(1,ii))
            j=nint(sxyR(2,ii))
            if(j.eq.nys)then ! top boundary coupling
                vxyR(1,ii)=normrand(mxy(1,i,j),sigma)
                vxyR(2,ii)=normrand(mxy(2,i,j),sigma)
            endif
        enddo
    endsubroutine
    !**********************************************************! 
    subroutine hydropass_LB_to_SRD_1o1(nxinLB,nxfinLB,nyinLB,nyfinLB,nxinSRD,nxfinSRD,nyinSRD,nyfinSRD)
        implicit none
        
        integer, intent(in) :: nxinLB,nxfinLB,nyinLB,nyfinLB,nxinSRD,nxfinSRD,nyinSRD,nyfinSRD
        
        mxy(1,nxinSRD:nxfinSRD,nyinSRD:nyfinSRD)=uxy(1,nxinLB:nxfinLB,nyinLB:nyfinLB)
        mxy(2,nxinSRD:nxfinSRD,nyinSRD:nyfinSRD)=uxy(2,nxinLB:nxfinLB,nyinLB:nyfinLB)
        
    endsubroutine
    !**********************************************************!
    subroutine linear_int_xdir(nxin,nxfin)
        
        implicit none
        
        integer,intent(in) :: nxin,nxfin
        integer:: prex,prey,i,j
        
        !copia nodi sovrapposti
        prey=ceiling(dfloat(nys)/2.0d0)
        do i=nxin,nxfin,2
            prex=ceiling(dfloat(i)/2.0d0)
            mxy(1,i,nys)=uxy(1,prex,prey)
            mxy(2,i,nys)=uxy(2,prex,prey)
        enddo
        !interpolazione lineare
        j=nys
        do i=1+nxin,nxfin-1,2
            mxy(1,i,j)=mxy(1,i-1,j) + &
                ((mxy(1,i+1,j) - mxy(1,i-1,j))/(dfloat(i+1)-dfloat(i-1)))*(dfloat(i)-dfloat(i-1))
            mxy(2,i,j)=mxy(2,i-1,j) + &
                ((mxy(2,i+1,j) - mxy(2,i-1,j))/(dfloat(i+1)-dfloat(i-1)))*(dfloat(i)-dfloat(i-1))  
                   
        enddo   
    endsubroutine
    !**********************************************************!
    endmodule