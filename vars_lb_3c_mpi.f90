    module vars_lb_3c
    
    implicit none 
    
	real*8,parameter :: pi_greek=3.1415926535897932384626433d0
    real*8, allocatable,dimension(:):: p,dex,dey
    integer,allocatable,dimension(:) :: ex,ey,opp
	real*8, allocatable,dimension(:,:,:,:) :: g,fb
    real*8, allocatable,dimension(:,:,:) :: rho_c,uxy,rho_c_gathered,out_data_vel,out_data_rho,g_gathered
	real*8, allocatable,dimension(:,:,:,:) :: buf_send_up,buf_send_down,buf_rcv_up,buf_rcv_down
	real*8, allocatable,dimension(:,:,:) :: buf_s_to_n,buf_n_to_s
	real*8, allocatable,dimension(:,:,:) :: buf_send_up_rhoc,buf_send_down_rhoc,buf_rcv_up_rhoc,buf_rcv_down_rhoc
    real*8, allocatable,dimension(:,:) :: omega_c,dnx,dny
	real*8, allocatable,dimension(:,:) :: buf_rho_s_to_n,buf_rho_n_to_s
	real*8,allocatable,dimension(:,:,:) :: delta_nci,delta_nci_opp
	integer,allocatable,dimension(:,:,:) :: pos_nci
    integer,allocatable,dimension(:,:) :: isfluid_LB,isfluid_LB_gathered
    real*8, allocatable,dimension(:) :: fx_LB_2c,fy_LB_2c,b_l,force_disj_sc
    real*8 :: tau1,tau2,tau3,one_ov_nuR,one_ov_nuB,one_ov_nuG,beta_st
	real*8 :: A_rep,cssq
	real*8 :: surf_tens_RB,surf_tens_RG,surf_tens_GB
	integer :: nx,ny,step,stamp,d_droplets,stamp_save
	integer :: threads
    integer :: shift
    !********************************************
    !MPI vars
    integer :: idrank,nprocss,ny_in,ny_end,nyrank
    integer:: top,bottom

    !********************************************
    contains
	function dimenumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the number of digits
    !     of an integer number
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************

      implicit none

      integer,intent(in) :: inum
      integer :: dimenumb
      integer :: i
      real*8 :: tmp

      i=1
      tmp=real(inum,kind=8)
      do
      if(tmp< 10.d0 )exit
        i=i+1
        tmp=tmp/ 10.0d0
      enddo

      dimenumb=i

      return

     end function dimenumb

    function write_fmtnumb(inum)
 
    !***********************************************************************
    !    
    !     LBsoft function for returning the string of six characters
    !     with integer digits and leading zeros to the left
    !     originally written in JETSPIN by M. Lauricella et al.
    !    
    !     licensed under the 3-Clause BSD License (BSD-3-Clause)
    !     author: M. Lauricella
    !     last modification July 2018
    !    
    !***********************************************************************
 
    implicit none

    integer,intent(in) :: inum
    character(len=6) :: write_fmtnumb
    integer :: numdigit,irest
    !real*8 :: tmp
    character(len=22) :: cnumberlabel
    numdigit=dimenumb(inum)
    irest=6-numdigit
    if(irest>0)then
        write(cnumberlabel,"(a,i8,a,i8,a)")"(a",irest,",i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)repeat('0',irest),inum
    else
        write(cnumberlabel,"(a,i8,a)")"(i",numdigit,")"
        write(write_fmtnumb,fmt=cnumberlabel)inum
    endif
 
    return
    end function write_fmtnumb   
   
    endmodule
    
    
    
